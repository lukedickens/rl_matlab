import os
import sqlite3
import re
import logging
import math

def getfiles(dirpath,matches=None):
    """
    Get lexicographically ordered list of files in a directory, filtered by the regex filter 'matches'
    """
    if matches is not None:
        a = [s for s in os.listdir(dirpath)
             if re.match(matches,s) is not None and os.path.isfile(os.path.join(dirpath, s))]
    else:
        a = [s for s in os.listdir(dirpath)
             if os.path.isfile(os.path.join(dirpath, s))]
    a.sort(key=lambda s: os.path.getmtime(os.path.join(dirpath, s)))
    return a


def getgroups(conn,cursor,keyvalreqs,keyvalinvreqs=None):
    groups = None
    # intersection of all these groups
    for k,v in keyvalreqs.iteritems():
        #print 'select gid from groupoptions where key = ? and value = ?',(k,v)
        cursor.execute('select gid from groupoptions where key = ? and value = ?',(k,v))
        thesegroups = cursor.fetchall()
        conn.commit()
        thesegroups = set([ e[0] for e in thesegroups ])
        #print "adding groups: ", thesegroups
        if groups is None:
            groups = thesegroups
        else:
            groups.intersection_update(thesegroups)
    # minus any groups that match these
    if keyvalinvreqs is None:
        keyvalinvreqs = dict()
    for k,v in keyvalinvreqs.iteritems():
        cursor.execute('select gid from groupoptions where key = ? and value = ?',(k,v))
        thesegroups = cursor.fetchall()
        conn.commit()
        thesegroups = set([ e[0] for e in thesegroups ])
        #print "removing groups: ", thesegroups
        if groups is not None:
            groups.difference_update(thesegroups)
    return groups

def getgroupmaxes(conn,cursor):
    cursor.execute('SELECT maxes.gid,exps.eid,round(exps.sc),(exps.sqr-exps.sc*exps.sc) FROM (SELECT eid,SUM(score)/COUNT(*) AS sc,SUM(score*score)/COUNT(*) AS sqr FROM scores GROUP BY eid) AS exps INNER JOIN (SELECT j.gid,max(j.sc) maxsc FROM (SELECT x.gid,x.eid,SUM(score)/COUNT(*) AS sc FROM scores AS s INNER JOIN experiments AS x on x.eid = s.eid GROUP BY x.eid) AS j GROUP BY j.gid) AS maxes on exps.sc = maxes.maxsc')
    results = cursor.fetchall()
    conn.commit()
    return results

def getmax(conn,cursor,groups,currentsqlfname,maxsqlfname, maxgid, maxeid, maxscore, maxvar):
    maxes = getgroupmaxes(conn,cursor)
    for (gid,eid,score,var) in maxes:
        if (score > maxscore) and (gid in groups):
            maxsqlfname = currentsqlfname
            maxgid = gid
            maxeid = eid
            maxscore = score
            maxvar = var
    return maxsqlfname, maxgid, maxeid, maxscore, maxvar

def main(*args,**kwargs):
    datadir = '.'

    agenttype = kwargs.get('agenttype','Sarsa')
    schedtype = 'sync'
    modeltypes = ['gw1','gw2','gw3','gw4','gw5','gw6']
    # single agents
    print '\\begin{table}'
    print '\\begin{tabular}{l' +'c'*len(modeltypes) +'}'
    print '\\toprule'
    print ((' & %s '*len(modeltypes)) % tuple(modeltypes)) + '\\\\'
    print '\\otoprule'
    output = '\hphantom{randbatch}'
    allbestparams = {}
    for modeltype in modeltypes:
        if agenttype != 'Sarsa':
            continue
        maxsqlfname = None
        maxgid = None
        maxeid = None
        maxscore = float('-inf')
        maxvar = None
        #output = modeltype + ' & ' + schedtype
        keyvalreqs = {'numagents':1,'1:agenttype':agenttype,'schedtype':schedtype}
        keyvalinvreqs = {'utiltype':'pow','utiltype':'exp','utiltype':'negexp'}
        # go through all files
        currentsqlfname = 'decsim_singleagent_%s.db' % modeltype
        if not os.path.exists(currentsqlfname):
            continue
        conn = sqlite3.connect(currentsqlfname)
        cursor = conn.cursor()
        groups = getgroups(conn,cursor,keyvalreqs,keyvalinvreqs)
        maxsqlfname, maxgid, maxeid, maxscore, maxvar = getmax(conn,cursor,groups,currentsqlfname, maxsqlfname, maxgid, maxeid, maxscore, maxvar)
        allbestparams[modeltype] = getalloptions(maxsqlfname,maxgid,maxeid)
        if maxscore != float('-inf'):
            strscore = '$%.0f\\pm%.0f$' % (maxscore,math.sqrt(maxvar))
        else:
            strscore = '--'
        output += strscore
        if modeltype != modeltypes[-1]:
            output += ' & '
        else:
            output += ' \\\\'
        #print "modeltype = %s, maxsqlfname = %s, maxgid = %d, maxeid = %d, maxscore = %.0f, maxvar = %.0f" % (modeltype, maxsqlfname, maxgid, maxeid, maxscore, maxvar)
    print output
    print '\\bottomrule'
    print '\\end{tabular}'
    print '\\end{table}'
    print

    try:
        print "Table for single-agent: %s" % (agenttype,)
        keys = ['1:alpha0', '1:alphadecay', '1:epsilon']
        print '\\begin{table}'
        print '\\begin{tabular}{l' +'c'*len(keys) +'}'
        print '\\toprule'
        output = ' & ' + optionkeystostr(keys)
        print output
        print '\\otoprule'
        for modeltype in modeltypes:
            bestparams = allbestparams[modeltype]
            output = ' %s & ' % (modeltype,)
            for key in keys:
                output += '& %s ' % bestparams.get(key,'--')
            print output + ' \\\\'
        print '\\bottomrule'
        print '\\end{tabular}'
        print '\\end{table}'
        print
    except:
        pass



    # multiagents
    dbpfx = kwargs.get('dbpfx',r'.*')
    modeltypes = kwargs.get('modeltypes',['gw1','gw2','gw3','gw4','gw5','gw6'])
    schedtypes = kwargs.get('schedtypes',['sync','inter','batch','rand','randbatch'])
    #agenttypes = kwargs.get('agenttypes',['Sarsa','SarsaLambda','ModSarsaLambda'])
    agenttypes = kwargs.get('agenttypes',[agenttype])
    utiltypes = [None,'exp']
    allbestparams = dict([ (agenttype,{}) for agenttype in agenttypes])
    
##    header = ''
##    header += ' & '
##    for agenttype in agenttypes:
##        for utiltype in utiltypes:
##            header += ' & ' + agenttype
##            if utiltype is not None:
##                header += ' U_{\\text{%s}}' % utiltype
##    header += ' \\\\\n'
##    scoretablestr = header
    # collect the best parameters chosen for each model, schedule and agenttype
    allbestparams = {}
    allbestscores = {}
    for modeltype in modeltypes:
        modelparams = {}
        modelscores = {}
        for schedtype in schedtypes:
            schedparams = {}
            schedscores = {}
##            output = modeltype + ' & ' + schedtype
            for agenttype in agenttypes:
                agentparams = {}
                agentscores = {}
                for utiltype in utiltypes:
                    utilparams = {}
                    if utiltype is None:
                        # find groups whose group options match keyvalreqs
                        # and whose groupoptions don't match invkeyvalreqs
                        keyvalreqs = {'numagents':2,'1:agenttype':agenttype,'schedtype':schedtype}
                        keyvalinvreqs = {'agentargs:utiltype':'pow','agentargs:utiltype':'exp','agentargs:utiltype':'negexp'}
                    else:
                        # find groups whose group options match keyvalreqs
                        # and whose groupoptions don't match invkeyvalreqs
                        keyvalreqs = {'numagents':2,'1:agenttype':agenttype,'schedtype':schedtype, 'agentargs:utiltype':utiltype}
                        keyvalinvreqs = None
                    # reset the maxes
                    maxsqlfname = None
                    maxgid = None
                    maxeid = None
                    maxscore = float('-inf')
                    maxvar = None
                    # go through all relevant files to find the best choise
                    for currentsqlfname in getfiles(datadir,matches=dbpfx+modeltype+r'.*\.db$'):
                        #print currentsqlfname
                        conn = sqlite3.connect(currentsqlfname)
                        cursor = conn.cursor()
                        groups = getgroups(conn,cursor,keyvalreqs,keyvalinvreqs)
                        maxsqlfname, maxgid, maxeid, maxscore, maxvar = getmax(conn,cursor,groups,currentsqlfname, maxsqlfname, maxgid, maxeid, maxscore, maxvar)
                    if maxscore != float('-inf'):
                        strscore = '$%.0f\\pm%.0f$' % (maxscore,math.sqrt(maxvar))
                        utilparams = getalloptions(maxsqlfname,maxgid,maxeid)
                        agentparams[utiltype] = utilparams
                        agentscores[utiltype] = (maxscore,math.sqrt(maxvar))
                    else:
                        agentparams[utiltype] = {}
                        strscore = '--'
##                    output += ' & ' + strscore
                schedparams[agenttype] = agentparams
                schedscores[agenttype] = agentscores
            #print "modeltype = %s, maxsqlfname = %s, maxgid = %d, maxeid = %d, maxscore = %.0f, maxvar = %.0f" % (modeltype, maxsqlfname, maxgid, maxeid, maxscore, maxvar)
            #print output + ' \\\\'
##            scoretablestr += output + ' \\\\\n'
            modelparams[schedtype] = schedparams
            modelscores[schedtype] = schedscores
        #print "\\midrule"
##        scoretablestr += '\\midrule\n'
        allbestparams[modeltype] = modelparams
        allbestscores[modeltype] = modelscores
##    print scoretablestr

    for agenttype in agenttypes:
        for utiltype in utiltypes:
            print "Table for (agent,util): (%s,%s)" % (agenttype,utiltype)
            # header
            print '\\begin{table}'
            print '\\begin{tabular}{l' +'c'*len(modeltypes) +'}'
            print '\\toprule'
            print ((' & %s '*len(modeltypes)) % tuple(modeltypes)) + '\\\\'
            print '\\otoprule'
            for schedtype in schedtypes:
                output = schedtype
                for modeltype in modeltypes:
                    bestscores = allbestscores[modeltype][schedtype][agenttype]
                    if utiltype in bestscores:
                        bestscore,stderr = bestscores[utiltype]
                        output += ' & $%.0f\\pm%.0f$ ' % (bestscore,stderr)
                    else:
                        output += ' & -- '
                print output + ' \\\\'
                if schedtype != schedtypes[-1]:
                    print '\\midrule'
                else:
                    print '\\bottomrule'
            print '\\end{tabular}'
            print '\\end{table}'
            print


    for agenttype in agenttypes:
        for utiltype in utiltypes:
##            keys = allbestparams[modeltypes[0]][schedtypes[0]][agenttype][utiltype].keys()
##            if len(keys) == 0:
##                continue
            print "Table for (agent,util): (%s,%s)" % (agenttype,utiltype)
##            keys = set(keys)
##            keys.discard('numepisodes')
##            keys.discard('maxsteps')
##            keys.discard('numtrials')
##            keys.discard('statportion')
##            keys.discard('cycles')
            if utiltype is None:
                keys = ['1:alpha0', '1:alphadecay', '1:epsilon', \
                        '2:alpha0', '2:alphadecay', '2:epsilon', \
                        'batchsize', 'avebatchsize']
            else:
                keys = [ '1:alpha0', '1:alphadecay', '1:epsilon', '1:utiltemp', '1:utilshift', \
                         '2:alpha0', '2:alphadecay', '2:epsilon', '2:utiltemp', '2:utilshift', \
                         'batchsize', 'avebatchsize']
            print '\\begin{table}'
            print '\\begin{tabular}{ll' +'c'*len(keys) +'}'
            print '\\toprule'
            output = ' & ' + optionkeystostr(keys)
            print output
            print '\\otoprule'
            for modeltype in modeltypes:
                for schedtype in schedtypes:
                    bestparams = allbestparams[modeltype][schedtype][agenttype][utiltype]
                    output = ' %s & %s ' % (modeltype,schedtype)
                    for key in keys:
                        output += '& %s ' % bestparams.get(key,'--')
                    print output + ' \\\\'
                if modeltype != modeltypes[-1]:
                    print '\\midrule'
                else:
                    print '\\bottomrule'
            print '\\end{tabular}'
            print '\\end{table}'
            print

def optionkeystostr(keys):
    output = ''
    for k in keys:
        if k.find('alpha0') == 2:
            tok = '$\\alpha_{0}^{' + k[0] + '}$'
        elif k.find('alphadecay') == 2:
            tok = '$p_{\\alpha}^{' + k[0] + '}$'
        elif k.find('epsilon') == 2:
            tok = '$\\epsilon^{' + k[0] + '}$'
        elif k.find('lambda') == 2:
            tok = '$\\lambda^{' + k[0] + '}$'
        elif k.find('eta') == 2:
            tok = '$\\eta^{' + k[0] + '}$'
        elif k.find('utiltemp') == 2:
            tok = '$\\tau_{u}^{' + k[0] + '}$'
        elif k.find('utilshift') == 2:
            tok = '$h_{u}^{' + k[0] + '}$'
        elif k.find('utilpow') == 2:
            tok = '$p_{u}^{' + k[0] + '}$'
        else:
            tok = k
        output += ' & %s' % tok
    output += ' \\\\'
    return output

def getalloptions(sqlfname,gid,eid):
    bestparams = {}
    conn = sqlite3.connect(sqlfname)
    cursor = conn.cursor()
    cursor.execute('select key,value from groupoptions where gid = ?', (gid,))
    for (k,v) in cursor:
        bestparams[k] = v
    conn.commit()
    cursor.execute('select key,value from options where eid = ?', (eid,))
    for (k,v) in cursor:
        bestparams[k] = v
    conn.commit()
    return bestparams

def getoptions(sqlfname,eid):
    bestparams = {}
    conn = sqlite3.connect(sqlfname)
    cursor = conn.cursor()
    cursor.execute('select key,value from options where eid = ?', (eid,))
    for (k,v) in cursor:
        bestparams[k] = v
    conn.commit()
    return bestparams

if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option('--dbpfx',
                        dest='dbpfx',
                        help='The prefix of the dbfile')
    parser.add_option('--agenttype',
                        dest='agenttype',
                        help='The agenttype')


    (options, args) = parser.parse_args()
    kwargs = dict([[k,v] for k,v in options.__dict__.iteritems() if not v is None ])
    print "args",args
    print "kwargs",kwargs
    main(*args,**kwargs)

