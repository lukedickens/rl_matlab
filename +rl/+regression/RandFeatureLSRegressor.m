classdef RandFeatureLSRegressor < handle & rl.regression.LSRegressor
% picks a random set of input vectors to use as RBF centres for feature rep.
%XXX currently uses all of them
    properties (SetAccess='public')
        featuredim
    end % properties - protected, protected
    
    methods
        function [ self ] = RandFeatureLSRegressor(featuredim,varargin)
            kwargs = rl.utils.dict(varargin{:});
            self = self@rl.regression.LSRegressor(kwargs);
            self.featuredim = featuredim;
        end % function - RandFeatureLSRegressor

        function [ weights ] = train(self,inputs,outputs)
            
            self.featuremap = @(X) permute(exp(-sum(permute(repmat(X,[1,1,size(inputs,1)]),[3,2,1]) - repmat(inputs,[1,1,size(X,2)]),2)),[3,1,2]);
            weights = train@rl.regression.LSRegressor(self,inputs,outputs);
        end

    end % methods
end % classdef LSRegressor
