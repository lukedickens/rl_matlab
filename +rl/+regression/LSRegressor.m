classdef LSRegressor < handle
    properties (SetAccess='public')
        inputs
        outputs
        weights
        featuremap
    end % properties - protected, protected
    
    methods
        function [ self ] = LSRegressor(varargin)
            kwargs = rl.utils.dict(varargin{:});
            self.featuremap = kwargs.get('featuremap',[]);
            self.inputs = [];
            self.outputs = [];
        end % function - LSRegressor

        function [ weights ] = train(self,inputs,outputs)
            self.inputs = inputs;
            self.outputs = outputs;
            if isempty(self.featuremap)
                Phi = inputs;
            else
                Phi = self.featuremap(inputs);
            end
            self.weights = inv(Phi'*Phi)*Phi'*outputs;
            if nargout > 0
                weights = self.weights;
            end
        end

        function [ predictions ] = classify(self,inputs)
            if isempty(self.featuremap)
                predictions = inputs*self.weights;
            else
                predictions = self.featuremap(inputs)*self.weights;
            end
        end


        function [] = plot_errors(self,ydims)
            if nargin < 2
                ydims = [1,2]; 
            end
            estimates = self.classify(self.inputs);
            figure;
            hold on;
            for i=1:size(self.outputs,1)
                y1diff = [self.outputs(i,ydims(1)) estimates(i,ydims(1))]
                y2diff = [self.outputs(i,ydims(2)) estimates(i,ydims(2))]
                plot(y1diff,y2diff);
            end
            hold off;
        end
    end % methods
end % classdef LSRegressor
