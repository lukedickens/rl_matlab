function [] = DECSimExperiment(varargin)

    % create a dictionary from the input arguments
    kwargs = dict(varargin{:});

    %    
    %modelargs = kwargs.subdict(false,'modeltype','obstype','extent','numpenalties','numobstacles','action_effects');
    modelargs = kwargs.get('modelargs',dict());
    modelargs('modeltype') = kwargs.get('modeltype','gw1');
    [model, modeltype] = GetModel(modelargs);
    %{ 
    This is a comment block
    %}
    sim = Simulation(model);

    if kwargs.iskey('decsimargs')
        decsimargs = kwargs('decsimargs');
    else
        decsimargs = dict;
        decsimargs('smap') = @(x)([x x ]);
        %decsimargs('amap') = @(x)([ceil(x/2) (rem(x+1,2)+1)]);
        decsimargs('invamap') = @(ja) 2*(ja(:,1)-1) + ja(:,2); % -1 + 1; unnecessary addition and subtraction
        decsimargs('numactionslist') = [2 2];
    end
    decsim = DECSim(sim,decsimargs);

    schedtype = kwargs.get('schedtype','inter');
    dbfile = kwargs.get('dbfile',['decsimex_' modeltype '_' schedtype '.db']);
    agenttypes = kwargs.get('agenttypes',{'Sarsa','Sarsa'});
    % static values for the agents (if needed)
    %alpha = kwargs.get('alpha',0.1); % not yet implemented in marshall
    agentargs = kwargs.get('agentargs',dict);
    if ~agentargs.iskey('gamma')
        agentargs('gamma') = kwargs.get('gamma',0.9);
    end
    if ~agentargs.iskey('lambda') & kwargs.iskey('lambda')
        agentargs('lambda') = kwargs('lambda');
    end
    if ~agentargs.iskey('eta')
        agentargs('eta') = kwargs.get('eta',0.1);
    end
    if kwargs.iskey('utilfunc')
        if ~agentargs.iskey('utilfunc')
            error('utilfunc multply defined');
        end
        warning('should now be defined in agentargs');
        agentargs('utilfunc') = kwargs('utilfunc');
    end
    if kwargs.iskey('utiltype')
        if ~agentargs.iskey('utiltype')
            error('utiltype multply defined');
        end
        warning('should now be defined in agentargs');
        agentargs('utiltype') = kwargs('utiltype');
    end
    if kwargs.iskey('utilmult')
        if ~agentargs.iskey('utilmult')
            error('utilmult multply defined');
        end
        warning('should now be defined in agentargs');
        agentargs('utilmult') = kwargs('utilmult');
    end
    if kwargs.iskey('utilpow')
        if ~agentargs.iskey('utilpow')
            error('utilpow multply defined');
        end
        warning('should now be defined in agentargs');
        agentargs('utilpow') = kwargs('utilpow');
    end
    if kwargs.iskey('utiltemp')
        if ~agentargs.iskey('utiltemp')
            error('utiltemp multply defined');
        end
        warning('should now be defined in agentargs');
        agentargs('utiltemp') = kwargs('utiltemp');
    end
    marshallargs = dict('agenttypes',agenttypes,'schedtype',schedtype,'dbfile',dbfile,'agentargs',agentargs);
    marshall = DECSimMarshall(decsim,marshallargs);


    %agentargs = dict(agent1paramnames,num2cell(agent1params));

    if ~kwargs.iskey('params')
        agent1paramnames = {'epsilon','alpha0','alphadecay' };
        agent1params = [ 0.1, 0.2, 0.7 ];
        agent1paramsteps = [ 0.1, 0.1, 0.1 ];
        agent1bounds = [ [0,1];[0,1];[0,1]];
        agent2paramnames = {'epsilon','alpha0','alphadecay' };
        agent2params = [ 0.1, 0.2, 0.7 ];
        agent2paramsteps = [ 0.1, 0.1, 0.1 ];
        agent2bounds = [ [0,1];[0,1];[0,1]];
        paramnames = { agent1paramnames agent2paramnames};
        params = [agent1params agent2params];
        paramsteps = [agent1paramsteps agent2paramsteps];
        bounds = [agent1bounds; agent2bounds];
    else
        paramnames = kwargs('paramnames');
        params = kwargs('params');
        paramsteps = kwargs('paramsteps');
        bounds = kwargs('bounds');
    end
%    marshall.set_agents(paramnames, params);

    if strcmpi(modeltype,'gw1')
        maxsteps = 30;
        numepisodes = 1000;
    elseif strcmpi(modeltype,'gw2')
        maxsteps = 20;
        numepisodes = 300;
    elseif strcmpi(modeltype,'gw3')
        maxsteps = 50;
        numepisodes = 1500;
    elseif strcmpi(modeltype,'gw4')
        maxsteps = 75;
        numepisodes = 2000;
    elseif strcmpi(modeltype,'gw5')
        maxsteps = 50;
        numepisodes = 2500;
    elseif strcmpi(modeltype,'gw6')
        maxsteps = 150;
        numepisodes = 3000;
    else
        maxsteps = kwargs('maxsteps');
        numepisodes = kwargs('numepisodes');
    end
    %marshall.run_multiple(maxsteps,numepisodes);
    statportion = 0.5;
    %[ numepisodes, accumreturn, expreturn ] = marshall.decsim.stats(statportion)

    cycles = 2;
    numtrials = 8;
    marshall.parameter_search( paramnames, params, paramsteps, bounds, maxsteps, numepisodes, cycles, numtrials, statportion);
end
