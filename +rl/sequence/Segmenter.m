classdef Segmenter < handle
    properties (SetAccess='public')
        sequence
        rep
    end % properties - protected, protected
    
    methods
        function self = Segmenter()
        end % function - Segmenter

        function [ data ] = segment(self)
            [r c] = size(self.x);
            input = self.x(:,1:c-1);
            output = self.x(:,2:c);
        end
    end % methods

    methods(Static)

        function [ x ] = fakesequence(n,l)
            function [ step ] = next(x,l)
                lenx = length(x);
                pre = zeros(3,1);
                pre(mod(sum(x(max(lenx-l,1):lenx)),3)+1) = 1; % one of the vector [1,0,0],[0,1,0],[0,0,1]
                T = [
                    0.7     0.2     0.1
                    0.1     0.7     0.2
                    0.2     0.1     0.7
                ];
                weights = T*pre;
                step = find(mnrnd(1,weights),1);
            end
            x = randint(1,1,[1,3]);
            for i=2:n
                x = [x next(x,l)];
            end
        end
    end % methods - static
end % classdef
