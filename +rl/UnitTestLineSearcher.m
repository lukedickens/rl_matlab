display('Unit testing the LineSearcher object');


searchargs = dict;

linesearcher = LineSearcher(0.5,0.25,[0,1],searchargs);
param = linesearcher.suggest_param();
assert(param == 0.5, 'Suggested parameter is not as expected');
linesearcher.post_score(10,0.5);
param = linesearcher.suggest_param();
assert(param == 0.75, 'Suggested parameter is not as expected');
linesearcher.post_score(8,0.5);
param = linesearcher.suggest_param();
assert(param == 0.25, 'Suggested parameter is not as expected');
linesearcher.post_score(8,0.5);


linesearcher = LineSearcher(0.5,0.25,[0,1],searchargs);
param = linesearcher.suggest_param();
assert(param == 0.5, 'Suggested parameter is not as expected');
linesearcher.post_score(-10,0.5);
param = linesearcher.suggest_param();
assert(param == 0.75, 'Suggested parameter is not as expected');
linesearcher.post_score(-12,0.5);
param = linesearcher.suggest_param();
assert(param == 0.25, 'Suggested parameter is not as expected');
linesearcher.post_score(-12,0.5);

display(['linesearcher.params: ' mat2str(linesearcher.params)]);
display(['linesearcher.scores: ' mat2str(linesearcher.scores)]);
display(['linesearcher.errs: ' mat2str(linesearcher.errs)]);


x = linspace(0,1,101);
y = -(x-0.3).^2;
f = @(x)(-(x-0.3).^2 + 0.005*randn());

%searchargs('verbose') = true;
linesearcher = LineSearcher(0.1,0.05,[0,1],searchargs);

[x,terminate] = linesearcher.suggest_param();
while ~terminate
    % display(['x: ' num2str(x)]);
    for i=1:16
        vals(i) = f(x);
    end
    display(['x: ' num2str(x) ...
    ', mean(accumrewards): ' num2str(mean(vals)) ] );
    display(['linesearcher.params: ' num2str(linesearcher.params) ]);

    linesearcher.post_scores(vals);
    [x, terminate] = linesearcher.suggest_param();
end

[x, terminate] = linesearcher.suggest_param()




