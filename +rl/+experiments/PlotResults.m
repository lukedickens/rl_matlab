function PlotResults(xs,allys,xlabelstr,ylabelstr,expnames,varargin)
    kwargs = rl.utils.dict(varargin{:});
    linecolors = kwargs.get('linecolors', bone(length(expnames)+3));
    linestyles = kwargs.get('linestyles', { '-' '--' ':' '-.' '--' });
    numstyles = length(linestyles);
    linemarkers = kwargs.get('linemarkers',[ '+' 'o' '*' 'x' 's' 'd' '^' 'v' '<' '>' '.']);
    nummarkers = length(linemarkers);

    figure;
    expnums = length(expnames);
    %%% plotting
    % success rates
    hold on;
    for x=1:expnums
        h = plot(xs,allys{x});
        set(h,'color',linecolors(x,:));
        set(h,'linewidth',2);
        set(h,'marker',linemarkers(mod(x-1,nummarkers)+1));
        set(h,'markersize',10);
        set(h,'linestyle',linestyles{mod(x-1,numstyles)+1});
        handles(x) = h;
    end
    fontsizelabel = kwargs.get('fontsizelabel',18);
    xlabel(xlabelstr,'fontsize',fontsizelabel);
    ylabel(ylabelstr,'fontsize',fontsizelabel);
    fontsizelegend = kwargs.get('fontsizelegend',16);
    legloc = kwargs.get('legloc',[]);
    legend(gca,expnames,'location',legloc,'fontsize',fontsizelegend);
    if kwargs.iskey('xlimits')
        xlim(gca,kwargs('xlimits'));
    end
    if kwargs.iskey('ylimits')
        ylim(gca,kwargs('ylimits'));
    end
    if kwargs.iskey('figfname')
        saveas(gcf,kwargs('figfname'));
    end
    if kwargs.iskey('epsfname')
        print(gcf,'-depsc',kwargs('epsfname'));
    end
    hold off;
end

