function [ results ] = LearningPerformance(testagents,sim,numtrials,outputepisodes,maxsteps,varargin)
% LEARNINGPERFORMANCE - runs and evaluates the learning performance of an agent on a given simulation
% ### parameters ###
%    testagents: list of instances of agent to test
%    sim: the simulation to test on
%    numtrials: how many independent tests
%    outputepisodes: vector of episode numbers at which the performance is to be evaluated.
%    maxsteps: max number of steps per epside
%    varargin: optional arguments
    numagents = length(testagents);
    outputepisodes = reshape(outputepisodes,[length(outputepisodes),1]); % make a column vector
    kwargs = rl.utils.dict(varargin{:});
    if  kwargs.iskey('agentnames')
        agentnames = kwargs('agentnames');
    else
        for g=1:numagents
            agentnames{g} = ['agent' num2str(g)];
        end
    end
    plotcorrectintrials = ~isempty(sim.optimalactions);

    % each agent is subjected to a number of trials
    for g=1:numagents
        agent = testagents{g};
        fprintf('Testing %s\n' ,agentnames{g})
        % each trial resets agent and sim and runs through outputting at the appropriate times.
        for t=1:numtrials
            agent.reset();
            sim.purge();
            lastoutput = 0;
            timeintrials  = 0;
            for i=1:length(outputepisodes)
                oe = outputepisodes(i);
                tic
                agent.run_multiple(sim,oe-lastoutput,maxsteps);
                timeintrials = timeintrials + toc;
                lastoutput = oe;
                [numeps, accreturn] = sim.stats();
                if numeps ~= oe
                    error(['numeps: ' num2str(numeps) ' and oe: ' num2str(oe) ' do not match']);
                end
                returnintrials(g,t,i) = accreturn;
                if plotcorrectintrials
                    bestactions = agent.best_actions();
                    correctintrials(g,t,i) = sim.portionoptimalpolicy(bestactions);
                end
            end
        end
        timestats(g,:) = [mean(timeintrials),std(timeintrials)];
    end
    results.outputepisodes = outputepisodes;
    results.returnintrials = returnintrials;
    results.correctintrials = correctintrials;

    % calc and plot returns
    returnmeans = permute(mean(returnintrials,2),[3,1,2]);
    returnerrs = permute(std(returnintrials,0,2)/sqrt(numtrials),[3,1,2]);
    %plot(outputepisodes,returnmeans,'o');
    errorbar(repmat(outputepisodes,1,numagents),returnmeans,returnerrs,'o');
    results.returnmeans = returnmeans;
    results.returnerrs = returnerrs;
    legend(agentnames);
    
    % calc and plot correctrates
    if plotcorrectintrials
        figure;
        correctmeans=permute(mean(correctintrials,2),[3,1,2]);
        correcterrs=permute(std(correctintrials,0,2)/sqrt(numtrials),[3,1,2]);
        %plot(outputepisodes,correctmeans);
        errorbar(repmat(outputepisodes,1,numagents),correctmeans,correcterrs,'o');
        legend(agentnames);
        results.correctmeans = correctmeans;
        results.correcterrs = correcterrs;
    end

    % plot times
    figure;
    errorbar([1:numagents],timestats(:,1),timestats(:,2),'o');
    results.timestats = timestats;

end
