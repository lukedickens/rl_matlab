function [ results ] = ParameterGrid(sim,agenttype,sharedargs,axisparams,axisvalues,numtrials,numepisodes,maxsteps,varargin)
% PARAMETERGRID - runs and evaluates the learning performance of an agent on a given simulation
% ### required arguments ###
%    sim: the simulation to test on
%    agenttype: string desc. of agent type
%    axisparams: names of variables that are to be tested
%    axisvalues: cell of lists of different values to be taken by each parameter. So axisparams{i} can take values in list axisvalues{i}
%    numtrials: how many independent tests
%    numepisodes: number episodes in which to evaluate agent
%    maxsteps: max number of steps per epside
%    varargin: optional arguments
% ### optional arguments ###
%    sharedargs: those keyword args sent to all agents
    kwargs = rl.utils.dict(varargin{:});
    sharedargs = kwargs.get('sharedargs',rl.utils.dict('gamma',0.9));
    portion = kwargs.get('portion',0.5); % by default use last 50% of episodes for performance measure

    numparams = numel(axisparams);
    [results.listvalues results.gridvalues results.gridsize] = allvalues(axisvalues);
    for i=1:size(results.listvalues,1)
        %axisparams{:}
        %results.listvalues(i,:)
        agentargs = rl.utils.dict(axisparams,num2cell(results.listvalues(i,:)')) + sharedargs;
        agent = rl.agents.discrete.get(agenttype,sim.numstates,sim.numactions,agentargs);
        for tr=1:numtrials
            sim.purge();
            agent.reset();
            agent.run_multiple(sim,numepisodes,maxsteps);
            [ recordedepisodes, R ] = sim.stats(portion);
            results.trialreturns(i,tr) = R;
        end
    end
    % return stats gives the mean and standard error for the returns.
   results.returnstats = [ mean(results.trialreturns,2) std(results.trialreturns,0,2)/sqrt(numtrials) ];
   results.returngridmeans = reshape(results.returnstats(:,1),results.gridsize);
   results.returngriderrs = reshape(results.returnstats(:,2),results.gridsize);
end

function [ listvalues, gridvalues, gridsize ] = allvalues(axisvalues)
    % does a cross product of all axisvalues
    numparams = length(axisvalues);
    for p=1:numparams
        gridsize(p) = length(axisvalues{p});
    end
    for p=1:numparams
        repgridsize = gridsize;
        repgridsize(p) = 1;
        repelementsize = ones(1,numparams);
        repelementsize(p) = gridsize(p);
        repelement = reshape(axisvalues{p},repelementsize);
        gridvalues{p} = repmat(repelement,repgridsize);
    end
    for i=1:numel(gridvalues{1})
        for p=1:numparams
            listvalues(i,p) = gridvalues{p}(i);
        end    
    end
end


