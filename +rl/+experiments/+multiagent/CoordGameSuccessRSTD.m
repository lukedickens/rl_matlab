function [] = TestSuiteCoordGameRSTD(varargin)

    kwargs = rl.utils.dict(varargin{:});
    numtrials = kwargs.get('numtrials',50);
    ks = kwargs.get('ks',[-100:5:0]);
    gametype = kwargs.get('gametype','coord');
    maxsteps = kwargs.get('maxsteps',300);
    numepisodes = 1;
    epsilon = kwargs.get('epsilon',0.05);
    alpha0 = kwargs.get('alpha0',0.1);
    alphadecay = kwargs.get('alphadecay',0);
    schedtype = kwargs.get('schedtype','sync');

    vanillaagenttype = kwargs.get('vanillaagenttype','Sarsa');
    agenttype = kwargs.get('agenttype','RSTD');
    namestem = kwargs.get('namestem',['results/' gametype 'game_successrate_' agenttype '_' schedtype ]);
    dbfile = kwargs.get('dbfile',[namestem '.db']);
    display(['saving to dbfile: ' dbfile]);
    epsfname = kwargs.get('epsfname',[namestem '.eps']);
    display(['eps figure to: ' epsfname]);
    figfname = kwargs.get('figfname',[namestem '.fig']);
    display(['fig figure to: ' figfname]);
    dummygame = rl.environment.multiagent.CoopStratGame.GetGame(gametype,'k',-40);

    marshallargs = rl.utils.dict('agenttypes',{vanillaagenttype vanillaagenttype }, 'schedtype', schedtype, 'dbfile', dbfile, 'agentargs', rl.utils.dict('gamma',0.9));
    marshall = rl.environment.multiagent.DECSimMarshall(dummygame,marshallargs);

        expnum = 1;
        % simple sarsa
        agent1paramnames = {'epsilon','alpha0','alphadecay' };
        agent1params = [ epsilon, alpha0, 0 ];
        agent2paramnames = {'epsilon','alpha0','alphadecay' };
        agent2params = [ epsilon, alpha0, 0 ];
        paramnames = { agent1paramnames agent2paramnames };
        params = [ agent1params agent2params ];
        marshall.set_agents(paramnames, params);
        %
        display('Testing basic agent')
        [ successrates, stderrrates ] = TestAgents(marshall,gametype,ks,maxsteps,numepisodes,numtrials,paramnames,params);
        expnames{expnum} = [vanillaagenttype '(\alpha=' num2str(alpha0) ')'];
	    allsuccessrates{expnum} = successrates;
        allstderrrates{expnum} = stderrrates;

    agentargs = rl.utils.dict('gamma',0.9);
    marshallargs = rl.utils.dict('agenttypes',{agenttype agenttype},'schedtype',schedtype,'dbfile',dbfile,'agentargs',agentargs);
    marshall = rl.environment.multiagent.DECSimMarshall(dummygame,marshallargs);

    switch agenttype
    case 'RSTD'
        alpharatios = kwargs.get('alpharatios',[ 2 , 5, 10 ]);
        for alpharatio=alpharatios
            expnum = expnum + 1;
            % (alphaplus + alphaminus)/2 = 0.1
            % alphaplus/alphaminus = alpharatio
            % therefore:
            % alphaplus = alpharatio*alphaminus
            % and:
            % alphaminus = 0.2 - alphaplus = 0.2-alpharatio*alphaminus
            % (1+alpharatio)*alphaminus = 0.2
            % therefore:
        %    alphaminus = 0.2/(1+alpharatio);
        %    alphaplus = alpharatio*alphaminus;
            if alpharatio > 1
                alphaplus = 0.1;
                alphaminus = alphaplus/alpharatio;
            else
                alphaminus = 0.1;
                alphaplus = alphaminus*alpharatio;
            end
            agent1paramnames = {'epsilon','alphaplus', 'alphaminus'};
            agent1params = [ epsilon, alphaplus, alphaminus ];
            agent2paramnames = {'epsilon', 'alphaplus', 'alphaminus' };
            agent2params = [ epsilon, alphaplus, alphaminus ];
            paramnames = { agent1paramnames agent2paramnames };
            params = [ agent1params agent2params ];
            marshall.set_agents(paramnames, params);
            %
            display(['Testing RSTD with alpharatio: ' num2str(alpharatio)]);
            [ successrates, stderrrates ] = TestAgents(marshall,gametype,ks,maxsteps,numepisodes,numtrials,paramnames,params);
            expnames{expnum} = ['RSTD \alpha_{+}:\alpha_{-}=' num2str(alphaplus,'%.3f') ':' num2str(alphaminus,'%.3f') ];
            allsuccessrates{expnum} = successrates;
            allstderrrates{expnum} = stderrrates;
        end
    case {'RSSarsa','RSQLearn'}
        kapas = kwargs.get('kapas',[ 0.01, 0.015, 0.02, 0.025, 0.03, 0.05 ]);
        for kapa=kapas
            expnum = expnum + 1;
            agent1paramnames = {'epsilon','alpha0', 'alphadecay', 'kapa' };
            agent1params = [ epsilon, alpha0, alphadecay, kapa ];
            agent2paramnames = {'epsilon','alpha0', 'alphadecay', 'kapa' };
            agent2params = [ epsilon, alpha0, alphadecay, kapa ];
            paramnames = { agent1paramnames agent2paramnames };
            params = [ agent1params agent2params ];
            marshall.set_agents(paramnames, params);
            %
            display(['Testing RSSarsa with kapa: ' num2str(kapa)]);
            [ successrates, stderrrates ] = TestAgents(marshall,gametype,ks,maxsteps,numepisodes,numtrials,paramnames,params);
            expnames{expnum} = [Agent.getfigurestr(agenttype) ' \kappa=' num2str(kapa,'%.3f')  ];
            allsuccessrates{expnum} = successrates;
            allstderrrates{expnum} = stderrrates;
        end
    otherwise
        error('Unsupported risk-sensitive agent type');
    end
    legloc = kwargs.get('legloc','southeast');
    linestyles = { '-' '--' ':' '-.' '--' ':' '-.' '--' ':' '-.'};
    linemarkers = [ '+' 'o' '*' 'x' 's' 'd' '^' 'v' '<' '>' '.'];
    linecolors = bone(length(expnames)+3);
    PlotResults(ks,allsuccessrates,linecolors,linemarkers,linestyles,figfname,epsfname,'k','success rate',legloc,expnames);

end

function PlotResults(xs,allys,linecolors,linemarkers,linestyles,figfname,epsfname,xlabelstr,ylabelstr,legloc,expnames)
    figure;
    expnums = length(expnames);
    %%% plotting
    % success rates
    hold on;
    for x=1:expnums
        h = plot(xs,allys{x});
        set(h,'color',linecolors(x,:));
        set(h,'linewidth',2);
        set(h,'marker',linemarkers(x));
        set(h,'markersize',10);
        set(h,'linestyle',linestyles{x});
        handles(x) = h;
    end
    xlabel(xlabelstr,'fontsize',18);
    ylabel(ylabelstr,'fontsize',18);
    legend(gca,expnames,'location',legloc,'fontsize',16);
    ylim(gca,[0,1])
    saveas(gcf,figfname);
    print(gcf,'-depsc',epsfname);
    hold off;
end

function [ successrates, stderrrates ] = TestAgents(marshall,gametype,ks,maxsteps,numepisodes,numtrials,paramnames,params)
    successrates = [];
    stderrrates = [];
    for k = ks
        game = rl.environment.multiagent.CoopStratGame.GetGame(gametype,'k',k);
        marshall.decsim = game;
        try
            marshall.eid = marshall.register_experiment(marshall.gid);
            marshall.record_params(paramnames,params);
            marshall.record_option('k',num2str(k));
        end
        successes = 0;
        resultgrid = zeros(3,3);
        for trial=1:numtrials
            marshall.run_multiple( maxsteps, numepisodes );
            optactions = marshall.getdominantjointactions();
            successes = successes + game.testoptimalactions(optactions); % add one to successes if the correct policy is learnt
            %optactions = [optactions{1} optactions{2}];
            resultgrid(optactions{:}) = resultgrid(optactions{:}) + 1;
            marshall.reset_all();
        end
        display([num2str(successes) ' successes for k = ' num2str(k)]);
        display([mat2str(resultgrid) 'resultgrid for k = ' num2str(k)]);
        successrate = successes/numtrials;
        stderrrate = (successes*successrate^2+(numtrials-successes)*(1-successrate)^2)/numtrials/sqrt(numtrials);
        try
            marshall.record_score(1,successrate,stderrrate);
        end
        successrates = [successrates successrate];
        stderrrates = [stderrrates stderrrate];
    end
end
