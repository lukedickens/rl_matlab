function [] = CoordGameSuccess(varargin)
% Runs a series of tests on the coordination or climbing games (those that take different values of k) with different agents and produces the results in graphical form

    kwargs = rl.utils.dict(varargin{:});
    numtrials = kwargs.get('numtrials',50);
    ks = kwargs.get('ks',[-100:5:0]);
    gametype = kwargs.get('gametype','coord');
    maxsteps = kwargs.get('maxsteps',300);
    numepisodes = 1;

    gamma = kwargs.get('gamma',0.9);
    epsilon0 = kwargs.get('epsilon0',0.05);
    epsilondecay = kwargs.get('epsilondecay',0);
    temperature0 = kwargs.get('temperature0',1);
    temperaturedecay = kwargs.get('temperaturedecay',0);
    alpha0 = kwargs.get('alpha0',0.1);
    alphadecay = kwargs.get('alphadecay',0);
    beta0 = kwargs.get('beta0',0.01);
    betadecay = kwargs.get('betadecay',0);

    schedtype = kwargs.get('schedtype','sync');


    % setting agent args
    agenttype = lower(kwargs.get('agenttype','sarsa'));
    agentstr = lower(agenttype);
    agentstrstem = rl.agents.base.Agent.getfigurestr(agenttype);
    critictype = kwargs.get('critictype','Sarsa');
    agentargs = rl.utils.dict('gamma',gamma,'epsilon0',epsilon0,'epsilondecay',epsilondecay,'temperature0',temperature0,'temperaturedecay',temperaturedecay,'alpha0',alpha0,'alphadecay',alphadecay);
    if strcmp(agenttype,'qp') | strcmp(agenttype,'vp')
        agentargs('beta0') = beta0;
        agentargs('betadecay') = betadecay;
        agentargs('critictype') = critictype;
        agentstr = [agentstr '-' lower(critictype)];
    end
    if strcmp(agenttype,'rssarsa') | strcmp(agenttype,'rsqlearn')  | strcmp(critictype,'rssarsa') | strcmp(critictype,'rsqlearn') 
        agentargs('kappa0') = kwargs.get('kappa0',0.1);
        agentargs('kappadecay') = kwargs.get('kappadecay',0);
    end
    if strcmp(agenttype,'sarsaxi') | strcmp(critictype,'sarsaxi') 
        agentargs('xi0') = kwargs.get('xi0',0.1);
        agentargs('xidecay') = kwargs.get('xidecay',0);
    end

    % search specific
    searchparam = kwargs.get('searchparam','utiltemp');
    switch searchparam
    case 'utiltemp'
        utiltype = kwargs.get('utiltype','exp');
        namestem = kwargs.get('namestem',[gametype 'game_successrate_' agentstr '_util' utiltype '_' schedtype ]);
        values = kwargs.get('utiltemps',[0.1, 1, 10]);
        agentargs('utiltype') = utiltype;
        agentargs('utilshift') = kwargs.get('utilshift',0);
    case 'utilmult'
        utiltype = kwargs.get('utiltype','rplusexp');
        namestem = kwargs.get('namestem',[gametype 'game_successrate_' agentstr '_util' utiltype '_' schedtype ]);
        values = kwargs.get('utilmults',[-0.2,-0.1, 0.1, 0.2]);
        agentargs('utiltype') = utiltype;
        agentargs('utilshift') = kwargs.get('utilshift',0);
    case 'alpha0'
        namestem = kwargs.get('namestem',[gametype 'game_successrate_' agentstr '_alphas_' schedtype ]);
        values = kwargs.get('alpha0s',[0.05,0.1,0.15,0.2]);
        agentargs.pop('alpha0');
    case 'beta0'
        namestem = kwargs.get('namestem',[gametype 'game_successrate_' agentstr '_betas_' schedtype ]);
        values = kwargs.get('beta0s',[0.005,0.01,0.015,0.02]);
        agentargs.pop('beta0');
    case 'kappa0'
        namestem = kwargs.get('namestem',[gametype 'game_successrate_' agentstr '_kappas_' schedtype ]);
        values = kwargs.get('kappa0s',[0.01,0.05,0.1,0.2,0.5]);
        try
            agentargs.pop('kappa0');
        end
    case 'xi0'
        namestem = kwargs.get('namestem',[gametype 'game_successrate_' agentstr '_xis_' schedtype ]);
        values = kwargs.get('xi0s',[0.08,0.1,0.12,0.15,0.2]);
        try
            agentargs.pop('xi0');
        end
    otherwise
        error(['Unrecognised searchparam: ' searchparam]);
    end
    datadir = kwargs.get('datadir','results');
    dbfile = kwargs.get('dbfile',[datadir '/' namestem '.db']);
    epsfname = kwargs.get('epsfname',[datadir '/' namestem '.eps']);
    figfname = kwargs.get('figfname',[datadir '/' namestem '.fig']);
    dummygame = rl.environment.multiagent.CoopStratGame.GetGame(gametype,'k',-40);

    expnum = 0;

    % we can force a vanilla agent to be run
    forcevanilla = kwargs.get('forcevanilla',false);
    if strcmp(searchparam,'utiltemp') | strcmp(searchparam,'utilmult') | forcevanilla
        expnum = expnum + 1;
        vanillaagenttype = lower(kwargs.get('vanillaagenttype',agenttype));
        vanillaagentargs = rl.utils.dict('gamma',gamma,'epsilon0',epsilon0,'epsilondecay',epsilondecay,'temperature0',temperature0,'temperaturedecay',temperaturedecay,'alpha0',alpha0,'alphadecay',alphadecay);
        if strcmp(vanillaagenttype,'qp') | strcmp(vanillaagenttype,'vp')
            vanillaagentargs('beta0') = beta0;
            vanillaagentargs('betadecay') = betadecay;
            vanillaagentargs('critictype') = kwargs.get('vanillacritictype',critictype);
        end
        if strcmp(vanillaagenttype,'rssarsa') | strcmp(agenttype,'rsqlearn')  | strcmp(critictype,'rssarsa') | strcmp(critictype,'rsqlearn') 
            vanillaagentargs('kappa0') = kwargs.get('kappa0',0.1);
            vanillaagentargs('kappadecay') = kwargs.get('kappadecay',0);
        end
        if strcmp(agenttype,'sarsaxi') | strcmp(critictype,'sarsaxi') 
            agentargs('xi0') = kwargs.get('xi0',0.1);
            agentargs('xidecay') = kwargs.get('xidecay',0);
        end



        % the dummy first agent - needed for utility based agents
        marshallargs = rl.utils.dict('agenttypes',{vanillaagenttype vanillaagenttype }, 'schedtype', schedtype, 'dbfile', dbfile, 'agentargs', vanillaagentargs);
        marshall = rl.environment.multiagent.DECSimMarshall(dummygame,marshallargs);
        % simple sarsa
        agentparamnames = { };
        agentparams = [ ];
        paramnames = { agentparamnames agentparamnames };
        params = [ agentparams agentparams ];
        marshall.set_agents(paramnames, params);
        %
        display('Testing basic agent')
        [ successrates, stderrrates ] = TestAgents(marshall,gametype,ks,maxsteps,numepisodes,numtrials,paramnames,params);
    %    expnames{expnum} = [ rl.agents.base.Agent.getfigurestr(vanillaagenttype) '(\alpha=' num2str(alpha0) ')'];
        expnames{expnum} = getExpName(vanillaagenttype,'alpha0',alpha0);
	    allsuccessrates{expnum} = successrates;
        allstderrrates{expnum} = stderrrates;
    end


    % the main experimental runs
    marshallargs = rl.utils.dict('agenttypes',{agenttype agenttype},'schedtype',schedtype,'dbfile',dbfile,'agentargs',agentargs);
    marshall = rl.environment.multiagent.DECSimMarshall(dummygame,marshallargs);
    agentparamnames = {searchparam};
    allagentparamnames = { agentparamnames agentparamnames };
    for value=values
        expnum = expnum + 1;
        %
        expnames{expnum} = getExpName(agenttype,searchparam,value,agentargs);
        display(['Testing ' searchparam ': ' num2str(value)]);
        agentparams = [ value ];
        allagentparams = [ agentparams agentparams ];
        marshall.set_agents(allagentparamnames, allagentparams);
        [ successrates, stderrrates ] = TestAgents(marshall,gametype,ks,maxsteps,numepisodes,numtrials,allagentparamnames,allagentparams);
        allsuccessrates{expnum} = successrates;
        allstderrrates{expnum} = stderrrates;
    end

    % drawing the graph
    % legend location
    legloc = kwargs.get('legloc','southeast');
    linestyles = { '-' '--' ':' '-.' '--' ':' '-.' '--' ':' '-.'};
    linemarkers = [ '+' 'o' '*' 'x' 's' 'd' '^' 'v' '<' '>' '.'];
    linecolors = bone(length(expnames)+3);
    rl.experiments.PlotResults(ks,allsuccessrates,'k','success rate',expnames, ...
            'ylimits',[0,1],'linecolors',linecolors,'linemarkers',linemarkers,'linestyles',linestyles,'figfname',figfname,'epsfname',epsfname,'legloc',legloc);
end

function expname = getExpName(agenttype,testparam,val,varargin)
    kwargs = rl.utils.dict(varargin{:});
    agentstrstem = rl.agents.base.Agent.getfigurestr(agenttype);
    switch testparam
    case { 'utilmult' }
        utilmult = val;
        if strcmp(kwargs('utiltype'),'rplusexp')
            if utilmult == 0
                expname = ['sarsa U(r)=r'];
            elseif utilmult > 0
                expname = [ agentstrstem ' U(r)=r + ' num2str(utilmult) 'e^{' num2str(utilmult) 'r}'];
            else
                expname = [ agentstrstem ' U(r)=r ' num2str(utilmult) 'e^{' num2str(utilmult) 'r}'];
            end
        else
            error('Inappropriate utiltype for use with utiltemp')
        end
    case { 'utiltemp' }
        utiltemp = val;
        utilshift = kwargs.get('utilshift',0);
        if strcmp(kwargs('utiltype'),'exp')
            if utilshift == 0
                expname = [agentstrstem ' U(r)=e^{r/' num2str(utiltemp) '}'];
            elseif utilshift > 0
                expname = [agentstrstem ' U(r)=e^{(r+' num2str(utilshift) ')/' num2str(utiltemp) '}'];
            else
                expname = [agentstrstem ' U(r)=e^{(r' num2str(utilshift) ')/' num2str(utiltemp) '}'];
            end
        else
            error('Inappropriate utiltype for use with utiltemp')
        end
    case { 'alpha0' }
        expname = [agentstrstem ' \alpha=' num2str(val) ];
    case { 'beta0' }
        if kwargs.iskey('alpha0')
            expname = [agentstrstem ' \alpha=' num2str(kwargs('alpha0')) ', \beta=' num2str(val) ];
        else
            expname = [agentstrstem ' \beta=' num2str(val)];
        end
    case { 'epsilon0' }
        expname = [agentstrstem ' \epsilon=' num2str(val) ];
    case { 'kappa0' }
        expname = [agentstrstem ' \kappa=' num2str(val) ];
    case { 'xi0' }
        expname = [agentstrstem ' \xi=' num2str(val) ];
    case { 'alphadecay' }
        alpha0 = kwargs('alpha0');
        expname = [agentstrstem ' \alpha_{t}=' num2str(alpha0) '/t^' num2str(val) ];
    case { 'betadecay' }
        beta0 = kwargs('beta0');
        expname = [agentstrstem ' \beta_{t}=' num2str(beta0) '/t^' num2str(val) ];
    case { 'epsilondecay' }
        epsilon0 = kwargs('epsilon0');
        expname = [agentstrstem ' \epsilon_{t}=' num2str(epsilon0) '/t^' num2str(val) ];
    case { 'gamma' }
        expname = [agentstrstem ' \gamma=' num2str(val) '}'];
    case { 'lambda' }
        expname = [agentstrstem ' \lambda=' num2str(val) '}'];
    end
end

%%function PlotResults(xs,allys,linecolors,linemarkers,linestyles,figfname,epsfname,xlabelstr,ylabelstr,legloc,expnames)
%%    figure;
%%    expnums = length(expnames);
%%    %%% plotting
%%    % success rates
%%    hold on;
%%    for x=1:expnums
%%        h = plot(xs,allys{x});
%%        set(h,'color',linecolors(x,:));
%%        set(h,'linewidth',2);
%%        set(h,'marker',linemarkers(x));
%%        set(h,'markersize',10);
%%        set(h,'linestyle',linestyles{x});
%%        handles(x) = h;
%%    end
%%    xlabel(xlabelstr,'fontsize',18);
%%    ylabel(ylabelstr,'fontsize',18);
%%    legend(gca,expnames,'location',legloc,'fontsize',16);
%%    ylim(gca,[0,1])
%%    saveas(gcf,figfname);
%%    print(gcf,'-depsc',epsfname);
%%    hold off;
%%end

function [ successrates, stderrrates ] = TestAgents(marshall,gametype,ks,maxsteps,numepisodes,numtrials,allagentparamnames,allagentparams)
    successrates = [];
    stderrrates = [];
    for k = ks
        game = rl.environment.multiagent.CoopStratGame.GetGame(gametype,'k',k);
        marshall.decsim = game;
        marshall.eid = marshall.register_experiment(marshall.gid);
        marshall.record_params(allagentparamnames,allagentparams);
        marshall.record_option('k',num2str(k));
        successes = 0;
        resultgrid = zeros(3,3);
        for trial=1:numtrials
            marshall.run_multiple( maxsteps, numepisodes );
            optactions = marshall.getdominantjointactions();
            successes = successes + game.isoptimalpolicy(optactions); % add one to successes if the correct policy is learnt
            %optactions = [optactions{1} optactions{2}];
            resultgrid(optactions{:}) = resultgrid(optactions{:}) + 1;
            marshall.reset_all();
        end
        display([num2str(successes) ' successes for k = ' num2str(k)]);
        display([mat2str(resultgrid) 'resultgrid for k = ' num2str(k)]);
        successrate = successes/numtrials;
        stderrrate = (successes*successrate^2+(numtrials-successes)*(1-successrate)^2)/numtrials/sqrt(numtrials);
        marshall.record_score(1,successrate,stderrrate);
        successrates = [successrates successrate];
        stderrrates = [stderrrates stderrrate];
    end
end
