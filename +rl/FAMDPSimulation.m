classdef POMDPSimulation < Simulation
%% An function approximation mdp simulation
    properties (SetAccess='private')
        % some attributes will belong to a simulation whether or not it is model based.
        featuriser
        dimensions
    end
    
    methods
        function sim = POMDPSimulation(model,featuriser)
            %% The constructor method
            sim = sim@Simulation(model);
            sim.featuriser = featuriser;
            sim.dimensions = dimensions;
        end % Simulation
        
        function state = init(sim)
            hidden_state = init@Simulation(sim); % (true) states are hidden observed states are called observations
            state = sim.featuriser(hidden_state);     % get first observed feature vector
        end % init
        function [ reward, nextstate ] = next(sim,action)
            %% takes:
            %%      action choice, action
            %% returns:
            prior_hs = sim.state; % prior hidden state
            [reward, post_hs] = next@Simulation(sim,action); % Update hidden state and next reward
            nextstate = sim.featuriser(post_hs);     % get next observed feature vector
        end % next
        function [unbiased_policy] = UnbiasedPolicy(sim)
            %%GETUNBIASEDPOLICY For every non-absorbing observation (rows) we give equal probability to each action (columns)
            %% This assumes there is one absorbing observation with the highest index
            unbiased_policy = @(hidden_state)(1./sim.numactions*ones(1,sim.numactions));
        end
    end % methods
end % classdef
