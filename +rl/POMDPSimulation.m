classdef POMDPSimulation < Simulation & handle
%% A pomdp simulation
    properties (SetAccess='public')
        obfunc
        num_obs
    end
    methods
        function sim = POMDPSimulation(model)
            %% The constructor method
            sim = sim@Simulation(model);
            sim.num_obs = model.num_obs;
            sim.obfunc = model.obfunc;
        end % Simulation
        
        function state = init(sim)
            dummy_s = -1; % dummy prior state and action needed to pass to obfunc
            dummy_a = -1; % 
            post_s = init@Simulation(sim); % (true) states are hidden observed states are called observations
            dist = sim.obfunc(dummy_s,dummy_a,post_s,[1:sim.num_obs]');     % get multinomial for observations
            state = find(mnrnd(1,dist),1); % get first observed state
            sim.newepisode(state);
        end % init
        function [ reward, nextstate ] = next(sim,action)
            %% takes:
            %%      action choice, action
            %% returns:
            prior_hs = sim.state; % prior hidden state
            [reward, post_hs] = next@Simulation(sim,action); % Update hidden state and next reward
            dist = sim.obfunc(prior_hs,action,post_hs,[1:sim.num_obs]'); % get multinomial for observations
            nextstate = find(mnrnd(1,dist')); % get next observed state
            sim.newstep(action,reward,nextstate); % currently we record the observed state in the trace
        end % next
        function [unbiased_policy] = UnbiasedPolicy(sim)
            %%GETUNBIASEDPOLICY For every non-absorbing observation (rows) we give equal probability to each action (columns)
            %% This assumes there is one absorbing observation with the highest index
            unbiased_policy = [repmat(1./sim.numactions*ones(1,sim.numactions),sim.num_obs-1,1); zeros(1,sim.numactions) ];
        end
    end % methods
end % classdef
