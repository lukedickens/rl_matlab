function [historygroups,expnames] = TestCompare(varargin)

% stop the warnings about structs caused by dict
warning('off','all');
if nargin > 0
    if isscalar(varargin{1})
        torun = [varargin{1}];
    else
        torun = varargin{1};
    end
else
    torun = [1:6];
end

runsper = 8;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%             GridWorld1            %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if any(torun == 1)

    model = GridWorld1();
    sim = Simulation(model);
    sim = SimRecorder(sim);
    VisualiseGrid(model,model.statenames);
    print(gcf,'-depsc','results/GW1.eps');
    numepisodes = 1500;
    maxsteps = 50;
    gamma = 0.9;

    historygroups = cell(0);
    %% Sarsa
    %% default values
    alpha0 = 0.12;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = Sarsa( sim, gamma, epsilon, @(i)(alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{1} = histories;
    expnames{1} = 'Sarsa';
    %% Sarsa Normed
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    epsilon = 1/40;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{2} = histories;
    expnames{2} = 'SarsaN';
    %% Sarsa Lambda
    %% default values
    alpha0 = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambda( sim, gamma, epsilon, @(i)(alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{3} = histories;
    expnames{3} = 'Sarsa(\lambda)';
    %% Sarsa
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambdaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{4} = histories;
    expnames{4} = 'Sarsa(\lambda)N';

%%    kwargs = dict;
%%    kwargs('ylim') = [-2000,4000];
%%    PlotHistoryGroups(historygroups,expnames,kwargs);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%             GridWorld2            %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if any(torun == 2)

    model = GridWorld2();
    sim = Simulation(model);
    sim = SimRecorder(sim);
    VisualiseGrid(model,model.statenames);
    print(gcf,'-depsc','results/GW2.eps');
    numepisodes = 1500;
    maxsteps = 50;
    gamma = 0.9;

    historygroups = cell(0);
    %% Sarsa
    %% default values
    alpha0 = 0.12;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = Sarsa( sim, gamma, epsilon, @(i)(alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{1} = histories;
    expnames{1} = 'Sarsa';
    %% Sarsa Normed
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    epsilon = 1/40;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{2} = histories;
    expnames{2} = 'SarsaN';
    %% Sarsa Lambda
    %% default values
    alpha0 = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambda( sim, gamma, epsilon, @(i)(alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{3} = histories;
    expnames{3} = 'Sarsa(\lambda)';
    %% Sarsa
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambdaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{4} = histories;
    expnames{4} = 'Sarsa(\lambda)N';

%%    kwargs = dict;
%%    kwargs('ylim') = [-2000,4000];
%%    PlotHistoryGroups(historygroups,expnames,kwargs);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%             GridWorld3            %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if any(torun == 3)

    model = GridWorld3();
    sim = Simulation(model);
    sim = SimRecorder(sim);
    VisualiseGrid(model,model.statenames);
    print(gcf,'-depsc','results/GW3.eps');
    numepisodes = 1500;
    maxsteps = 50;
    gamma = 0.9;

    historygroups = cell(0);
    %% Sarsa
    %% default values
    alpha0 = 0.12;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = Sarsa( sim, gamma, epsilon, @(i)(alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{1} = histories;
    expnames{1} = 'Sarsa';
    %% Sarsa Normed
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    epsilon = 1/40;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{2} = histories;
    expnames{2} = 'SarsaN';
    %% Sarsa Lambda
    %% default values
    alpha0 = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambda( sim, gamma, epsilon, @(i)(alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{3} = histories;
    expnames{3} = 'Sarsa(\lambda)';
    %% Sarsa
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambdaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{4} = histories;
    expnames{4} = 'Sarsa(\lambda)N';

%%    kwargs = dict;
%%    kwargs('ylim') = [-2000,4000];
%%    PlotHistoryGroups(historygroups,expnames,kwargs);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%             GridWorld4            %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if any(torun == 4)

    model = GridWorld4();
    sim = Simulation(model);
    sim = SimRecorder(sim);
    VisualiseGrid(model,model.statenames);
    print(gcf,'-depsc','results/GW4.eps');
    numepisodes = 1500;
    maxsteps = 50;
    gamma = 0.9;

    historygroups = cell(0);
    %% Sarsa
    %% default values
    alpha0 = 0.12;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = Sarsa( sim, gamma, epsilon, @(i)(alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{1} = histories;
    expnames{1} = 'Sarsa';
    %% Sarsa Normed
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    epsilon = 1/40;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{2} = histories;
    expnames{2} = 'SarsaN';
    %% Sarsa Lambda
    %% default values
    alpha0 = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambda( sim, gamma, epsilon, @(i)(alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{3} = histories;
    expnames{3} = 'Sarsa(\lambda)';
    %% Sarsa
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambdaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{4} = histories;
    expnames{4} = 'Sarsa(\lambda)N';

%%    kwargs = dict;
%%    kwargs('ylim') = [-2000,4000];
%%    PlotHistoryGroups(historygroups,expnames,kwargs);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%             GridWorld5            %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if any(torun == 5)

    model = GridWorld5();
    sim = Simulation(model);
    sim = SimRecorder(sim);
    VisualiseGrid(model,model.statenames);
    print(gcf,'-depsc','results/GW5.eps');
    numepisodes = 1500;
    maxsteps = 50;
    gamma = 0.9;

    historygroups = cell(0);
    %% Sarsa
    %% default values
    alpha0 = 0.12;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = Sarsa( sim, gamma, epsilon, @(i)(alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{1} = histories;
    expnames{1} = 'Sarsa';
    %% Sarsa Normed
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    epsilon = 1/40;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{2} = histories;
    expnames{2} = 'SarsaN';
    %% Sarsa Lambda
    %% default values
    alpha0 = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambda( sim, gamma, epsilon, @(i)(alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{3} = histories;
    expnames{3} = 'Sarsa(\lambda)';
    %% Sarsa
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambdaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{4} = histories;
    expnames{4} = 'Sarsa(\lambda)N';

%%    kwargs = dict;
%%    kwargs('ylim') = [-2000,4000];
%%    PlotHistoryGroups(historygroups,expnames,kwargs);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%             GridWorld6            %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if any(torun == 6)

    model = GridWorld6();
    sim = Simulation(model);
    sim = SimRecorder(sim);
    VisualiseGrid(model,model.statenames);
    print(gcf,'-depsc','results/GW6.eps');
    numepisodes = 1500;
    maxsteps = 50;
    gamma = 0.9;

    historygroups = cell(0);
    %% Sarsa
    %% default values
    alpha0 = 0.12;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = Sarsa( sim, gamma, epsilon, @(i)(alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{1} = histories;
    expnames{1} = 'Sarsa';
    %% Sarsa Normed
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    epsilon = 1/40;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{2} = histories;
    expnames{2} = 'SarsaN';
    %% Sarsa Lambda
    %% default values
    alpha0 = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambda( sim, gamma, epsilon, @(i)(alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{3} = histories;
    expnames{3} = 'Sarsa(\lambda)';
    %% Sarsa
    %% default values
    alpha0 = 0.15;
    eta = 1/10;
    lambda = 0.2;
    epsilon = 1/35;
    histories = cell(0);
    for r=1:runsper
        [ policy, Q ] = SarsaLambdaNormed( sim, gamma, epsilon, @(i)(alpha0), @(i)(eta*alpha0), lambda, numepisodes, maxsteps);
        histories{r} = sim.history;
        sim.purge();
    end
    historygroups{4} = histories;
    expnames{4} = 'Sarsa(\lambda)N';

%%    kwargs = dict;
%%    kwargs('ylim') = [-2000,4000];
%%    PlotHistoryGroups(historygroups,expnames,kwargs);
end


end
