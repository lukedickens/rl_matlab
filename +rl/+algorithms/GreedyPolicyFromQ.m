function [GreedyPolicy] =  GreedyPolicyFromQ(Q, absorbing)
%%GREEDYPOLICYFROMQ Gets a greedy-policy from a Q function (and the absorbing states)
%% Note: to get a greedy policy from Q, you only need the Q and the absorbing states (if there are any)
if nargin < 2
    % if absorbing not passed in then assume no absorbing states
    absorbing = zeros(1,size(Q,1));
end
numstates = length(Q(:,1));
numactions = length(Q(1,:));
GreedyPolicy = zeros(numstates, numactions); % each row has numactions possible actions each has an assigned probability
%
%% Notation:
%%      s = state
for s = 1:numstates
    % policy unspecified at absorbing states
    if absorbing(s)
        continue
    end
    % find max value for each state, and set probability to 1
    [value, index] = max(Q(s,:));
    GreedyPolicy(s, index) = 1;
end







