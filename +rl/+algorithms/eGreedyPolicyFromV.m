function [eGreedyPolicy] =  eGreedyPolicyFromV(V, model, gamma, epsilon)
    if ~isfield(model,'absorbing')
    % if absorbing not available in then assume no absorbing states
    absorbing = zeros(1,size(Q,1));
else
    absorbing = model.absorbing;
end
[ GreedyPolicy ] = rl.algorithms.GreedyPolicyFromV(V, model, gamma);
numactions = length(GreedyPolicy(1,:));
UnbiasedPolicy = 1./numactions * ~absorbing'*ones(1,numactions);
eGreedyPolicy = (1-epsilon)*GreedyPolicy + epsilon*UnbiasedPolicy;






