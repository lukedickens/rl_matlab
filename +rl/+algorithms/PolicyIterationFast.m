function [optimal_policy, V, Q ] =  PolicyIterationFast( model, gamma, theta, policy0, Q0 )
    % Finds Optimal Policy for given MDP and will optionally return optimal value function too
    numstates = model.numstates;
    numactions = model.numactions;
    if ~isfield(model,'absorbing')
        % if absorbing not available in model then assume no absorbing states
        absorbing = zeros(1,numstates);
    else
        absorbing = model.absorbing;
    end
    initial = model.initial;

    %
    if nargin < 4
        % default initial policy is unbiased
        policy = rl.algorithms.GetUnbiasedPolicy(numactions, absorbing);
    else
        policy = policy0;
    end
    if nargin < 5
        % default state-action value is zero
        Q = zeros(numstates, numactions); 
    else
        Q = Q0;
    end

    %
    total = 0;
    distinctpolicies = numstates*numactions;
    diff = 1; %arbitrary number which is not zero
    while (diff ~= 0) & (total < distinctpolicies) %need only loop as many times as there are unique policies.
        %fprintf('In PolicyIterationFast %dth iteration',total);
        [V] = rl.algorithms.PolicyEvaluationFast( model, gamma, theta, policy ); % Policy Evaluation
        [new_policy] =  rl.algorithms.GreedyPolicyFromV(V, model, gamma); % Policy Improvement
        %diff = sum(sum((policy - new_policy).^2)); % elementwise square then sum, any difference gives diff>0
        diff = any(any((policy ~= new_policy))); % any difference gives diff>0
        % avoid enless loops
        total = total + 1;
        policy = new_policy;
    end
    optimal_policy = policy;

    if nargout > 2
        T = model.T;
        R = model.R;
        for s=1:numstates, for a=1:numactions
            %Rsa = R(:,s,a)
            %sizeRsa = size(Rsa)
            %Tsa = T(:,s,a)
            %sizeTsa = size(Tsa)
            %sizeV = size(V)
            Q(s,a) =  T(:,s,a)'*(R(:,s,a)+gamma*V);
        end, end
    end

end
