function [ V ] = PolicyEvaluationSteps( model, gamma, steps, policy)
%%POLICYEVALUATIONSTEPS Gives truncated estimate of V function from MDP model with dynamic programming approach
%% takes:
%%      a model (MDP)
%%      geometric discount, gamma;
%%      number of iterations, steps;
%%      a policy, policy
%%
%% returns: 
%%      an estimate for V
%%
    numstates = model.numstates;
    numactions = model.numactions;
    t = model.t;
    r = model.r;
    if ~isfield('absorbing',model)
        % if absorbing not available in model then assume no absorbing states
        absorbing = zeros(1,numstates);
    else
        absorbing = model.absorbing;
    end

    assert (numstates == length(policy(:,1))); % number of states - introspecting policy matrix
    assert (numactions == length(policy(1,:))); % number of actions - introspecting policy matrix
    V = zeros(numstates, 1); % i.e. optimal value function vector (optimal value function for each state) 11x1
    newV = V;

    %% Notation:
    %%      prior_s = prior state
    %%      prior_a = prior action
    %%      post_s = posterior state
    for i = 1:steps
        for prior_s = 1:numstates
            if absorbing(prior_s) % do not update absorbing states
                continue;
            end
            tmpV = 0;
            for prior_a =1:numactions
                tmpQ = 0;
                for post_s=1:numstates
                    tmpQ = tmpQ + t(prior_s,prior_a,post_s)*(r(prior_s,prior_a,post_s) + gamma*V(post_s));
                end
                tmpV = tmpV + policy(prior_s,prior_a)*tmpQ;
            end
            newV(prior_s) = tmpV;
        end
        diffVec = abs(newV - V);
        Delta = max(diffVec);
        V = newV;
    end

end
