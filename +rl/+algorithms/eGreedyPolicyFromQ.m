function [eGreedyPolicy] =  eGreedyPolicyFromQ(Q, epsilon, absorbing)
% EGREEDYPOLICYFROMQ : get epsilon greedy policy given a Q table and epsilon (absorbing states optionally specified)
if nargin < 3
    % if absorbing not passed in then assume no absorbing states
    absorbing = zeros(1,size(Q,1));
end
[ GreedyPolicy ] = rl.algorithms.GreedyPolicyFromQ(Q, absorbing);
numactions = length(GreedyPolicy(1,:));
UnbiasedPolicy = 1./numactions * ~absorbing'*ones(1,numactions);
eGreedyPolicy = (1-epsilon)*GreedyPolicy + epsilon*UnbiasedPolicy;






