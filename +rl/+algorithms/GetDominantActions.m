function [ dominant_actions ] = GetDominantActions(policy, absorbing, actionnames)
%%GETDOMINANTACTIONS Gets the (first) dominant action name for each state in a policy
[ tmp dominant_action_ids ] = max(policy');
z = size(policy);
S = z(1);
A = z(2);
assert (A == length(actionnames));
replicated_names = repmat(actionnames',S,1);
dominant_actions = replicated_names(sub2ind([S,A],[1:S]', dominant_action_ids'));
dominant_actions(find(absorbing)) = ' ';
end
