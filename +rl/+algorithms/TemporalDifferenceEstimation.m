function [Q] = TemporalDifferenceEstimation( simulation, gamma, alpha, numepisodes, maxsteps, policy)
%%TEMPORALDIFFERENCEESTIMATION Estimates Q function from MDP simulation 
%% takes:
%%      a simulation
%%      geometric discount, gamma;
%%      level of e-Greedy policy randomness, epsilon;
%%      fixed learning rate, alpha;
%%      number of episodes, numepisodes;
%%      maximum number of steps per epsiode, maxsteps;
%%      and a policy to estimate
%%
%% returns: 
%%      an estimate for Q
numstates = simulation.numstates;
numactions = simulation.numactions;
absorbing = simulation.absorbing;
init = simulation.init;
next = simulation.next;
%
assert (numstates == size(policy,1)); % number of states - introspecting policy matrix
assert (numactions == size(policy,2)); % number of actions - introspecting policy matrix
%
Q = zeros(numstates, numactions); % i.e. state-action value function estimate

%% Notation:
%%      s = prior state
%%      a = prior action
%%      ss = posterior state
%%      aa = posterior action
totsteps = 0;
for i=1:numepisodes
  s = init(); % get initial state
  a = find(mnrnd(1,policy(s,:)),1); % select action

  for j=1:maxsteps
    totsteps = totsteps + 1;
    % get next state and reward from simulation
    [ reward, ss ] = next(s,a);
    if absorbing(ss) ~= 1
        % sample next action
        aa = find(mnrnd(1,policy(ss,:)),1);
        %% then update
        % for efficiency we only want to read this value once.
        priorQ = Q(s,a);
        % the temporal difference update
        Q(s,a) = priorQ + alpha(totsteps)*(reward + gamma*Q(ss,aa) - priorQ );
        % ready for next iteration
        s = ss;
        a = aa;
    else
        % the final temporal difference update
        priorQ = Q(s,a)
        Q(s,a) = priorQ + alpha(totsteps)*(reward + gamma*0 - priorQ);
        break;
    end
  end
end

end


