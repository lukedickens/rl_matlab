function [optimal_policy V Q ] =  PolicyIteration( model, gamma, theta, policy0, Q0 )
% Finds Optimal Policy for given MDP and will optionally return optimal value function too
    numstates = model.numstates;
    numactions = model.numactions;
    t = model.t;
    r = model.r;
    if ~isfield(model,'absorbing')
        % if absorbing not available in model then assume no absorbing states
        absorbing = zeros(1,numstates);
    else
        absorbing = model.absorbing;
    end
    initial = model.initial;

    %
    if nargin < 4
        % default initial policy is unbiased
        policy = rl.algorithms.GetUnbiasedPolicy(numactions, absorbing);
    else
        policy = policy0;
    end
    if nargin < 5
        % default state-action value is zero
        Q = zeros(numstates, numactions); 
    else
        Q = Q0;
    end

    %
    diff = 1; %arbitrary number which is not zero
    while (diff ~= 0)
        [V] = rl.algorithms.PolicyEvaluation( model, gamma, theta, policy ); % Policy Evaluation
        [new_policy] =  rl.algorithms.GreedyPolicyFromV(V, model, gamma); % Policy Improvement
        diff = sum(sum((policy - new_policy).^2)); % elementwise square then sum, any difference gives diff>0
        policy = new_policy;
    end
    % optimal_V
    optimal_policy = policy;

    if nargout > 2
        t = model.t;
        r = model.r;
        for s=1:numstates, for a=1:numactions
            Qtmp = 0;
            for ss=1:numstates
                Qtmp = Qtmp + t(s,a,ss)'*(r(s,a,ss)+gamma*V(s));
            end
            Q(s,a) =  Qtmp;
        end, end
    end
end

