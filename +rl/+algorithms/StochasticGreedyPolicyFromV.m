function [ greedy_policy ] = StochasticGreedyPolicyFromV(V, model, gamma, tol, rel)
%%STOCHASTICGREEDYPOLICYFROMV Gets policy of all (near)-dominant actions for each state in a value function
%% takes:
%%      an estimate for V
%%      a model (MDP)
%%      geometric discount, gamma;
%%      absolute accuracy, tol;
%%      relative accuracy, rel;
%%
%% returns: 
%%      a stochastic greedy policy (including all near dominant actions)
%%
    numstates = model.numstates;
    numactions = model.numactions;
    t = model.t;
    r = model.r;
    if ~isfield(model,'absorbing')
        % if absorbing not available in model then assume no absorbing states
        absorbing = zeros(1,numstates);
    else
        absorbing = model.absorbing;
    end
    greedy_policy = zeros(numstates, numactions); % each row has numactions possible actions each has an assigned probability

    %% Notation:
    %%      s = prior state
    %%      a = prior action
    %%      ss = posterior state
    for s = 1:numstates
        if absorbing(s)
            continue
        end
        tmpQs = zeros(numactions, 1);
        for a =1:numactions
            tmpQ = 0;
            for ss=1:numstates
                tmpQ = tmpQ + t(s,a,ss)*(r(s,a,ss) + gamma*V(ss));
            end
            tmpQs(a) = tmpQ;
        end
        [value, indices] = almost_max(tmpQs, tol, rel);
        greedy_policy(s, indices) = 1/sum(indices);
    end

end

function [ true_max allmost_max_indices ] = almost_max(vector,tol,rel)
    true_max = max(vector)
    allmost_max_indices = almost_equal(vector,true_max,tol,rel)
end

function [ res ] = almost_equal(x,y,tol,rel)
    test1 = tol
    test2 = rel*abs(x)
    absdiff = abs(x-y)
    res1 = (absdiff <= test1);
    res2 = (absdiff <= test2);
    res = res1 | res2
end
