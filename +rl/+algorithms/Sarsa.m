function [optimal_policy Q deltas ] = Sarsa( simulation, gamma, epsilon, alpha, numepisodes, maxsteps, policy0, Q0 )
%%SARSA find optimal policy from an MDP simulation.
%% takes:
%%      a simulation
%%      gamma : geometric discount
%%      epsilon : level of e-Greedy policy randomness
%%      alpha : learning rate number or function (mapping step number to learning step size)
%%      numepisodes : number of episodes
%%      maxsteps : maximum number of steps per epsiode
%% optional arguments
%%      policy0 : an initial policy
%%      Q0 : initial Q values
%%
%% returns: 
%%      Optimal Policy
%%      optionally a Q-estimate.
%%

    numstates = simulation.numstates;
    numactions = simulation.numactions;
    absorbing = simulation.absorbing;

    if isnumeric(alpha)
        % alpha should be a function, this converts numeric
        % alphas to constant functions.
        alpha = @(i) alpha;
    end

    %
    if nargin < 7
        % default initial policy is unbiased
        policy = rl.algorithms.GetUnbiasedPolicy(numactions, absorbing);
    else
        policy = policy0;
    end
    if nargin < 8
        % default state-action value is zero
        Q = zeros(numstates, numactions); 
    else
        Q = Q0;
    end

    %% Notation:
    %%      s = prior state
    %%      a = prior action
    %%      ss = posterior state
    %%      aa = posterior action
    totsteps = 0;
    for i=1:numepisodes
      s = simulation.init(); % get initial state
      a = find(mnrnd(1,policy(s,:)),1); % get action from policy
      for j=1:maxsteps
        totsteps = totsteps + 1;
        % get next state and reward from simulation
        [ reward, ss ] = simulation.next(a);
        if simulation.terminal() ~= 1
            % get next action from policy
            aa = find(mnrnd(1,policy(ss,[1:numactions])),1);
            %% and then update
            % for efficiency we only want to read this value once.
            priorQ = Q(s,a);
            % the temporal difference update
            Q(s,a) = priorQ + alpha(totsteps)*(reward + gamma*Q(ss,aa) - priorQ );
            % update the policy
            policy(s,:) =  rl.algorithms.eGreedyPolicyFromQ(Q(s,:), epsilon, absorbing(s));
            % ready for next iteration
            s = ss;
            a = aa;
        else
            % the final temporal difference update
            priorQ = Q(s,a);
            delta = (reward + gamma*0 - priorQ);
            Q(s,a) = priorQ + alpha(totsteps)*delta;
            deltas(totsteps) = delta;
            % update the policy
            policy(s,:) =  rl.algorithms.eGreedyPolicyFromQ(Q(s,:), epsilon, absorbing(s));
            break;
        end
      end
    end
    optimal_policy = rl.algorithms.GreedyPolicyFromQ(Q, absorbing);
end


