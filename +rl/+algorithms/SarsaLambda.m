function [optimal_policy, varargout ] = SarsaLambda( simulation, gamma, epsilon, alpha, lambda, numepisodes, maxsteps, policy0, Q0 )
%%SARSA find optimal policy from an MDP simulation.
%% takes:
%%      simulation : a simulation
%%      gamma : geometric discount
%%      epsilon : level of e-Greedy policy randomness
%%      alpha : learning rate function; maps integer to learning step size
%%      lambda : eligibility decay parameter
%%      numepisodes : number of episodes
%%      maxsteps : maximum number of steps per epsiode
%% optional arguments
%%      policy0 : an initial policy
%%      Q0 : initial Q values
%%
%% returns: 
%%      Optimal Policy
%%      optionally a Q-estimate.
%%

    numstates = simulation.numstates;
    numactions = simulation.numactions;
    absorbing = simulation.absorbing;

    %
    if nargin < 6
        % default initial policy is unbiased
        policy = rl.algorithms.GetUnbiasedPolicy(numactions, absorbing);
    else
        policy = policy0;
    end
    if nargin < 7
        % default state-action value is zero
        Q = zeros(numstates, numactions); 
    else
        Q = Q0;
    end

    %% Notation:
    %%      s = prior state
    %%      a = prior action
    %%      ss = posterior state
    %%      aa = posterior action
    totsteps = 0;
    for i=1:numepisodes
      % init eligibility trace each episode
      z = zeros(numstates, numactions); 
      s = simulation.init(); % get initial state
      a = find(mnrnd(1,policy(s,:)),1); % get action from policy
      for j=1:maxsteps
        totsteps = totsteps + 1;
        % get next state and reward from simulation
        [ reward, ss ] = simulation.next(a);
        % for efficiency we only want to read this value once.
        priorQ = Q(s,a);
        % update eligibility vector
        z(s,a) = z(s,a) + 1;
        % different behaviour depending if it is a terminal step
        isterminal = simulation.terminal();
        if isterminal ~= 1
            % get next action from policy
            aa = find(mnrnd(1,policy(ss,[1:numactions])),1);
            % the td-error
            delta = (reward + gamma*Q(ss,aa) - priorQ );
        else
            % the final td-error
            delta = (reward + gamma*0 - priorQ );
        end
        % the temporal difference update
        Q = Q + alpha(totsteps)*delta*z;
        % the eligibility degrade
        z = gamma*lambda*z;
        % update the policy
        policy =  rl.algorithms.eGreedyPolicyFromQ(Q, epsilon, absorbing);
        % break from loop if terminal
        if isterminal == 1
            break
        end % if
        % ready for next iteration
        s = ss;
        a = aa;
      end % for each step in episode
    end % for each episode.
    optimal_policy = rl.algorithms.GreedyPolicyFromQ(Q, absorbing);
end


