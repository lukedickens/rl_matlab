function [greedy_policy] =  GreedyPolicyFromV(V, model, gamma)
%%GREEDYPOLICYFROMV Gets a greedy-policy from a V function, a model, and a geometric discount gamma
%% Note: To get a greedy policy from V, you need to know the model
numstates = model.numstates;
numactions = model.numactions;
t = model.t;
r = model.r;
if ~isfield(model,'absorbing')
    % if absorbing not available in then assume no absorbing states
    absorbing = zeros(1,size(V,1));
else
    absorbing = model.absorbing;
end

greedy_policy = zeros(numstates, numactions); % each row has numactions possible actions each has an assigned probability

%% Notation:
%%      s = prior state
%%      a = prior action
%%      ss = posterior state
for s = 1:numstates
    if absorbing(s)
        continue
    end
    tmpQs = zeros(numactions, 1);
    for a =1:numactions
        tmpQ = 0;
        for ss=1:numstates
            tmpQ = tmpQ + t(s,a,ss)*(r(s,a,ss) + gamma*V(ss));
        end
        tmpQs(a) = tmpQ;
    end
    [value, index] = max(tmpQs);
    greedy_policy(s, index) = 1;
end

end

