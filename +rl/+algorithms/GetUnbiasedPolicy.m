function [unbiased_policy] = GetUnbiasedPolicy(numactions, absorbing)
%%GETUNBIASEDPOLICY For every non-absorbing state (rows) we give equal probability to each action (columns)
unbiased_policy = 1./numactions * ~absorbing'*ones(1,numactions);
end
