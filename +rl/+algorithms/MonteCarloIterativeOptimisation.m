function [ optimal_policy Q ] = MonteCarloIterativeOptimisation( simulation, gamma, epsilon, alpha, numepisodes, maxsteps, policy0, Q0)
%%MONTECARLOITERATIVEOPTIMISATION Finds optimal policy with iterative updates to Q function.
%% Every visit updates (simpler than first visit updates)
%% takes:
%%      a simulation
%%      geometric discount, gamma;
%%      level of e-Greedy policy randomness, epsilon;
%%      alpha : learning rate number or function (mapping step number to learning step size)
%%      number of traces/episodes per batch, numepisodes;
%%      maximum number of steps per episode, maxsteps
%%      initial policy (optional)
%%
%% returns:
%%      Estimate of the Optimal Policy.

    numstates = simulation.numstates;
    numactions = simulation.numactions;
    absorbing = simulation.absorbing;

    if isnumeric(alpha)
        % alpha should be a function, this converts numeric
        % alphas to constant functions.
        alpha = @(i) alpha;
    end

    %
    if nargin < 6
        % default initial policy is unbiased
        policy = rl.algorithms.GetUnbiasedPolicy(numactions, absorbing);
    else
        policy = policy0;
    end
    if nargin < 7
        % default state-action value is zero
        Q = zeros(numstates, numactions); 
    else
        Q = Q0;
    end

    for i=1:numepisodes

        [trace] = simulation.run(maxsteps,policy);
        tracelen = length(trace.rewards);
        return_t = 0; % get last reward as return for penultimate state and action.
        
        for t=tracelen:-1:1       % step through time-steps in reverse order
            return_t = trace.rewards(t) + gamma*return_t; % return for time t in terms of reward and return at t+1
            s = trace.states(t); % get state index from trace at time t
            a = trace.actions(t); % get action index
            priorQ = Q(s,a);
            Q(s,a) = priorQ + alpha(totsteps)*(return_t - priorQ); % update Q.
        end

        % update the policy with epsilon greedy policy from Q
        policy = rl.algorithms.eGreedyPolicyFromQ(Q, epsilon, absorbing);
    end
    % finally use the greedy policy from Q
    optimal_policy = rl.algorithms.GreedyPolicyFromQ(Q, absorbing);
end


