function [ optimal_policy V ] =  ValueIteration(model, gamma, theta, V0 )
% Finds Optimal Policy for given MDP
% Finds Optimal Policy for given MDP and will optionally return optimal value function too
    numstates = model.numstates;
    numactions = model.numactions;
    t = model.t;
    r = model.r;
    if ~isfield(model,'absorbing')
        % if absorbing not available in model then assume no absorbing states
        absorbing = zeros(1,numstates);
    else
        absorbing = model.absorbing;
    end
    initial = model.initial;
    if nargin < 4
        % default state-action value is zero
        V = zeros(numstates, 1); 
    else
        V = V0;
    end
    newV = V;

    Delta = 2*theta;
    while Delta > theta
        for prior_s = 1:numstates
            if absorbing(prior_s) % do not update absorbing states
                continue;
            end
            tmpQs = zeros(1,numactions);
            for prior_a =1:numactions
                tmpQ = 0;
                for post_s=1:numstates
                    tmpQ = tmpQ + t(prior_s,prior_a,post_s)*(r(prior_s,prior_a,post_s) + gamma*V(post_s));
                end
                tmpQs(prior_a) = tmpQ;
            end
            newV(prior_s) = max(tmpQs);
        end
        diffVec = abs(newV - V);
        Delta = max(diffVec);
        V = newV;
    end
    optimal_policy =  rl.algorithms.GreedyPolicyFromV(V, model, gamma);

    if nargout > 2
        t = model.t;
        r = model.r;
        for s=1:numstates, for a=1:numactions
            Qtmp = 0;
            for ss=1:numstates
                Qtmp = Qtmp + t(s,a,ss)'*(r(s,a,ss)+gamma*V(s));
            end
            Q(s,a) =  Qtmp;
        end, end
    end
end
