function [ optimal_policy Q ] = QLearning( simulation, gamma, epsilon, alpha, numepisodes, maxsteps, policy0, Q0)
%% takes:
%%      a simulation
%%      gamma : geometric discount
%%      epsilon : level of e-Greedy policy randomness
%%      alpha : learning rate number or function (mapping step number to learning step size)
%%      maxsteps : maximum number of steps per epsiode
%%      numepisodes : number of episodes
%% optional arguments
%%      policy0 : an initial policy
%%      Q0 : initial Q values
%%  
%% returns:
%%      Optimal Policy
%%      Q-estimate (optional)

    numstates = simulation.numstates;
    numactions = simulation.numactions;
    absorbing = simulation.absorbing;
    init = simulation.init;
    next = simulation.next;

    if isnumeric(alpha)
        % alpha should be a function, this converts numeric
        % alphas to constant functions.
        alpha = @(i) alpha;
    end

    %
    if nargin < 7
        % default initial policy is unbiased
        policy = rl.algorithms.GetUnbiasedPolicy(numactions, absorbing);
    else
        policy = policy0;
    end
    if nargin < 8
        % default state-action value is zero
        Q = zeros(numstates, numactions); 
    else
        Q = Q0;
    end

    %% Notation:
    %%      s = prior state
    %%      a = prior action
    %%      ss = posterior state
    %%      aa = posterior action
    totsteps = 0
    for i=1:numepisodes
      s = init(); % get starting state
      % select action from policy
      a = find(mnrnd(1,control_policy(s,:)),1);

      for j=1:maxsteps    
        totsteps = totsteps + 1;
        % get next state and reward from simulation
        [ reward, ss ] = next(s,a);
        % if not at terminating state
        if absorbing(ss) ~= 1
            % select next action from policy
            aa = find(mnrnd(1,control_policy(ss,:)),1); 
            % cache Q(s,a) for efficiency
            priorQ = Q(s,a);
            % update the Q-table
            Q(s,a) = priorQ + alpha(totsteps)*(reward + gamma*max(Q(ss,:)) - priorQ );
            % update the control policy
            control_policy(s,:) =  rl.algorithms.eGreedyPolicyFromQ(Q(s,:), epsilon, absorbing(s));
            % ready for next iteration
            s = ss;
            a = aa;
        else
            % cache Q(s,a) for efficiency
            priorQ = Q(s,a);
            % the final temporal difference update
            Q(s,a) = priorQ + alpha(totsteps)*(reward + gamma*0 - priorQ);
            % update the control policy
            control_policy(s,:) =  rl.algorithms.eGreedyPolicyFromQ(Q(s,:), epsilon, absorbing(s));
            break;
          break;
        end
      end
    end
    optimal_policy = rl.algorithms.GreedyPolicyFromQ(Q, absorbing);

end


