function [ optimal_policy Q ] = SarsaNormed( simulation, gamma, epsilon, alpha, alpha_, numepisodes, maxsteps, policy0, Q0 )
%%SARSA find optimal policy from an MDP simulation.
%% takes:
%%      a simulation (MDP)
%%      geometric discount, gamma;
%%      level of e-Greedy policy randomness, epsilon;
%%      learning rate function for td, alpha; maps integer to learning step size
%%      learning rate function for var, alpha_; maps integer to learning step size
%%      number of episodes, numepisodes;
%%      maximum number of steps per epsiode, maxsteps;
%%      optional first argument is an initial policy
%%      optional second argument is an initial Q function
%%
%% returns: 
%%      Optimal Policy
%%      optionally a Q-estimate.
%%

    numstates = simulation.numstates;
    numactions = simulation.numactions;
    absorbing = simulation.absorbing;

    %
    if nargin < 6
        % default initial policy is unbiased
        policy = rl.algorithms.GetUnbiasedPolicy(numactions, absorbing);
    else
        policy = policy0;
    end
    if nargin < 7
        % default state-action value is zero
        Q = zeros(numstates, numactions); 
    else
        Q = Q0;
    end
    Qvar = ones(numstates, numactions); 

    %% Notation:
    %%      s = prior state
    %%      a = prior action
    %%      ss = posterior state
    %%      aa = posterior action
    totsteps = 0;
    for i=1:numepisodes
      s = simulation.init(); % get initial state
      a = rl.algorithms.DrawFromDist(policy(s,:)); % get action from policy
      for j=1:maxsteps
        totsteps = totsteps + 1;
        % get next state and reward from simulation
        [ reward, ss ] = simulation.next(a);
        % for efficiency we only want to read these values once.
        priorQ = Q(s,a);
        priorQvar = Qvar(s,a);
        % different behaviour depending if it is a terminal step
        isterminal = simulation.terminal();
        if isterminal ~= 1
            % get next action from policy
            aa = rl.algorithms.DrawFromDist(policy(ss,[1:numactions]));
            %% and then update
            td_error = reward + gamma*Q(ss,aa) - priorQ;
        else
            % the final temporal difference update
            td_error = reward + gamma*0 - priorQ;
        end % if
        %
        % the temporal difference update (normalised by the variance)
        Q(s,a) = priorQ + alpha(totsteps)*td_error/priorQvar;
        % now update the variance
        postQvar = priorQvar + alpha_(totsteps) *(td_error^2 - priorQvar);
        if postQvar > 0 % don't let it fall below zero
            Qvar(s,a) = postQvar;
        end % if
        %
        % update the policy
        policy(s,:) =  rl.algorithms.eGreedyPolicyFromQ(Q(s,:), epsilon, absorbing(s));
        % break from loop if terminal
        if isterminal == 1
            break
        end % if
        %
        % ready for next iteration
        s = ss;
        a = aa;
      end % for each step
    end % for each episode
    optimal_policy = rl.algorithms.GreedyPolicyFromQ(Q, absorbing);
end


