function [Q] = MonteCarloEstimation(simulation,gamma,numepisodes,maxsteps,policy)
%%MONTECARLOESTIMATION Estimates Q function from MDP model
%% Uses every visit updates (as this is simpler than first visit updates)
%% takes:
%%      a MDP simulation
%%      geometric discount, gamma;
%%      number of traces/episodes, numepisodes;
%%      maximum number of steps per epsiode, maxsteps;
%%      policy to evaluate, policy;
%%
%% returns: 
%%      an estimate for Q
%%
    numstates = simulation.numstates;
    numactions = simulation.numactions;
    assert (numstates == size(policy,1)); % number of states - introspecting policy matrix
    assert (numactions == size(policy,2)); % number of actions - introspecting policy matrix

    % initialise returns lists
    returns = cell(numstates,numactions);
    for i=1:numepisodes

        [trace] = simulation.run(maxsteps,policy);
        tracelen = length(trace.rewards);
        return_t = 0; % get last reward as return for penultimate state and action.
        
        for t=tracelen:-1:1       % step through time-steps in reverse order
            return_t = trace.rewards(t) + gamma*return_t; % return for time t in terms of reward and return at t+1
            s = trace.states(t); % get state index from trace at time t
            a = trace.actions(t); % get action index
            returns{s, a} = [returns{s, a} return_t ]; % append return value to appropriate return list
        end
    end
    % update the Q values by averaging the return lists for each state-action pair
    Q = get_averages(returns);
end

% subfunctions (get_averages)
function Q = get_averages( returns )
% Returns a vector with the averages of the list of lists
[numrows,numcols] = size(returns); % find the shape of the returns
    for i=1:numrows
        for j=1:numcols
            if ~isempty(returns{i,j})
                Q(i,j) = mean(returns{i,j});
            else 
                Q(i,j) = 0; % default estimate for empty list
            end
        end
    end
end

