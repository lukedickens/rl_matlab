function [ V ] = PolicyEvaluationFast(model, gamma, theta, policy)
%%POLICYEVALUATION Estimates V function from MDP model with dynamic programming approach
%% takes:
%%      a model (MDP)
%%      geometric discount, gamma;
%%      accuracy threshold, theta;
%%      a policy, policy
%%
%% returns: 
%%      an estimate for V
%%
    numstates = model.numstates;
    numactions = model.numactions;
    T = model.T;
    R = model.R;
    if ~isfield(model,'absorbing')
        % if absorbing not available in model then assume no absorbing states
        absorbing = zeros(1,numstates);
    else
        absorbing = model.absorbing;
    end

    assert (numstates == size(policy,1)); % number of states - introspecting policy matrix
    assert (numactions == size(policy,2)); % number of actions - introspecting policy matrix

    V = zeros(numstates, 1); % i.e. optimal value function vector (optimal value function for each state) 11x1

	% the policy transition operator matrix element PT(k,i,j) is probability of
	% transitioning from si to sk under aj given policy and transition funciton
	PT = repmat(permute(policy,[3,1,2]),[numstates,1,1]).*T;

    Delta = 2*theta; % ensure initial Delta is greater than theta
    while Delta >= theta
		oldV = V;
        %V = sum(sum(PT.*(R + gamma*repmat(V,[1,numstates,numactions])),3),1)'.*~absorbing';
        V = sum(sum(PT.*bsxfun(@plus,R,gamma*V),3),1)'.*~absorbing';
        Delta = max(abs(oldV - V));
    end
end
