function [optimal_policy Q ] = MonteCarloBatchOptimisation( simulation, gamma, epsilon, numbatches, numepisodes, maxsteps, policy)
%%MONTECARLOBATCHOPTIMISATION Finds optimal policy for mdp (simulation) using montecarlo batch optimisation
%% takes:
%%      a simulation
%%      geometric discount, gamma;
%%      level of e-Greedy policy randomness, epsilon;
%%      number of batches, numbatches;
%%      number of traces/episodes per batch, numepisodes;
%%      maximum number of steps per episode, maxsteps
%%      initial policy (optional)
%%
%% returns:
%%      Estimate of the optimal policy.
%%      optionally the estimated optimal Q function estimate,

    numactions = simulation.numactions;
    absorbing = simulation.absorbing;
    %
    % default initial policy is unbiased
    if nargin < 7
        % policy may be unspecified
        policy = rl.algorithms.GetUnbiasedPolicy(numactions, absorbing);
    end

    for i=1:numbatches
        % estimate the Q function with monte-carlo estimation
        Q = rl.algorithms.MonteCarloEstimation(simulation,gamma,numepisodes,maxsteps,policy);
        % update the policy with epsilon greedy policy from Q
        policy =  rl.algorithms.eGreedyPolicyFromQ(Q, epsilon, absorbing);
    end
    % finally use the greedy policy from Q
    optimal_policy = rl.algorithms.GreedyPolicyFromQ(Q, absorbing);

end
