classdef RobotArmSim < DynSim & handle
    methods
        %% ----- CONSTself.RUCTOself.R
        function self = RobotArmSim(x0s,u0, T, dt, target, costWeightsInc, costWeightsFinal, storetraj)
            self = self@DynSim(x0s,u0, T, dt, target, costWeightsInc, costWeightsFinal, storetraj);
        end

        function [l, l_x, l_xx, l_u, l_uu, l_ux] = cost_fn(self,x,target,u,terminal)     % x is 4x1, t = nan signals final time
            if nargin < 4
                terminal = false;
            end
            x = x - target;
            if terminal, % final cost
            % if final cost isnan returns 1 if true and we enter the if statement.
                l = 1/2*x'*self.Qf*x;                                                         
            else % running cost                                                                           
                l = 1/2*u'*self.R*u + 1/2*x'*self.Q*x;                                             
            end
            % compute derivatives of cost
            if nargout>1,
                l_x = self.Q*x;                                                             % q 4x1
                l_xx = self.Q;                                                              % self.Q 4x4
                l_u = self.R*u;                                                             % r 4x1
                l_uu = self.R;                                                              % self.R 1x1
                l_ux = zeros(length(self.R),length(self.Q));                                     % P 1x4    
                if terminal, % final cost
                    l_x = diag(self.Qf).*x;                                                 
                    l_xx = self.Qf;                                                                        
                end
            end % if - more than one output
        end

        function [ xdot, xdot_x, xdot_u  ] = dyn_fn(x,u)
            m1_   = 1.4; %0.7; % [kg]    % segment mass
            m2_   = 1.1; %1.5; % m2_   = 2.5; % [kg]
            l1_   = 0.3; % [m]   % segment length
            l2_   = 0.33; % [m]
            s1_   = 0.11;   % segment center of mass
            s2_   = 0.165;
            i1_   = 0.025;  % segment moment of inertia
            % i2_   = 0.072; 
            i2_   = 0.045;
            b11_  = 0.5;    % joint friction
            b22_  = 0.5;
            b12_  = 0.1;
            b21_  = 0.1;

            % to add weight to the end: add weight to mass m2 and increase i2 by I = I + 
            EPS = 1E-5;     % finite difference epsilon


            %------------------------ compute inertia I and extra torque H --------
            % temp vars
            mls = m2_*l1_*s2_;
            iml = i1_ + i2_ + m2_*l1_^2;
            dd = i2_*iml-i2_^2;
            sy = sin(x(2,:));
            cy = cos(x(2,:));

            % inertia
            I_11 = iml+2*mls*cy;
            I_12 = i2_+mls*cy;
            I_22 = i2_*ones(size(cy));

            % determinant
            detI = dd-mls^2*cy.^2;

            % inverse inertia I1
            I1_11 = i2_./detI;
            I1_12 = (-i2_-mls*cy)./detI;
            I1_22 = (iml+2*mls*cy)./detI;

            % temp vars
            sw = sin(x(2,:));
            cw = cos(x(2,:));
            y = x(3,:);
            z = x(4,:);

            % extra torque H (Coriolis, centripetal, friction)
            H = [-mls*(2*y+z).*z.*sw + b11_*y + b12_*z;...
                mls*y.^2.*sw + b22_*z + b12_*y];


            %------------- compute xdot = inv(I) * (torque - H) ------------
            torque = u - H;
            xdot = [x(3:4,:); ...
                   I1_11.*torque(1,:) + I1_12.*torque(2,:); ...
                   I1_12.*torque(1,:) + I1_22.*torque(2,:)];


            %----------- compute xdot_x using finite differences ------------
            if nargout>1,
               x1 = repmat(x, [1,4]) + eye(4)*EPS;
               x2 = repmat(x, [1,4]) - eye(4)*EPS;
               uu = repmat(u,[1,4]);
               f1 = RobotArmSim.dyn_fn(x1,uu);
               f2 = RobotArmSim.dyn_fn(x2,uu);
               xdot_x = (f1-f2)/2/EPS;

               xdot_u = zeros(4,2);
               xdot_u(3,1) = I1_11;
               xdot_u(3,2) = I1_12;
               xdot_u(4,1) = I1_12;
               xdot_u(4,2) = I1_22;
            end
        end

    end % methods

end % RobotArmSim
