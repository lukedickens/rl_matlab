classdef Base < handle % keeps track of rewards
    %% ----- SET UP
    properties (Hidden=true)
        episode
        history % a history of traces
        step
        torecord
    end
   
    methods
        %% ----- CONSTRUCTOR
        function self = Base(torecord)
            self.purge();
            if nargin < 1
                %   recording:   s,a,r
                self.torecord = [0,0,1];
            else
                self.torecord = torecord;
            end
        end % Base
        
        % ----- storing values in traces
        function [] = newepisode(self,state)
            self.episode = self.episode + 1;
            if self.torecord(1)
                self.history{self.episode}.states = [state];
            end
            if self.torecord(2)
                self.history{self.episode}.actions = [];
            end
            if self.torecord(3)
                self.history{self.episode}.rewards = [];
            end
            self.step = 1;
        end % newepisode
        function [ ] = newstep(self,action,reward,state)
            if self.torecord(2)
                self.history{self.episode}.actions(:,self.step) = action;
            end
            if self.torecord(3)
                self.history{self.episode}.rewards(:,self.step) = reward;
            end
            self.step = self.step + 1;
            if self.torecord(1)
                self.history{self.episode}.states(:,self.step) = state;
            end
        end % step
        function purge(self)
            % remove all records to begin fresh recording
            self.history = {};
            self.episode = 0;
            self.step = 1;
        end % purge
        
        % get statistics for the reward traces
        function [ numepisodes, accumreturn, expreturn, stderr ] = stats(self,portion)
            if nargin < 2
                portion = 1;
            end
            returns = [];
            totalnumepisodes = length(self.history);
            numepisodes = floor(totalnumepisodes *portion);
            startpoint = totalnumepisodes-numepisodes;
            for j=1:numepisodes
                returns(j) = sum(self.history{j+startpoint}.rewards);
            end
            returns = returns(find(isfinite(returns)));
            numepisodes = length(returns);
            accumreturn = sum(returns);
            expreturn = accumreturn/numepisodes;
            stderr = std(returns)/sqrt(numepisodes);
        end % stats

        % this gets a sequence of accumulated returns in either stepwise or episodewise way.
        function [ accums ] = getaccums(self,stepwise)
            if nargin < 2
                stepwise = true;
            end
            if stepwise
                tot = 0;
                j=1;
                for i=1:length(self.history)
                    for r=self.history{i}.rewards
                        tot = tot + r;
                        accums(j) = tot;
                        j = j+1;
                    end
                end
            else % episodewise
                tot = 0;
                for i=1:length(self.history)
                    tot = tot + sum(self.history{i}.rewards);
                    accums(i) = tot;
                end
            end
        end


    end % methods
end % classdef
