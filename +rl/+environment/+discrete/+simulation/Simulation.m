classdef Simulation < handle & rl.environment.discrete.simulation.Base
%% An mdp simulation
    properties (SetAccess='public')
        optimalactions
        initial
    end
    properties (Hidden=true)
        numstates % number of states
        numactions % number of actions
        statenames % ordered list of state names (index is state id)
        actionnames % ordered list of action names (index is action id)
        absorbing % boolean vector over state ids corresponding to whether states are absorbing
        t
        r
        state % the current state
    end
    
    methods
        function self = Simulation(model,varargin)
            kwargs = rl.utils.dict(varargin{:});
            self = self@rl.environment.discrete.simulation.Base(kwargs.get('torecord',[0,0,1]));
            %% The constructor method
            self.numstates = model.numstates;
            self.numactions = model.numactions;
            self.statenames = model.statenames;
            self.actionnames = model.actionnames;
            self.absorbing = model.absorbing;
            self.initial = model.initial;
            self.t = model.t;
            self.r = model.r;
            try
                self.optimalactions = model.optimalactions;
            end
        end % Simulation
        
        function state = init(self)
            % Initialises state from initial distribution and then outputs this (as no hidden state)
            state = find(mnrnd(1,self.initial),1); % index of the 1 in random vector drawn from initial multinomial parameters
            self.state = state;
            self.newepisode(state);
        end % init
        
        function [ reward, nextstate ] = next(self,action)
            %% takes:
            %%      action choice, action
            %% returns:
            %%      next state and reward setting self.state to new state
            dist = self.t(self.state,action,[1:self.numstates]'); % weights over next state
            nextstate = find(mnrnd(1,dist),1); % next state drawn from initial multinomial parameters
            reward = self.r(self.state,action,nextstate); % reward is r(s,a,s')
            self.state = nextstate; % current state is now the newly created one
            self.newstep(action,reward,nextstate);
        end % next
        function [trace] = run(self,maxsteps,policy)
            %% takes:
            %%      maximum number of steps per epsiode, maxsteps;
            %%      control policy, policy;
            %% returns:
            %%      a simulated trace/episode in the sim following the policy

            %% NOtation
            % s - the prior/previous state
            % a - the prior/previous action
            % ss - the subsequent state
            % aa - the subsequent action
            % get the initial state
            self.init();
            % store the initial state
            trace.states(1) = self.state;
            for i = 1:maxsteps
                % if the trace has not terminated then choose another action from the policy
                action = find(mnrnd(1,policy(self.state,:)),1);
                % store the action
                trace.actions(i) = action;
                % the posterior state is drawn probabilistically from the next function
                % the state-action-state triple determines the reward
                [reward] = self.next(action);
                % store the reward
                trace.rewards(i) = reward;
                % store the state
                trace.states(i+1) = self.state;
                if self.terminal()
                    break;
                end % if
            end % for
        end % run_trace

        function answer = terminal(self)
            try
                answer = self.absorbing(self.state);
            catch
                answer = false;
            end % try
            return
        end % init
        function [unbiased_policy] = UnbiasedPolicy(self)
            %%GETUNBIASEDPOLICY For every non-absorbing state (rows) we give equal probability to each action (columns)
            unbiased_policy = 1./self.numactions * ~self.absorbing'*ones(1,self.numactions);
        end

        % check what proportion of a list of test actions match the optimal policy (one of the optimal policies)
        function [ portion ] = portionoptimalpolicy(self,testactions)
            matches = 0;
            for s=1:self.numstates
                if self.isoptimalaction(s,testactions(s))
                    matches = matches + 1;
                end
            end
            portion = matches/self.numstates;
        end
        % check whether list of test actions match the optimal policy (one of the optimal policies)
        function [ isoptimal ] = isoptimalpolicy(self,testactions)
            isoptimal = true;
            for s=1:self.numstates
                if ~self.isoptimalaction(s,testactions(s))
                    isoptimal = false;
                    break;
                end
            end
        end
        function [ isoptimal ] = isoptimalaction(self,s,a)
            if self.optimalactions{s} == 0
                % terminal states don't count
                isoptimal = true;
            else
                optactions = self.optimalactions{s};
                isoptimal = any(optactions == a);
            end
        end
    end % methods

end % classdef
