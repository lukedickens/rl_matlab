classdef NonStationarySim < handle & rl.environment.discrete.simulation.Simulation
%% An mdp simulation
    properties(Hidden=true)
        %epstep
    end
    methods
        function self = NonStationarySim(model,varargin)
            kwargs = rl.utils.dict(varargin{:});
            self = self@rl.environment.discrete.simulation.Simulation(model,kwargs);
        end % NonStationarySim
        
        function state = init(self)
            % Initialises state from initial distribution and then outputs this (as no hidden state)
            state = init@rl.environment.discrete.simulation.Simulation(self);
            %self.epstep = 1;
        end % init
        
        function [ reward, nextstate ] = next(self,action)
            %% takes:
            %%      action choice, action
            %% returns:
            %%      next state and reward setting self.state to new state
            dist = self.t(self.state,action,[1:self.numstates]',self.step); % weights over next state
            nextstate = find(mnrnd(1,dist),1); % next state drawn from initial multinomial parameters
            reward = self.r(self.state,action,nextstate,self.step); % reward is r(s,a,s',tk)
            self.state = nextstate; % current state is now the newly created one
            self.newstep(action,reward,nextstate);
            %self.epstep = self.epstep + 1;
        end % next

    end % methods

end % classdef
