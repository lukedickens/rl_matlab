function [ vertices ] = VisualiseGrid(model, labels, fontsize, fmt )
%%VISUALISEGRID Displays a graphical representation of a grid-world MDP
%%
%%  [ patches ] = VISUALISEGRID(model) shows the basic grid
%%
%%  [ patches ] = VISUALISEGRID(model,labels) shows labels(i) at each state i
%%
%%  [ patches ] = VISUALISEGRID(model,labels, fontsize) shows labels(i) at each state i, with specified fontsize
%%
%%  [ patches ] = VISUALISEGRID(model,labels, fontsize, fmt) shows labels(i) at each state i, with specified format and fontsize
%%
%%  In each case it returns an ordered list of the patch handles in the image

if nargin < 4
    % default format
    fmt = '%1.1f';
end
if nargin < 3
    % default fontsize
    fontsize = 12;
end
if nargin < 2
    % default empty labels
    labels = repmat(' ',model.numstates,1);
end

% if the labels are numeric we want to convert them to text
if isnumeric(labels)
    labels = num2str(labels,fmt);
end

% shape gives the span of the gridword
shape = model.shape;
% locs is the real states
locs = model.locs;
% absorbing is a vector showing whether states absorb
absorbing = model.absorbing;
% initial is a vector showing whether states are initial
initial = model.initial;

numrows = shape(1);
numcols = shape(2);
%delete(gcf);
h = figure(1);
bx = [ 0 1 1 0 ]';
by = [ 0 0 -1 -1 ]';
fill(numcols*bx,numrows*by,'k');
xs = [];
ys = [];
patches = [];
texthandles = [];
name = 1;
state = 1;
for loc = locs'
    r = loc(1);
    c = loc(2);
    label = labels(state,:);
    isabsorbing = absorbing(state);
    isinitial = (initial(state) ~= 0);
    if  isinitial
        colour = 'y';
    elseif isabsorbing
        if model.r(1,1,state) < mean(model.r(1,1,:))
            colour = 'r';
        else
            colour = 'g';
        end
    else
        colour = 'c';
    end
    y = by-r+1;
    x = bx+c-1;
    p = patch(x,y,colour);
    patches = [ patches p ];
    text(x(1)+0.2,y(1)-0.5,label,'FontSize',12);
    state = state+1;
end
axes_handle = gca;
set(axes_handle,'YTick',[]);
set(axes_handle,'XTick',[]);
axis tight;
daspect('manual');
daspect([2 2 1]);
figure_handle = gcf;
set(figure_handle,'OuterPosition',[0 0 100*numcols 100*numrows ]);
vertices.handle = h;
vertices.patches = patches;
end


