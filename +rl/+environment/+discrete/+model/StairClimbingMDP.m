function [ model ] = StairClimbingMDP()
%%STAIRCLIMBINGMDP Creates a model of the simple stair climbing MDP from the lectures

% States are:  {P <-- s1 <=> s2 <=> s3 <=> s4 <=> s5 --> G];
numstates = 7; 
statenames =  ['s1'; 's2'; 's3'; 's4'; 's5'; 's6'; 's7'];

% Actions are: {L,R} --> {1, 2 }
numactions = 2; 
actionnames =  ['L'; 'R'];

% Matrix indicating absorbing states
absorbing = [
%P  1   2   3   4   5   G   <-- STATES 
1   0   0   0   0   0   1
];

% Matrix indicating starting state distribution
initial = [
%P  1   2   3   4   5   G   <-- STATES 
0   0   0   1   0   0   0
];

% load transition
T = transition_matrix();

% load reward matrix
R = reward_matrix(numstates,numactions);

%% create the output struct
model.numstates = numstates; % number of states
model.statenames = statenames; % and their names
model.numactions = numactions; % number of actions
model.actionnames = actionnames; % and their names
model.initial = initial; % probability distribution over initial states
model.absorbing = absorbing; % which states terminate an episode
model.t = @(s,a,s_) T(s_,s,a); % the transition function - note the different order of the arguments.
model.r = @(s,a,s_) R(s_,s,a); % the reward function
model.T = T; % the transition probabilities in matrix form
model.R = R; % the rewards in matrix form

end
%--------------------------------------------------------------------------

% the transition subfunction
function prob = transition_function(prior_s, action, post_s) % reward function (defined locally)
T = transition_matrix()
prob = T(post_s,prior_s,action)
end

% get the transition matrix
function T = transition_matrix()
TL = [
%P  1   2   3   4   5   G   <-- FROM STATE
1   1   0   0   0   0   0 ; % P TO STATE
0   0   1   0   0   0   0 ; % 1
0   0   0   1   0   0   0 ; % 2    
0   0   0   0   1   0   0 ; % 3
0   0   0   0   0   1   0 ; % 4
0   0   0   0   0   0   0 ; % 5
0   0   0   0   0   0   1 ; % G
];
TR = [
%P  1   2   3   4   5   G   <-- FROM STATE
1   0   0   0   0   0   0 ; % P TO STATE
0   0   0   0   0   0   0 ; % 1
0   1   0   0   0   0   0 ; % 2    
0   0   1   0   0   0   0 ; % 3
0   0   0   1   0   0   0 ; % 4
0   0   0   0   1   0   0 ; % 5
0   0   0   0   0   1   1 ; % G
];
T = cat(3, TL, TR); %transition probabilities for each action 
end

%--------------------------------------------------------------------------

% the locally defined reward function
function rew = reward_function(prior_s, action, post_s) % reward function (defined locally)
if ((prior_s == 2) && (action == 1) && (post_s == 1))
    rew = -10.0;
elseif ((prior_s == 6) && (action == 2) && (post_s == 7))
    rew = 10.0;
elseif (action == 1)
    rew = 1.0;
else
    rew = -1.0;
end
end

% get the reward matrix
function R = reward_matrix(numstates, numactions)
% i.e. 11x11 matrix of rewards for being in state s, performing action a and ending in state s'
R = zeros(numstates, numstates, numactions); 
for i = 1:numstates
   for j = 1:numactions
      for k = 1:numstates
         R(k, i, j) = reward_function(i, j, k);
      end
   end    
end
end

