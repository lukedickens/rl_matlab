function [ model ] = BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc, varargin )
    kwargs = rl.utils.dict(varargin{:});
    %% BUILDGRIDWORLD takes an ad-hoc description of a grid world and returns a mdp or pomdp model
    %% build the grid
    obstacles = sortrows(obstacles);
    [ locs, neighbours  ] = get_topology(shape,obstacles);
    %
    %% how many states and actions are there
    numstates = length(locs);
    numactions = length(action_effects);
    %
    %% give the states and actions names
    magnitude = floor(log10(numstates))+1;
    format = ['%0' num2str(magnitude) 'd'];
    statenames = [repmat(['s';],numstates,1) num2str([1:numstates]',format)];
    actionnames = ['N'; 'E'; 'S'; 'W'];
    %
    %% absorbing_locs is a list of locations corresponding to absorbing states
    %% absorbing is a boolean array with 1s for all absorbing state indices
    absorbing_locs = sortrows(absorbing_locs);
    absorbing_states = find(ismember(locs,absorbing_locs,'rows'));
    absorbing = zeros(1,numstates);
    absorbing(absorbing_states) = 1;
    %
    %% initial is a list of weights proportional to the probability of the initial state having that index
    sorted = sortrows([initial_desc.locs initial_desc.weights]);
    initial_desc.locs = sorted(:,1:2);
    initial_desc.weights = sorted(:,3);
    % now assign weights to the initial distribution appropriately
    initial_states = find(ismember(locs,initial_desc.locs,'rows'));
    initial = zeros(1,numstates);
    initial(initial_states) = initial_desc.weights;
    %
    %% Notation:
    %%      prior_s = prior state
    %%      post_s = posterior state
    %
    %% build the transition matrix
    T = zeros(numstates,numstates,numactions);
    for action=1:numactions
        for effect=1:numactions
            outcome = mod(action+effect-1,numactions);
            if outcome == 0
                outcome = numactions;
            end % if
                prob = action_effects(effect);
            for prior_s=1:numstates
                post_s = neighbours(prior_s,outcome);
                T(post_s,prior_s,action) = T(post_s,prior_s,action) + prob;
            end % for
        end % for
    end % for
    %
    %% build the reward matrix
    R = reward_desc.default*ones(numstates,numstates,numactions);
    if length(reward_desc.special_locs) ~= 0
        % if there are special locations
        % make sure special locs (and their values) are sorted correctly
        sorted = sortrows([reward_desc.special_locs reward_desc.special_vals]);
        reward_desc.special_locs = sorted(:,1:2);
        reward_desc.special_vals = sorted(:,3);
        % get the state index of each special location
        special_states = find(ismember(locs,reward_desc.special_locs,'rows'));
        % update the reward matrix so that all transitions ending in the appropriate state
        % have the right reward (reward only depends on posterior state).
        % NOTE: There is redundancy in this matrix (some transitions never occur) we 
        % do not care what reward value these transitions are given
        R(special_states,:,:) = repmat(reward_desc.special_vals,[1,numstates,numactions]);
    end % if
    %
    %% create the output struct
    model.shape = shape;
    model.locs = locs; % the mapping from states to locs
    model.absorbing = absorbing; % which states terminate an episode
    model.initial = initial/sum(initial); % probability distribution over initial states
    model.numstates = numstates; % number of states
    model.statenames = statenames; % and their names
    model.numactions = numactions; % number of actions
    model.actionnames = actionnames; % and their names
    model.t = @(s,a,s_) T(s_,s,a); % the transition function
    model.r = @(s,a,s_) R(s_,s,a); % the reward function
    model.T = T; % the transition probabilities in matrix form
    model.R = R; % the rewards in matrix form
    model.neighbours = neighbours;

    %% the final input argument relates to the pomdp part, observation_scheme
    %TODO turn into dictionary
    if kwargs.iskey('obstype')
        obstype = kwargs('obstype');
%%        if strcmp('surround',obstype) % all surrounding cells (inc. diagonals)
        if strcmp('blocks',obstype) % just those cells blocking movement NESW
            %%obinttobits = @(i)([mod(floor((i-1)/8),2),mod(floor((i-1)/4),2),mod(floor((i-1)/2),2),mod(i-1,2) ])
            obbitstoint = @(b)(b(:,1)*8+b(:,2)*4+b(:,3)*2+b(:,4)+1); % maps from rows of indices (neighbour matches) to integer rep
            potential_obs = 17;
        elseif strcmp('horz',obstype) % just cells east or west
            obbitstoint = @(b)(b(:,2)*2+b(:,4)+1);
            potential_obs = 5;
        elseif strcmp('vert',obstype) % just cells north or south
            obbitstoint = @(b)(b(:,1)*2+b(:,3)+1);
            potential_obs = 5;
        elseif strcmp('min',obstype) % no observation other than normal and terminal
            obbitstoint = @(b)(ones(length(b(:,1)),1));
            potential_obs = 2;
        else
            % do nothing, i.e. keep it as a fully observable
        end % if
        obbits = (neighbours == repmat([1:numstates]',1,numactions)); % gets the ob bits for each state (walls)
        obints = obbitstoint(obbits); % get the integer reps of obs at each state (column vec)
        obints(find(absorbing)) = potential_obs; % set the terminal ob for each absorbing state
        % for performance we count only those observations that exist.
        obcounts = sum(repmat(obints,1,potential_obs)==repmat([1:potential_obs],numstates,1),1);
        intstoindices = find(obcounts); % i.e. those whose counts are greater than 0
        % then we must map the integer representations to the compressed indices
        model.num_obs = length(intstoindices);
        statestoobs = zeros(numstates,1);
        for s = [1:numstates]
            statestoobs(s) = find(intstoindices == obints(s));
        end
        % for the general observation function we take the arc s,a,s_ but
        % only the final state s_ counts
        model.obfunc = @(s,a,s_,o)(double(statestoobs(s_)==o));
        model.statestoobs = statestoobs;
        magnitude = floor(log10(model.num_obs))+1;
        format = ['%0' num2str(magnitude) 'd'];
        obsnames = [repmat(['o';],model.num_obs,1) num2str([1:model.num_obs]',format)];
        model.obsnames = obsnames;
        model.type = 'pomdp';
    else
        model.type = 'mdp';
    end

end % BuildGridWorld

%--------------------------------------------------------------------------

function [ x, y, v] = unpack(row)
    x=row(1);
    y=row(2);
    v=row(3);
end % unpack

function [ locs,neighbours ] = get_topology(shape,obstacles)
    % obstacles is a list of locations corresponding to locations that are not valid states
    height = shape(1);
    width = shape(2);
    locs = [];
    neighbour_locs = [];
    index = 1;
    for i=1:height
        for j=1:width
            loc = [i,j];
            if is_location(loc,shape,obstacles)
                locs = [ locs ; loc ];
                these_neighbours = [];
                these_neighbours = [ these_neighbours ; get_neighbour(loc,'nr',shape,obstacles) ];
                these_neighbours = [ these_neighbours ; get_neighbour(loc,'ea',shape,obstacles) ];
                these_neighbours = [ these_neighbours ; get_neighbour(loc,'so',shape,obstacles) ];
                these_neighbours = [ these_neighbours ; get_neighbour(loc,'we',shape,obstacles) ];
                neighbour_locs = cat(3, neighbour_locs , these_neighbours );
            else
                continue
            end % if
        end % for
    end % for

    % translate neighbour lists from locations to states
    numstates = length(locs(:,1));
    neighbours = zeros(numstates,4);
    for s=1:numstates
        for dir=1:4
            % find neighbour location
            nloc = neighbour_locs(dir,:,s);
            % turn location into a state number
            nstate = find(ismember(locs,nloc,'rows'));
            % insert into neighbour matrix
            neighbours(s,dir) = nstate;
        end
    end
end

function newloc = get_neighbour(loc,dir,shape,obstacles)
  i = loc(1);
  j = loc(2);
  nr = [i-1,j];
  ea = [i,j+1];
  so = [i+1,j];
  we = [i,j-1];
  if ( isequal(dir,'nr') & is_location(nr,shape,obstacles) )
    newloc = nr;
  elseif ( isequal(dir,'ea') & is_location(ea,shape,obstacles) )
    newloc = ea;
  elseif ( isequal(dir,'so') & is_location(so,shape,obstacles) )
    newloc = so;
  elseif ( isequal(dir,'we') & is_location(we,shape,obstacles) )
    newloc = we;
  else 
    % default is to return same location
    newloc = loc;
  end % if
end % get_neighbour

function res = is_location(loc,shape,obstacles)
  if ( loc(1)<1 | loc(2)<1 | loc(1)>shape(1) | loc(2)>shape(2) )
    res = false;
  elseif ismember(loc,obstacles,'rows')
    res = false;
  else
    res = true;
  end % if
end % is_location


%--------------------------------------------------------------------------

