function [ model ] = GridWorld4(varargin)
%%GRIDWORLD4 the cliff grid world
kwargs = rl.utils.dict(varargin{:});
%
shape = [4,12];
action_effects = [ 1, 0, 0, 0];
obstacles = [];
absorbing_locs = [[4,2];[4,3];[4,4];[4,5];[4,6];[4,7];[4,8];[4,9];[4,10];[4,11];[4,12];];
%special_rewards = [[4,2,-100];[4,3,-100];[4,4,-100];[4,5,-100];[4,6,-100];[4,7,-100];[4,8,-100];[4,9,-100];[4,10,-100];[4,11,-100];]
reward_desc.default = -1;
reward_desc.special_locs = [4*ones(10,1) [2:11]'];
reward_desc.special_vals = -100*ones(length(reward_desc.special_locs),1);
initial_desc.locs = [[4,1];];
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = rl.environment.discrete.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc,kwargs);
end

%--------------------------------------------------------------------------
