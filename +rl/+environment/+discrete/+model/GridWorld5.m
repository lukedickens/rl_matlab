function [ model ] = GridWorld5(varargin)
% A maze from Joe Roop (Joseph.Roop@asdl.gatech.edu) found at:
% http://ccl.northwestern.edu/netlogo/models/community/Reinforcement%20Learning%20Maze
kwargs = rl.utils.dict(varargin{:});
shape = [11,11];
action_effects = [ 1, 0, 0, 0];
obstacles = [];
penalty_locs =  [ ones(11,1) [1:11]'; [2:10]' ones(9,1); [2:10]' 11*ones(9,1); 11*ones(11,1) [1:11]'; 7*ones(4,1) [2:5]'; 7*ones(4,1) [7:10]'; [4,5];[5,5];[5,6];[5,7]; [9,3];[9,4]; ];
goal_locs = [[2,10];];
absorbing_locs =  [penalty_locs;goal_locs];
reward_desc.default = 0;
reward_desc.special_locs = absorbing_locs;
reward_desc.special_vals = [-10*ones(length(penalty_locs),1); 10];
initial_desc.locs = [[10,2];];
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = rl.environment.discrete.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc,kwargs);
end

%--------------------------------------------------------------------------
