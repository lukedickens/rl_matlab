function [ model ] = GridWorld7(varargin)

kwargs = rl.utils.dict(varargin{:});
shape = [5,5];
action_effects = [ 1, 0, 0, 0];
obstacles = [[2,2];[3,2];[3,4];[4,4]];
penalty_locs = [];
goal_locs = [[3,3]];
absorbing_locs = [goal_locs;penalty_locs];
reward_desc.default = 0;
reward_desc.special_locs = absorbing_locs;
reward_desc.special_vals = [10];
initial_desc.locs = [[[1:5]' ones(5,1)];[1,2];[4,2];[5,2];[1,3];[2,3];[4,3];[5,3];[1,4];[2,4];[5,4];[[1:5]' 5*ones(5,1)]];
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = rl.environment.discrete.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc,kwargs);
end

%--------------------------------------------------------------------------
