function [ model ] = GridWorld6(varargin)

kwargs = rl.utils.dict(varargin{:});

shape = [7,18];
action_effects = [ 1, 0, 0, 0];
obstacles = [[[1:6]' 16*ones(6,1)]];
penalty_locs = [4*ones(12,1) [1:12]'];
goal_locs = [[5,1];[1,18];];
absorbing_locs = [goal_locs;penalty_locs];
reward_desc.default = -1;
reward_desc.special_locs = penalty_locs;
reward_desc.special_vals = [-100*ones(length(penalty_locs),1)];
initial_desc.locs = [[3,1];];
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = rl.environment.discrete.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc,kwargs);
end

%--------------------------------------------------------------------------
