function [ model ] = HowaiMDP(varargin)
%MDPHOWAI - as used in Nikhil Howai and Holly Phillips' (?) paper on human epsilon/exploration levels
kwargs = rl.utils.dict(varargin{:});

% States are:
%   P   P   P   P
%   P   4   S   P
%   P   G   2   P
%   P   P   P   P
numstates = 5; 
statenames =  ['S'; '2'; 'G'; '4'; 'P'];

% Actions are: {L,R} --> {1, 2 }
numactions = 4; 
actionnames =  ['N'; 'E'; 'S'; 'W'];

% Matrix indicating absorbing states
absorbing = [
%S   2   G   4   P % <-- states
0   0   1   0   1
];

% Matrix indicating starting state distribution
initial = [
%S   2   3   G   P % <-- states
1   0   0   0   0
];

% load transition
forw = kwargs.get('forw',0.8);
T = transition_matrix(forw);

% load reward matrix
R = reward_matrix(numstates,numactions);

%% create the output struct
model.numstates = numstates; % number of states
model.statenames = statenames; % and their names
model.numactions = numactions; % number of actions
model.actionnames = actionnames; % and their names
model.initial = initial; % probability distribution over initial states
model.absorbing = absorbing; % which states terminate an episode
model.t = @(s,a,s_) T(s_,s,a); % the transition function - note the different order of the arguments.
model.r = @(s,a,s_) R(s_,s,a); % the reward function
model.T = T; % the transition probabilities in matrix form
model.R = R; % the rewards in matrix form

%                       S       2       G       4       P
model.optimalactions = {[3,4]   4       0       3       0};

end
%--------------------------------------------------------------------------

% the transition subfunction
function prob = transition_function(prior_s, action, post_s) % reward function (defined locally)
T = transition_matrix()
prob = T(post_s,prior_s,action)
end

% get the transition matrix
function T = transition_matrix(forw)
% forw is a value [0,1] that tells us what likelihood to move in the direction of chosen action.
side = (1-forw)/2;
T1 = [
%        S         2         G         4         P <-- from state
         0      forw         0      side         0  % S to state
         0         0      side         0         0  % 2
         0      side         0         0         0  % G
      side         0      forw         0         0  % 4
 forw+side      side      side forw+side    1.0000  % P
];
T2 = [
%        S         2         G         4         P <-- from state
         0      side         0      forw         0  % S to state
      side         0      forw         0         0  % 2
         0         0         0      side         0  % G
         0         0      side         0         0  % 4
 forw+side forw+side      side      side    1.0000  % P
];
T3 = [
%        S         2         G         4         P <-- from state
         0         0         0      side         0  % S to state
      forw         0      side         0         0  % 2
         0      side         0      forw         0  % G
      side         0         0         0         0  % 4
      side forw+side forw+side      side    1.0000  % P
];
T4 = [
%        S         2         G         4         P <-- from state
         0      side         0         0         0  % S to state
      side         0         0         0         0  % 2
         0      forw         0      side         0  % G
      forw         0      side         0         0  % 4
      side      side forw+side forw+side    1.0000  % P
];
T = cat(3, T1, T2, T3, T4); %transition probabilities for each action 
end

%--------------------------------------------------------------------------

% the locally defined reward function
function rew = reward_function(prior_s, action, post_s) % reward function (defined locally)
    if (post_s == 5)
        rew = -10.0;
    elseif (post_s == 3)
        rew = 10.0;
    else
        rew = 0;
    end
end

% get the reward matrix
function R = reward_matrix(numstates, numactions)
% i.e. 11x11 matrix of rewards for being in state s, performing action a and ending in state s'
R = zeros(numstates, numstates, numactions); 
for i = 1:numstates
   for j = 1:numactions
      for k = 1:numstates
         R(k, i, j) = reward_function(i, j, k);
      end
   end    
end
end

