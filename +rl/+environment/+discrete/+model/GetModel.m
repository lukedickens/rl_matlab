function [ model, fullmodeltype ] = GetModel(modeltype,varargin)
    import rl.environment.discrete.model.*
    kwargs = rl.utils.dict(varargin{:});
    obstype = kwargs.get('obstype','');
    fullmodeltype = [modeltype obstype];
    switch logical(true)
    case strcmpi(modeltype,'gw1')
        model = GridWorld1(kwargs);
    case strcmpi(modeltype,'gw2')
        model = GridWorld2(kwargs);
    case strcmpi(modeltype,'gw3')
        model = GridWorld3(kwargs);
    case strcmpi(modeltype,'gw4')
        model = GridWorld4(kwargs);
    case strcmpi(modeltype,'gw5')
        model = GridWorld5(kwargs);
    case strcmpi(modeltype,'gw6')
        model = GridWorld6(kwargs);
    case strcmpi(modeltype,'gw7')
        model = GridWorld7(kwargs);
    case strcmpi(modeltype,'randgrid')
        model = RandomGridWorld(kwargs);
    end
end

