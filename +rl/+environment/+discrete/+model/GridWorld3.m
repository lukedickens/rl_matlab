function [ model ] = GridWorld3(varargin)
% A more complex gridworld

kwargs = rl.utils.dict(varargin{:});
%
shape = [5,5];
action_effects = [ 0.8, 0.1, 0.0, 0.1];
obstacles = [[2,2];[3,2];[3,4]];
absorbing_locs = [[5,1];[5,2];[5,3];[5,4];[5,5];[3,5];];
reward_desc.default = 0;
reward_desc.special_locs = [[3,3];[3,5];[5,1];[5,2];[5,3];[5,4];[5,5]];
reward_desc.special_vals = [[1  ];[10 ];[-10];[-10];[-10];[-10];[-10]];
initial_desc.locs = [[4,1];];
initial_desc.weights = ones(size(initial_desc.locs,1),1);

%
[ model ] = rl.environment.discrete.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc,kwargs);

%                       1       2       3       4       5
%                       6               7       8       9
%                       10              11              12
%                       13      14      15      16      17
%                       18      19      20      21      22
model.optimalactions = {2       2       3       3       3   ...
                        1               2       2       3   ...
                        1               1               0   ...
                        1       2       1       2       1   ...
                        0       0       0       0       0   };

end

%--------------------------------------------------------------------------
