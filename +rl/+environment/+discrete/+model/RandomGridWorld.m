function [ model ] = RandomGridWorld(varargin)
%%RANDOMGRIDWORLD(extent,numpenalties,numobstacles)
%%  creates a random square grid world with extent X extent size
%%  randomly fills this with a number of penalty blocks of cells equal to numpenalties
%%  then randomly fills this with a number of obstacle blocks of cells equal to numobstacles
%%

kwargs = rl.utils.dict(varargin{:});
extent = kwargs.get('extent',5);
shape = kwargs.get('shape',[extent,extent]);
action_effects = kwargs.get('action_effects',[ 0.8, 0.1, 0, 0.1]);
maxlengthpenalty = kwargs.get('maxlengthpenalty',2);
minlengthpenalty = kwargs.get('minlengthpenalty',2);
maxlengthobstacle = kwargs.get('maxlengthobstacle',2);
minlengthobstacle = kwargs.get('minlengthobstacle',2);
% we want to fill approximately 1/3 of the space with obstacles or penalties
numpenalties = kwargs.get('numpenalties',floor(shape(1)*shape(2)/maxlengthpenalty/3/2));
numobstacles = kwargs.get('numobstacles',floor(shape(1)*shape(2)/maxlengthobstacle/3/2));
probvert = kwargs.get('probvert',0.5);

% a random maze
%
initial_desc.locs = [[extent,randint(1,1,[1,extent])];];
initial_desc.weights = ones(size(initial_desc.locs,1),1);

goal_locs =  [[1,randint(1,1,[1,extent])];];
avoiding = [goal_locs; initial_desc.locs; ];
[ penalty_locs, penalty_superblocks ]  =  RandomlyGenerateBlocks(extent,avoiding,numpenalties,probvert,minlengthpenalty,maxlengthpenalty);
absorbing_locs = [penalty_locs; goal_locs;];
reward_desc.default = 0;
reward_desc.special_locs = absorbing_locs;
reward_desc.special_vals = [-10*ones(length(penalty_locs),1);[10];];

avoiding = [avoiding; penalty_superblocks];
[ obstacles ] = RandomlyGenerateBlocks(extent,avoiding,numobstacles,probvert,minlengthobstacle,maxlengthobstacle);

%
[ model ] = BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc,kwargs);

end

function [ blocks, varargout ] = RandomlyGenerateBlocks(extent,avoiding,numblocks,probvert,minlength,maxlength)
% randomly construct blocks of non-contiguous states
blockcount = 0;
% so the function doesn't hang, we declare a timeout count
timeout = 1000;
totcount = 0;
% we don't want the blocks to overlap initial or terminating locations, or each other
superblocks = [];
blocks = [];
while (blockcount ~= numblocks) && (totcount ~= timeout)
    blocklength = randint(1,1,[minlength,maxlength]);
    adder1 = ones(blocklength+1,1);
    adder2 = [0:blocklength]';
%    superblock = repmat([randint(1,1,[[1,extent-blocklength-1]),randint(1,1,[1,extent-1]]),2*blocklength+2,1);
    % superblock is an Nx2 matrix, each row is a grid location [x y]
    % start by assigning a set of coords to a single random number between 1 and extent (forms the x coords)
    % and a random number between 1 and extent-blocklength+1 this is y coord of the first location. 
    superblock = repmat([randint(1,1,[1,extent]),randint(1,1,[1,extent-blocklength+1])],2*blocklength+2,1);
    % then increment the y coords of the first 'blocklength' pairs to represent counting up (adder2)
    superblock(1:1+blocklength,2) = superblock(1:1+blocklength,2) + adder2;
    % for the second set of coords, we increment the ys as before, and the xs universally by one
    superblock(2+blocklength:2*blocklength+2,:) = superblock(2+blocklength:2*blocklength+2,:) + [adder1 adder2];
    % with random prob
    if rand(1) < probvert
        % reflect block in x=y (switch x and y coords)
        superblock = superblock(:,2:-1:1);
    end
    % the subblock is the set of coordinates that corresponds to the real locations to be filled
    subblock = superblock(1:blocklength,:);
    %XXX this subblock line was added to squeeze more in but didn't work effectively
    %if ~any(ismember(superblocks,superblock,'rows')) && ~any(ismember(avoiding,subblock,'rows'))
    if ~any(ismember(superblocks,superblock,'rows')) && ~any(ismember(avoiding,superblock,'rows'))
        superblocks = [ superblocks; superblock;];
        blocks = [ blocks; subblock;];
        blockcount = blockcount + 1;
    end
    totcount = totcount+1;
end
varargout{1} = superblocks;
%
end

%--------------------------------------------------------------------------
