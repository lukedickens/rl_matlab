function [ model ] = GridWorld2(varargin)

kwargs = rl.utils.dict(varargin{:});
% The simple GridWorld1 by another method
%
shape = [4,4];
action_effects = [ 1, 0, 0, 0];
obstacles = [];
absorbing_locs = [[1,1];[4,4];];
reward_desc.default = -1;
reward_desc.special_locs = [];
reward_desc.special_vals = [];
initial_desc.locs = [ones(3,1) [2:4]';2*ones(4,1) [1:4]';3*ones(4,1) [1:4]';4*ones(3,1) [1:3]';];
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = rl.environment.discrete.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc,kwargs);

%                       1       2       3       4
%                       5       6       7       8
%                       9       10      11      12
%                       13      14      15      16
model.optimalactions = {0       4       4       [3,4]   ...
                        1       [1,4]   [1:4]   3       ...
                        1       [1:4]   [2,3]   3       ...
                        [1,2]   2       2       0 };
end

%--------------------------------------------------------------------------
