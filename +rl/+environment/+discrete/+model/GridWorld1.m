function [ model ] = GridWorld1(varargin)
kwargs = rl.utils.dict(varargin{:});
% The simple GridWorld1 by another method
%
shape = [3,4];
action_effects = [ 0.8, 0.1, 0.0, 0.1];
obstacles = [[2,2];];
absorbing_locs = [[1,4];[2,4];];
reward_desc.default = -1;
reward_desc.special_locs = [[1,4];[2,4]];
reward_desc.special_vals = [[10];[-100]];
initial_desc.locs = [[3,1];];
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = rl.environment.discrete.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc,kwargs);

%                       1       2       3       4
%                       5       #       6       7
%                       8       9       10      11
model.optimalactions = {2       2       2       0   ...
                        1               4       0   ...
                        1       4       4       3};
end

%--------------------------------------------------------------------------
