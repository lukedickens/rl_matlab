function [ model ] = GridWorldHowai(varargin)
%GRIDWORLDHOWAI - as used in Nikhil Howai and Holly Phillips' (?) paper on human epsilon/exploration levels
% but as a grid with 16 states. Rather than the abstracted 5 state version they use.

kwargs = rl.utils.dict(varargin{:});
%
shape = [4,4];
action_effects = kwargs.get('action_effects',[ 0.8, 0.1, 0.0, 0.1]);
obstacles = [];
penalties = [[repmat(1,4,1) [1:4]'];[2,1];[2,4];[3,1];[3,4];[repmat(4,4,1) [1:4]']];
goal = [3,2];
absorbing_locs = [penalties;goal];
reward_desc.default = 0;
reward_desc.special_locs = [absorbing_locs];
reward_desc.special_vals = [repmat(-10,size(penalties,1),1);10];
initial_desc.locs = [[2,3];];
initial_desc.weights = 1;
%
[ model ] = rl.environment.discrete.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc,kwargs);

%                       1       2       3       4
%                       5       6       7       8
%                       9       10      11      12
%                       13      14      15      16
model.optimalactions = {0       0       0       0   ...
                        0       3       [3,4]   0   ...
                        0       0       4       0};
end

%--------------------------------------------------------------------------
