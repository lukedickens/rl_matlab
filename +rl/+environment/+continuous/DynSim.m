classdef DynSim < handle
    properties (SetAccess = 'public')
        % behaviour
        dyn_fn
        cost_fn
        init_fn
		% state
        current_x
        xdim
		% control
        udim
		% time
        step
		total_steps
        T
        dt
    end % properties

    methods
        %% ----- CONSTRUCTOR
        function self = DynSim(T, dt, dyn_fn, cost_fn, init_fn, xdim, udim)
            self.init_fn = init_fn;
            self.dyn_fn = dyn_fn;
            self.cost_fn = cost_fn;
            self.T = T;
            self.dt = dt;
            self.total_steps = floor(T/dt);
            self.xdim = xdim;
            self.udim = udim;
        end

        %% -----Runge Kutta (obtain next x) 
        function [next_x] = rungeKutta(self, x, u, dt)
            [k1] = self.dyn_fn(x, u)*dt ;                  % x is a column vector, u is a scalar (F) CONTROL
            [k2] = self.dyn_fn(x+k1/2, u)*dt ; 
            [k3] = self.dyn_fn(x+k2/2, u)*dt ; 
            [k4] = self.dyn_fn(x+k3, u)*dt ; 
            next_x = x + 1/6*(k1+2*k2+2*k3+k4);
        end % rungeKutta
 
        %% needs to initialise x0
        function [ x0 ] = init(self)
            x0 = self.init_fn();
            self.current_x = x0;
            self.step = 1;
        end % init

        %% ----- next
        function [next_x, cost] = next(self, current_u)
            % ----- Runge Kutta
            next_x = self.rungeKutta(self.current_x, current_u);
            if sum(isnan(next_x))>0
                error('RLOC:NaNx','next_x contains NaN!') % 1st part is the name of the error, second is what gets printed on the screen
            end
            % ----- terminal cost
            cost = self.cost_fn(self.current_x,current_u,self.dt);
            self.current_x = next_x;
            % ----- update
            self.step = self.step + 1;
            if self.terminal()
               cost = cost + self.cost_fn(self.current_x,[],self.dt); %NB u is not used as its terminal cost, the empty u indicates a terminal cost.
            end
        end % next

        %% ----- keeps track if reached end of total_steps for Agent
        function [ finished ] = terminal(self)
            finished = (self.step == self.total_steps);
        end % terminal

    end % methods
end % DynSim
