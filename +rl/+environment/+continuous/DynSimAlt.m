classdef DynamicSystem < handle
    
    properties (SetAccess = 'public')
        choose_dyn % 0=dynamics1 (Topp), 1=dynamics2 (Yasini), 2=dynamics3 (Sturis)
        choose_example % even number=healthy parameters, odd number=unhealthy parameters
        init_x % initial values (glc0, ins0, beta0)
        current_x % glc, ins, beta valus
        additionalValues % only neccessary for dynamics 3
        
        current_ext % external parametes, food and activity
        
        % parameters, dynamics
        params
        
        numTotalSteps
        dyn
        dynODE45
    end % properties
    
    methods
        
        function dynSys = DynamicSystem(disSim)
            % choose dynamics and example initial data on the basis of choose_dyn
            dynSys.choose_dyn = disSim.choose_dyn;
            dynSys.choose_example = disSim.choose_example;
            if mod(dynSys.choose_dyn-1,4) == 0
                % parameters, dyn1
                [example] = dyn1Example(dynSys.choose_example);
                dynSys.params = example(1:1,4:12);
                % function handle, dyn1
                dynSys.dyn = @(current_x, current_u, dynSys) dyn1(current_x, current_u, dynSys.params, disSim.foodE.fct, disSim.actE.fct);
                % function handle, dyn1ODE45
                dynSys.dynODE45 = @(t, next_x) dyn1ODE45(next_x, dynSys.params, disSim.foodE.fct, disSim.actE.fct);
            elseif mod(dynSys.choose_dyn-1,4) == 1
                % parameters, dyn2
                [example] = dyn2Example(dynSys.choose_example);
                dynSys.params = example(1:1,4:11);
                % function handle, dyn2
                dynSys.dyn = @(current_x, current_u, dynSys) dyn2(current_x, current_u, dynSys.params, disSim.foodE.fct, disSim.actE.fct);
                % function handle, dyn2ODE45
                dynSys.dynODE45 = @(t, next_x) dyn2ODE45(next_x, dynSys.params, disSim.foodE.fct, disSim.actE.fct);
            elseif mod(dynSys.choose_dyn-1,4) == 2
                % parameters, dyn3
                [example] = dyn3Example(dynSys.choose_example);
                dynSys.params = example(1:1,4:23);
                dynSys.additionalValues = example(1:1,24:26);
                % function handle, dyn3
                dynSys.dyn = @(current_x, current_u, dynSys) dyn3(current_x, current_u, dynSys.params, disSim.foodE.fct, disSim.actE.fct);
                % function handle, dyn3ODE45
                dynSys.dynODE45 = @(t, next_x) dyn3ODE45(next_x, dynSys.params, disSim.foodE.fct, disSim.actE.fct);
            elseif mod(dynSys.choose_dyn-1,4) == 3
                % parameters, dyn3
                [example] = dyn4Example(dynSys.choose_example);
                dynSys.params = example(1:1,4:16);
                dynSys.additionalValues = example(1:1,17:19);
                % function handle, dyn3
                dynSys.dyn = @(current_x, current_u, dynSys) dyn4(current_x, current_u, dynSys.params, disSim.foodE.fct, disSim.actE.fct);
                % function handle, dyn3ODE45
                dynSys.dynODE45 = @(t,next_x) dyn4ODE45(next_x, dynSys.params, disSim.foodE.fct, disSim.actE.fct);
            end
            dynSys.init_x = example(1,1:3);
            dynSys.numTotalSteps = disSim.numTotalSteps;
        end % function - DynamicSystem
        
        
        % called at the beginning to initialise the state (glc0, ins0,
        % beta0)
        function [current_x] = init(dynSys)
            dynSys.current_x = dynSys.init_x;
            current_x = dynSys.current_x;
        end % function - init
        
        
        % takes current control u and updates state
        function [current_x, current_xODE45] = next(dynSys, disSim, currentStep)
            % built current_x, if dyn3 or dyn4 additional values required
            dynSys.current_x = disSim.current_x;
            if mod(dynSys.choose_dyn-1,4) == 2 || mod(dynSys.choose_dyn-1,4) == 3
                dynSys.current_x(4:6) = dynSys.additionalValues;
            end
         
            % rungeKutta at every timeStep currentStep*dtODE45
            [current_x] = rungeKutta(dynSys, dynSys.current_x, disSim.current_u, disSim.dt);           
            
            % ode45 for the timesteps from (currentStep*dtODE45)+1 until ((currentStep+1)*dtODE45)-1
            if currentStep == 2
                tspan = [( (currentStep+1)*disSim.dt : disSim.dt : (((currentStep-1)*disSim.dtODE45)*disSim.dt) )];
            else
                tspan = [( (((currentStep-2)*disSim.dtODE45)+2)*disSim.dt : disSim.dt : (((currentStep-1)*disSim.dtODE45)*disSim.dt) )];
            end

            [t,current_xODE45] = ode45(dynSys.dynODE45, tspan, current_x);
            
            if any(isnan(current_x))
                error('DynamicSystem:Out of Bounds','next_x contains NaN!');
            end

        end % function - next
        
        
        % to update in discrete time steps, using infinitesimal dynamics
        % rungeKutta
        function [next_x] = rungeKutta(dynSys, current_x, current_u, dt)  
            [k1] = dynSys.dyn(current_x, current_u, dynSys);
            [k2] = dynSys.dyn(current_x+1/2*k1'*dt, current_u, dynSys);
            [k3] = dynSys.dyn(current_x+1/2*k2'*dt, current_u, dynSys);
            [k4] = dynSys.dyn(current_x+k3'*dt, current_u, dynSys);
            next_x = current_x + dt*1/6*(k1'+2*k2'+2*k3'+k4');
        end % function - rungeKutta
        
        
    end % methods
    
end % DynamicSystem
