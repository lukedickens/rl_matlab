classdef Discretifier < handle
    
    properties (SetAccess = 'public')
        dynsim

        getU
        getS
        
        init_x % initial values (glc0, ins0, beta0)
        current_x % glc, ins, beta value
        current_s % state
        current_a % action
        current_c % cost
        current_u % control
        current_act % activity
        history % stores everything
        
        % objects
        dynSys % DynamicSystem
        agent % agent
        costFunction % costFunction
        foodE % food event
        actE % activity event
        
    end % properties
    
    methods
        function disSim = Discretifier(choose_dyn, choose_example, choose_distr, choose_costFct)
            disSim.choose_dyn = choose_dyn;
            disSim.choose_example = choose_example;
            disSim.choose_distr = choose_distr;
            disSim.choose_costFct = choose_costFct;
            numdays = 1;
            if disSim.choose_dyn == 1
                disSim.simTotalTime = 1/24/60*1440*numdays; % [d]
                disSim.dt = 1/24/60; % [min]
                disSim.dtODE45 = 30; % [min], e.g. 480 for 8 hours between getting the states of the dynamics, 4 times a day
                disSim.numTotalSteps = floor(floor(disSim.simTotalTime/disSim.dt)/disSim.dtODE45);
                disSim.getU = @(current_a) disSim.mappingAtoU1(current_a);
            elseif disSim.choose_dyn == 2 
                disSim.simTotalTime = numdays*1440; % [min], e.g. 720 for 12 hours 
                disSim.dt = 1/60; % [s], 1/60/10 100ms
                disSim.dtODE45 = 60*240; % [s] e.g. 900 for every 15 minutes, 96 times/day
                disSim.numTotalSteps = floor(floor(disSim.simTotalTime/disSim.dt)/disSim.dtODE45);
                disSim.getU = @(current_a) disSim.mappingAtoU234(current_a);
            elseif disSim.choose_dyn == 3
                disSim.simTotalTime = numdays*1440; % [min], e.g. 720 for 12 hours 
                disSim.dt = 1/60; % [s], 1/60/10 100ms
                disSim.dtODE45 = 60*240; % [s] e.g. 900 for every 15 minutes, 96 times/day
                disSim.numTotalSteps = floor(floor(disSim.simTotalTime/disSim.dt)/disSim.dtODE45);
                disSim.getU = @(current_a) disSim.mappingAtoU234(current_a);
            elseif disSim.choose_dyn == 4
                disSim.simTotalTime = 300; % [min], e.g. 720 for 12 hours 
                disSim.dt = 1/60/1000; % [ms], 1/60/10 1000ms
                disSim.dtODE45 = 60000*10; % [s] e.g. 900 for every 15 minutes, 96 times/day
                disSim.numTotalSteps = floor(floor(disSim.simTotalTime/disSim.dt)/disSim.dtODE45);
                disSim.getU = @(current_a) disSim.mappingAtoU234(current_a);
            end
            
            % creates an objects of DynamicSystem
            disSim.dynSys = DynamicSystem(disSim);
            
            % creates an object of agent with several predefined parameters
            agentype = 'qlearn'; % 'sarsa' or 'qlearn'
            numstates = 10;
            numactions = 10;
            alpha =  0.80; % learning rate, what extent the new information will override the old information, how much weight to the just experienced reward
            epsilon = 0.20; % defines the greedy behaviour of the agent
            gamma = 0.90; % discount rate, determines the importance of future rewards, gamma=0 considering only current rewards, gamme-->1 strive for a long-term high reward
            temperature = 100; % temperature (tau), for softmax policy, high temperature-->actions are nearly equiprobable, low temperature-->greater difference in selection probability
            policytype = 'softmaxSA'; % 'egreedy' or 'softmax' or 'softmaxSA'
            totalSteps = disSim.numTotalSteps;
            disSim.agent = rl.agents.discrete.get(agentype,'numstates',numstates,'numactions',numactions,'totalSteps',totalSteps,'gamma',gamma,'alpha',alpha,'epsilon',epsilon,'policytype',policytype,'temperature',temperature);
            
            % creates an object of cost function
            disSim.costFunction = costFct(disSim.choose_costFct);
            
            % creates an object of food event
            disSim.foodE = FoodEvent(disSim.choose_dyn, disSim.choose_distr);
            
            % creates an object of activity event
            disSim.actE = ActivityEvent(disSim.choose_dyn);
            
        end % function - Discretifier
        

        % run the agent multiple times
        function [ ] = run_multiple(disSim, numepisodes)
            % train agent on multiple runs of the passed in simulation.
            for i=1:numepisodes
                run(disSim);
            end % for each episode
        end % function - run_multiple
        
        
        % run the agent once
        function [] = run(disSim)
            currentStep = 1;
            % runs a single episode of the passed in simulation.
            [disSim] = init(disSim, currentStep); % get initial state
            
            for currentStep=2:disSim.numTotalSteps+1
                % gets next state and reward from disSim
                [disSim] = next(disSim, currentStep);
                if currentStep < disSim.numTotalSteps+1
                    disSim.current_a = disSim.agent.next(disSim.current_c, disSim.current_s);
                    %disSim.history.aValues(currentStep) = disSim.current_a;
                    % mapping action a to control u
                    disSim.current_u = disSim.getU(disSim.current_a);
                    %disSim.history.uValues(currentStep) = disSim.current_u;
                else
                  %  disSim.agent.last(disSim.agent, disSim.current_s);
                end              
            end
        end % function - run
        
        
        % function to get the first glc, ins, beta-cell mass values, first state and first action
        function [disSim] = init(disSim, currentStep)
            % get first three current_x values (eg. glucose, insulin,
            % beta-cell mass) and current_c value (cost) while calling the
            % init function of DynamicSystem
            [disSim.current_x] = disSim.dynSys.init();
            disSim.init_x = disSim.current_x;
            %disSim.history.glcValues(currentStep) = disSim.current_x(1);
            %disSim.history.insValues(currentStep) = disSim.current_x(2);
            %disSim.history.currentStep(currentStep) = currentStep;
            
            % first mappingXtoS to get the (initial) state s of the system
            disSim.current_s = disSim.mappingXtoS();
            %disSim.history.sValues(currentStep) = disSim.current_s;
            
            % first call of the function first of the agent to get the first action a
            disSim.current_a = first(disSim.agent, disSim.current_s);
            %disSim.history.aValues(currentStep) = disSim.current_a;
            
            % mapping action a to control u
            disSim.current_u = disSim.getU(disSim.current_a);
            %disSim.history.uValues(currentStep) = disSim.current_u;
        end % function - init
        
        
        % function to get glc, ins, beta-cell mass values, cost, state
        % until currentStep = numTotalSteps-1
        function [disSim] = next(disSim, currentStep) 
            %disSim.history.currentStep(currentStep) = currentStep;
           
            % gets next current_x
            [disSim.current_x, current_xODE45] = disSim.dynSys.next(disSim, currentStep);

            if currentStep == 2 
                %disSim.history.glcValues(currentStep) = disSim.current_x(1);           
                %disSim.history.insValues(currentStep) = disSim.current_x(2);
                %disSim.history.betaValues(currentStep) = disSim.current_x(3);
                %disSim.history.glcValues( (currentStep+1) : ((currentStep-1)*disSim.dtODE45) ) = current_xODE45(1:length(current_xODE45),1);
                %disSim.history.insValues( (currentStep+1) : ((currentStep-1)*disSim.dtODE45) ) = current_xODE45(1:length(current_xODE45),2);
                %disSim.history.betaValues( (currentStep+1) : ((currentStep-1)*disSim.dtODE45) ) = current_xODE45(1:length(current_xODE45),3);
            else
                %disSim.history.glcValues(((currentStep-2)*disSim.dtODE45)+1) = disSim.current_x(1);           
                %disSim.history.insValues(((currentStep-2)*disSim.dtODE45)+1) = disSim.current_x(2);
                %disSim.history.betaValues(((currentStep-2)*disSim.dtODE45)+1) = disSim.current_x(3);
                %disSim.history.glcValues( (((currentStep-2)*disSim.dtODE45)+2) : ((currentStep-1)*disSim.dtODE45) ) = current_xODE45(1:length(current_xODE45),1);
                %disSim.history.insValues( (((currentStep-2)*disSim.dtODE45)+2) : ((currentStep-1)*disSim.dtODE45) ) = current_xODE45(1:length(current_xODE45),2);
                %disSim.history.betaValues( (((currentStep-2)*disSim.dtODE45)+2) : ((currentStep-1)*disSim.dtODE45) ) = current_xODE45(1:length(current_xODE45),3);
            end
      
            disSim.current_x = current_xODE45(length(current_xODE45),1:3);
           
            % gets next current_c
            current_glc = disSim.current_x(1);    
            [disSim.current_c] = disSim.costFunction.fct(current_glc);
            disSim.history.cValues(currentStep) = disSim.current_c;
            
            % mapping glc, ins, beta values to get state s
            disSim.current_s = disSim.mappingXtoS();
            %disSim.history.sValues(currentStep) = disSim.current_s; 
            %XXX suggested alternative
            %disSim.history.sValues(end+1) = disSim.current_s;
        end % function - next
        
        
        
        function [] = last(disSim, currentStep)
            %disSim.history.aValues(currentStep-1) = disSim.current_a;
            disSim.current_a = disSim.agent.last(disSim.current_c, disSim.current_s);
            %disSim.history.aValues(currentStep) = disSim.current_a;
            
            % mapping action a to control u
            disSim.current_u = disSim.getU();
            %disSim.history.uValues(currentStep) = disSim.current_u;
        end
        
        
        % mapping glucose value x to state s
        function [current_s] = mappingXtoS(disSim)
            current_glc = disSim.current_x(1);
            if current_glc >= 210
                current_s = 1;
            elseif current_glc < 210 && current_glc >= 190
                current_s = 2;
            elseif current_glc < 190 && current_glc >= 170
                current_s = 3;
            elseif current_glc < 170 && current_glc >= 150
                current_s = 4;
            elseif current_glc < 150 && current_glc >= 130
                current_s = 5;
            elseif current_glc < 130 && current_glc >= 110
                current_s = 6;
            elseif current_glc < 110 && current_glc >= 90
                current_s = 7;
            elseif current_glc < 90 && current_glc >= 70
                current_s = 8;
            elseif current_glc < 70 && current_glc >= 50
                current_s = 9;
            elseif current_glc < 50
                current_s = 10;
            else
                error('Glucose value is out of range','glucose value is not possible!');
            end
        end % function - mappingXtoS
        

        % mapping action a to control u
        function [current_u] = mappingAtoU1(disSim, current_a)
            if current_a == 1
                current_u = 0;
            elseif current_a == 2
                current_u = 1.6487;
            elseif current_a == 3
                current_u = 4.4817;
            elseif current_a == 4
                current_u = 7.3891;
            elseif current_a == 5
                current_u = 12;
            elseif current_a == 6
                current_u = 20.0855;
            elseif current_a == 7
                current_u = 33.1155;
            elseif current_a == 8
                current_u = 90.0171;
            elseif current_a == 9
                current_u = 148.4132;
            elseif current_a == 10
                current_u = 244.6919;
            else
                current_u = 0;
            end
            current_u = min(10,current_u);
        end % function - mappingAtoU
        
        
        % mapping action a to control u
        function [current_u] = mappingAtoU234(disSim, current_a)
            if disSim.current_a == 1
                current_u = 0;
            elseif current_a == 2
                current_u = 0.25;
            elseif current_a == 3
                current_u = 0.5;
            elseif current_a == 4
                current_u = 0.75;
            elseif current_a == 5
                current_u = 1;
            elseif current_a == 6
                current_u = 1.25;
            elseif current_a == 7
                current_u = 1.5;
            elseif current_a == 8
                current_u = 1.75;
            elseif current_a == 9
                current_u = 2;
            elseif current_a == 10
                current_u = 2.25;
            else
                current_u = 0;
            end
            current_u = min(10,current_u);
        end % function - mappingAtoU
        

        function []  = reset(disSim)
            disSim.history = [];
            % if you are counting steps you need to reset that too.
        end
        
    end % methods
    
end % Discretifier
