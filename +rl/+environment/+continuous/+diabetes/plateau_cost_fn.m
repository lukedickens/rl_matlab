function cost = plateau_cost_fn(x, u, dt)
% Plateau cost function (as developed by Luke).
    glc = x(1);
    sigmasqrd1 = 40;
    sigmasqrd2 = 160;
    if ~isfinite(glc)
        error('Glucose value is not finite');
    end
    lthresh = 70;
    uthresh = 110;
    if glc <= lthresh
        x = glc - lthresh;
        cost = 1 - exp(-x.^2/2/sigmasqrd1);
    elseif glc > uthresh
        x = glc - uthresh;
        cost = 1 - exp(-x.^2/2/sigmasqrd2);
    else
        cost = 0;
    end
end

