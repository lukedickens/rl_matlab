function [x0] = yasini_init_fn(params)
% Initial state function for diabetes system from Yasini et al 2009.
% x0 : is the initial state of the system (see below)
    x0(1) = 70; % glucose [mg dl^-1]
    x0(2) = 0; % plasma insulin concentration [microUnits ml^-1]
    x0(3) = 0; % insulin variable for the remote compartment [min^-1]
end
