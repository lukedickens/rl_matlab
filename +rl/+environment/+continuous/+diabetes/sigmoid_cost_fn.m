function cost = sigmoid_cost_fn(x, u)
% Sigmoid cost function (as developed by Katrin).
    glc = x(1);
    if glc <= 90
        x = glc - 80;
        cost = -1./ (1+exp(x * + 0.5)/0.06);
    elseif glc > 90
        x = glc - 100;
        cost = -1./ (1+exp(x * - 0.3)/0.006);
    else % catches NaN errors
        error('no valid glucose value')
    end   
end

