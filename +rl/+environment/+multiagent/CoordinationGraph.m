classdef CoordinationGraph < handle & rl.environment.discrete.simulation.Base
%% An decentralised simulation for two agents
    properties (SetAccess='public')
        numagents
        numactionsper
        edges
        numedges
        payoffs
    end
    
    methods
        function self = CoordinationGraph(varargin)
            kwargs = rl.utils.dict(varargin{:});
            self = self@rl.environment.discrete.simulation.Base(kwargs.get('torecord',[0,0,1]));
            self.numagents = kwargs.get('numagents',4);
            self.numactionsper = kwargs.get('numactionsper',5);
            self.numedges = kwargs.get('numedges',floor((self.numagents^2-self.numagents)/2*0.5));
            self.initialise_graph();
            self.populate_rewards();
        end % Simulation

        function [] = initialise_graph(self,varargin)
            kwargs = rl.utils.dict(varargin{:});
            self.edges = zeros(self.numagents);
            graphtype = kwargs.get('graphtype','kok');
            switch graphtype
            case 'kok'
                for g=1:self.numedges
                    edgecounts = sum(self.edges);
                    minedgecount = min(edgecounts);
                    inserted =false;
                    while ~inserted
                        [v leastconnected] = find(edgecounts == minedgecount);
                        if length(leastconnected) >= 2
                            newedge = randsample(leastconnected,2);
                        else
                            nextleastconnected = find(edgecounts == minedgecount +1);
                            newedge = [leastconnected randsample(nextleastconnected,1)];
                        end
                        if self.edges(newedge(1),newedge(2)) == 0
                            self.edges(newedge(1),newedge(2)) = 1;
                            self.edges(newedge(2),newedge(1)) = 1;
                            inserted = true;
                        end
                    end
                end
            otherwise
                error(['Unrecognised graphtype: ' graphtype]);
            end
        end

        function [ ] = populate_rewards(self)
            self.payoffs = cell(self.numagents);
            for i=1:self.numagents-1
                for j=i+1:self.numagents
                    if self.edges(i,j)
                        self.payoffs{i,j} = self.create_coord_reward_func(self.numactionsper);
                    end
                end
            end
        end

        function [ s ] = init(self)
            s = ones(1,self.numagents);
        end
        function [ r, s ] = next(self,a)
            s = ones(1,self.numagents);
            rtot = 0;
            for i=1:self.numagents
                for j=find(self.edges(i,:))
                    if i < j
                        rtot = rtot + self.payoffs{i,j}(a(i),a(j));
                    else
                    end
                end
            end
            r = rtot*ones(1,self.numagents);
        end
    end % methods

    methods(Static)
        function [ allrewards ] = create_coord_reward_func(numactions)
            coordinating_action1 = randi(numactions);
            coordinating_action2 = randi(numactions);
            allrewards = 10*rand(numactions);
            allrewards(coordinating_action1,:) = 0;
            allrewards(:,coordinating_action2) = 0;
            allrewards(coordinating_action1,coordinating_action2) = 10*rand(1)+5;
        end
    end
end % classdef
