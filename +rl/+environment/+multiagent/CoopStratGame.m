classdef CoopStratGame < handle & rl.environment.discrete.simulation.Base
%% An decentralised simulation for two agents
    properties (SetAccess='public')
        gametype
        payofffns
        numagents
        numactionslist % list of number of actions one value per agent
        numstateslist
    end
    
    methods
        function self = CoopStratGame(varargin)
            kwargs = rl.utils.dict(varargin{:});
            self = self@rl.environment.discrete.simulation.Base(kwargs.get('torecord',[0,0,1]));
            if kwargs.iskey('payofffns')
                self.payofffns = kwargs('payofffns');
            elseif kwargs.iskey('payoffvals')
                payoffvals = kwargs('payoffvals');
                if kwargs.iskey('payoffstds')
                    payoffstds = kwargs('payoffstds');
                    self.payofffns = self.build_rand_payoffs(payoffvals,payoffstds);
                else
                    self.payofffns = self.build_payoffs(payoffvals);
                end % if - 
            else
                error('No way to define payofffns');
            end % for - 
            self.numagents = ndims(self.payofffns);
            self.numactionslist = size(self.payofffns);
            self.numstateslist = ones(1,self.numagents);
            self.gametype = kwargs.get('gametype','none');
        end % Simulation

        function [ payofffns ] = build_rand_payoffs(self,payoffvals,payoffstds)
            n = numel(payoffvals);
            ns = size(payoffvals);
            if n ~= numel(payoffstds)
                error('Wrong size payoffstds');
            end
            listpayoffvals = reshape(payoffvals,[1,n]);
            listpayoffstds = reshape(payoffstds,[1,n]);
            for i=1:n
                v = listpayoffvals(i);
                s = listpayoffstds(i);
                listpayofffns{i} = @() v+s*randn(1);
            end
            payofffns = reshape(listpayofffns,ns);
        end 

        function [ payofffns ] = build_payoffs(self,payoffvals)
            n = numel(payoffvals);
            ns = size(payoffvals);
            listpayoffvals = reshape(payoffvals,[1,n]);
            for i=1:n
                v = listpayoffvals(i);
                listpayofffns{i} = @() v;
            end
            payofffns = reshape(listpayofffns,ns);
        end 
        
        function [ jointstate ] = init(self)
            jointstate = repmat(1,1,self.numagents);
            self.newepisode(jointstate(1));
        end

        function [ jointreward, nextjointstate ] = next(self,jointaction)
            %% takes:
            %%      action choice, action
            %% returns:
            %%      next state and reward setting sim.state to new state
    	    ind = num2cell(jointaction);
            fn = self.payofffns{ind{:}};
            jointreward = repmat(fn(),1,self.numagents);
            nextjointstate = repmat(1,1,self.numagents);
            self.newstep(ind,jointreward(1),nextjointstate(1));
        end % next

        function answer = terminal(self)
            answer = false;
        end % init

        function [ result ] = isoptimalpolicy(self,testactions)
            switch self.gametype
                case 'coord'
                    % make into vector from cell
                    testactions = [testactions{1} testactions{2}];
                    result = ( all(testactions == [1 1]) | all(testactions == [3 3]) );
                case {'climbing','climbing-rand','climbing-rand2'}
                    % make into vector from cell
                    testactions = [testactions{1} testactions{2}];
                    result = all(testactions == [1 1]);
                otherwise
                    error('Unrecognised game type: Cannot test actions');
            end
        end

    end % methods

    methods(Static)
        function [ game ] = GetGame(gametype,varargin)
            kwargs = rl.utils.dict(varargin{:});
            switch gametype
                case 'coord'
                    numagents = kwargs.get('numagents',2);
                    k = kwargs.get('k',-100);
                    if numagents == 2
                        game = rl.environment.multiagent.CoopStratGame('payoffvals',[[10 0 k];[0 2 0];[k 0 10]],'gametype',gametype);
                    elseif numagents == 3
                        payoffvals = zeros(3,3,3);
                        payoffvals(1,1,1) = 10;
                        payoffvals(2,2,2) = 2;
                        payoffvals(3,3,3) = 10;
                        payoffvals(1,1,3) = k;
                        payoffvals(1,3,1) = k;
                        payoffvals(3,1,1) = k;
                        payoffvals(1,3,3) = k;
                        payoffvals(3,3,1) = k;
                        payoffvals(3,1,3) = k;
                        game = rl.environment.multiagent.CoopStratGame('payoffvals',payoffvals);
                    end
                case 'climbing'
                    % claus and boutilier
                    % lauer and ?
                    k = kwargs.get('k',-30);
                    game = rl.environment.multiagent.CoopStratGame('payoffvals',[[11 k 0];[k 7 6];[0 0 5]],'gametype',gametype);
                case 'climbing-rand'
                    % kapetenakis
                    k = kwargs.get('k',-30);
                    game = rl.environment.multiagent.CoopStratGame('payoffvals',[[11 k 0];[k 7 6];[0 0 5]],'gametype',gametype);
                    game.payofffns{2,2} = @() 14*(rand(1) < 0.5);
                case 'climbing-rand2'
                    % kapetenakis 2
                    k = kwargs.get('k',-30);
                    payofffns = cell(3,3);
                    payofffns{1,1} = @() 10+2*(rand(1) < 0.5);
                    payofffns{1,2} = @() 5+(k-5)*(rand(1) < 0.5);
                    payofffns{1,3} = @() 8-16*(rand(1) < 0.5);
                    payofffns{2,1} = @() 5+(k-5)*(rand(1) < 0.5);
                    payofffns{2,2} = @() 14*(rand(1) < 0.5);
                    payofffns{2,3} = @() 12*(rand(1) < 0.5);
                    payofffns{3,1} = @() 5-10*(rand(1) < 0.5);
                    payofffns{3,2} = @() 5-10*(rand(1) < 0.5);
                    payofffns{3,3} = @() 10*(rand(1) < 0.5);
                    game = rl.environment.multiagent.CoopStratGame('payofffns',payofffns,'gametype',gametype);
                otherwise
                    error('Unrecognised gametype');
            end
        end
    end % methods - Static
end % classdef
