classdef DECSimMarshall < handle
    %% A decentralised simulation for two (or more) agents
    properties (SetAccess='public')
        decsim % the DECSim simulation object
        agents % the agents
        agentargs % the arguments shared by all agents (for setting agents)
        agenttypes % the types of agent
        numagents
        schedtype % whether we suppress updates for various agents during particular timesteps.
        %XXX Now a method suppress_who % determines which agent to suppress at each timestep
        last_suppress % last suppress for randbatch scheduling
        batchsize % batchsize for randbatch and batch scheduling
        weights % weights for rand and randbatch scheduling
        totalsteps % keep track of total number of steps, for scheduling
        episode % keep track of episode for scheduling
        % output parameters
        verbose
        dbid
        eid
        gid
    end % properties
    
    methods
        function self = DECSimMarshall(decsim,kwargs)
            % databases
            self.verbose = true;
            dbfile = kwargs.get('dbfile','DECSimMarshall.db');
            self.dbid = mksqlite('open', dbfile);
            self.create_tables();
            self.register_group();
            % creating the object
            self.decsim = decsim;
            if nargin == 1
                kwargs = dict;
            end
            self.numagents = self.decsim.numagents;
            % should record sim type perhaps with sim.decsim.type()
%            if kwargs.iskey('numagents')
%                self.numagents = kwargs('numagents');
%            else
%                self.numagents = 2;
%                warning(['Guessing number of agents as ' num2str(self.numagents)]);
%            end
            self.record_groupoption('numagents',num2str(self.numagents));
            self.agentargs = kwargs.get('agentargs',rl.utils.dict());
            keys = self.agentargs.keys();
            for k=1:length(keys)
                key = keys{k};
                value = self.agentargs(keys{k});
                if ischar(value)
                    % do nothing
                elseif isscalar(value)
                    value = num2str(value);
                elseif ismatrix(value)
                    value = mat2str(value);
                else
                    error('Unsupported type for database');
                end
                display(['DECSimMarshall agentargs setting:' key ', ' value ]);
                self.record_groupoption(['agentargs:' keys{k}],value);
            end
            if kwargs.iskey('agenttypes')
                self.agenttypes = kwargs('agenttypes');
            else
                for g=1:self.numagents
                    self.agenttypes{g} = 'Sarsa';
                end
            end
            for g=1:self.numagents
                self.record_groupoption([num2str(g) ':agenttype'],self.agenttypes{g});
            end
            %TODO remove these (start by setting errors we can set static args for the agents
            if kwargs.iskey('gamma')
                warning('should now be defined in agentargs');
                self.agentargs('gamma') = kwargs('gamma');
                self.record_groupoption('agentargs:gamma',num2str(self.agentargs('gamma')));
            end
            if kwargs.iskey('epsilon')
                warning('should now be defined in agentargs');
                self.agentargs('epsilon') = kwargs('epsilon');
                self.record_groupoption('agentargs:epsilon',num2str(self.agentargs('epsilon')));
            end
            if kwargs.iskey('alpha')
                warning('should now be defined in agentargs');
                self.agentargs('alpha') = kwargs('alpha');
                self.record_groupoption('agentargs:alpha',num2str(self.agentargs('alpha')));
            end
            if kwargs.iskey('lambda')
                warning('should now be defined in agentargs');
                self.agentargs('lambda') = kwargs('lambda');
                self.record_groupoption('agentargs:lambda',num2str(self.agentargs('lambda')));
            end
            if kwargs.iskey('eta')
                warning('should now be defined in agentargs');
                self.agentargs('eta') = kwargs('eta');
                self.record_groupoption('agentargs:eta',num2str(self.agentargs('eta')));
            end
            if kwargs.iskey('utilfunc')
                warning('should now be defined in agentargs');
                self.agentargs('utilfunc') = kwargs('utilfunc');
                self.record_groupoption('agentargs:utilfunc',func2str(self.agentargs('utilfunc')));
            end
            if kwargs.iskey('utiltype')
                warning('should now be defined in agentargs');
                self.agentargs('utiltype') = kwargs('utiltype');
                self.record_groupoption('agentargs:utiltype',self.agentargs('utiltype'));
            end
            % agents can be passed in too
            if kwargs.iskey('agents')
                self.agents = kwargs('agents');
            end
            % scheduling
            self.schedtype = kwargs.get('schedtype','sync');
            self.record_groupoption('schedtype',self.schedtype);
            switch self.schedtype
                case 'batch'
                    self.batchsize = kwargs.get('batchsize',10);
                    self.record_groupoption('batchsize',num2str(self.batchsize));
                case 'randbatch'
                    self.batchsize = kwargs.get('batchsize',10);
                    self.record_groupoption('avebatchsize',num2str(self.batchsize));
                    self.weights = kwargs.get('suppressweights',ones(1,self.numagents)/self.numagents);
                    self.record_groupoption('weights',mat2str(self.weights));
                    self.last_suppress = [];
                case 'rand'
                    self.weights = kwargs.get('suppressweights',ones(1,self.numagents)/self.numagents);
                    self.record_groupoption('weights',mat2str(self.weights));
            end
        end % Simulation

        function [] = reset(self)
            self.totalsteps = 1;
            self.episode = 1;
        end % reset function

        function [] = run(self,maxsteps)

            % initialise enviroment
            jointstate = self.decsim.init();

            % initialise agents and get first action
            for i=1:self.numagents
                jointaction(i) = self.agents{i}.first(jointstate(i));
            end % for - init each agent

            % act on environment
            [ jointreward, jointstate ] = self.decsim.next(jointaction);
            for i=1:maxsteps
                suppress = self.suppress_who(self.numagents,self.totalsteps,self.episode);
                % get agents' next action and potential update
                for g=1:self.numagents
                    jointaction(g) = self.agents{g}.next(jointreward(g),jointstate(g),suppress(g));
                end
                % perform joint action and get next joint reward and state
                [ jointreward, jointstate ] = self.decsim.next(jointaction);
                if self.decsim.terminal()
                    break
                end
                self.totalsteps = self.totalsteps + 1;
            end % while

            % the last update
            suppress = self.suppress_who(self.numagents,self.totalsteps,self.episode);
            for g=1:self.numagents
                self.agents{g}.last(jointreward(g),jointstate(g),suppress(g));
            end % for
            self.episode = self.episode + 1;
        end % method - run

        function [ ] = run_multiple( self, maxsteps, numepisodes )
%%            if self.verbose 
%%                display(['Entered run_multiple with maxsteps = ' num2str(maxsteps) ' and numepisodes = ' num2str(numepisodes) ] );
%%            end
            % train agent on multiple runs of the passed in simulation.
            for i=1:numepisodes
                self.run( maxsteps );
            end % for each episode
        end % method - run_multiple

        function [ results ] =  parameter_search(self, paramnames, params, paramsteps, bounds, maxsteps, numepisodes, cycles, numtrials, statportion)
            %DECSIMMARSHALL/PARAMETER_SEARCH trial and find best parameters.
            % paramnames is cell of cells (one per agent). Each subcell is cell of parameter names.
            % params = starting parameter values
            % paramsteps = initial step for each parameter value
            % bounds = lower/upper bounds on each parameter value
            % cycles = passes through each parameter for the parameter searcher (optional)
            % numtrials = number of times to run test per parameter choice (optional)
            % statportion = what proportion of the training episodes to take as performance
            %     measure (accumreturn), counting back from the last episode (optional).
            if nargin < 8
                statportion = 0.5;
                if nargin < 7
                    numtrials = 8;
                    if nargin < 6
                        cycles = 2;
                    end
                end
            end % if - argin management
            searcher = ParameterSearcher(paramnames, params, paramsteps, bounds, cycles);
            [ params, terminate ] = searcher.suggest_params(); % get initial suggestion
            while ~terminate
                self.display_params(paramnames,params);
                self.set_agents(paramnames,params); % create agents based on parameters
                self.register_experiment(self.gid);
                self.record_params(paramnames,params);
                self.record_option('maxsteps',num2str(maxsteps));
                self.record_option('numepisodes',num2str(numepisodes));
                self.record_option('cycles',num2str(cycles));
                self.record_option('numtrials',num2str(numtrials));
                self.record_option('statportion',num2str(statportion));
                for l=1:numtrials
                    self.run_multiple(maxsteps,numepisodes);
                    [ partnumeps , accumreturn, expreturn, stderr ] = self.decsim.stats(statportion);
                    scores(l) = accumreturn;
                    self.record_score(l,accumreturn,stderr);
                    self.decsim.purge();
                    for g=1:self.numagents
                        self.agents{g}.reset();
                    end
                end
                [score,err] = searcher.post_scores(scores); % post the scores it returns a mean and error.
                [ params, terminate ] = searcher.suggest_params(); % get initial suggestion
            end % while - not terminated
                [ results.params ] = searcher.suggest_params(); % get initial suggestion
            
        end % function - parameter_search

        function [] = display_params(self,paramnames,params)
            s = '';
            pindex = 1;
            for g=1:self.numagents
                theseparamnames = paramnames{g};
                for i=1:length(theseparamnames)
                    s = [s theseparamnames{i} num2str(g) ': ' num2str(params(pindex)) ', '];
                    pindex = pindex + 1;
                end % for each parameter name for this agent
            end % for each agent
            display(s);
        end % function - display_params

        % setter/getter functions
        function [] = set_agents(self,paramnames,params)
            if self.numagents ~= length(paramnames)
                error('Length of paramnames suggests a different number of agents than are supported by DECSimMarshall');
            end
            remainingparams = params;
            for g=1:self.numagents
                theseparamnames = paramnames{g};
                numparams = length(theseparamnames);
                theseparams = num2cell(remainingparams(1:numparams));
                if self.agentargs.iskey('utiltemp')
                    error('Should not be in this dictionary')
                end
                kwargs = rl.utils.dict(theseparamnames,theseparams) + self.agentargs;
                kwargs('numstates') = self.decsim.numstateslist(g);
                kwargs('numactions') = self.decsim.numactionslist(g);
                remainingparams = remainingparams(numparams+1:length(remainingparams));
                self.agents{g} = rl.agents.discrete.get(self.agenttypes{g},kwargs);
            end % for - setting each agent
        end % function - set_agents

        function [] = reset_agents(self)
            for g=1:self.numagents
                self.agents{g}.reset();
            end % for - setting each agent
        end

        function [] = reset_all(self)
            self.reset_agents();
            self.decsim.purge();
        end

        function [ optpols ] = getpolicies(self)
            for g=1:self.numagents
                optpols{g}  = self.agents{g}.GetGreedyPolicy();
            end % for - setting each agent
        end

        function [ domjointactions ] = getdominantjointactions(self)
            for g=1:self.numagents
                [vs is]  = max(self.agents{g}.greedy_policy(),[],2);
                domjointactions{g} = is;
            end % for - setting each agent
        end

        function [ domactions ] = getdominantactions(self)
            domjointactions = self.getdominantjointactions();
            for s=1:self.decsim.sim.numstates
                for g=1:self.numagents
                    thisjointaction(g) = domjointactions{g}(s);
                end
                domactions(s) = self.decsim.invamap(thisjointaction);
            end
        end

        function [ isoptimal, portion ] = testdominantactions(self)
            testactions = self.getdominantactions();
            isoptimal = self.decsim.sim.isoptimalpolicy(testactions);
            if nargout > 1
                portion = self.decsim.sim.portionoptimalpolicy(testactions);
            end
        end

        % sql functions (XXX push to new class maybe)
        function [] = record_params(self,paramnames,params)
            remainingparams = params;
            for g=1:self.numagents
                theseparamnames = paramnames{g};
                numparams = length(theseparamnames);
                theseparams = remainingparams(1:numparams);
                remainingparams = remainingparams(numparams+1:length(remainingparams));
                for i=1:numparams
                    self.record_option([num2str(g) ':' theseparamnames{i}],num2str(theseparams(i)) );
                end % for - each agent param
            end % for - each agent
        end % function - record_params

        function [] = record_groupoption(self,key,value)
            mksqlite(self.dbid,['INSERT INTO groupoptions(gid,key,value) VALUES(' num2str(self.gid) ',''' key ''',''' value ''')']);
        end % function

        function [] = record_option(self,key,value)
            mksqlite(self.dbid,['INSERT INTO options(eid,key,value) VALUES(' num2str(self.eid) ',''' key ''',''' value ''')']);
        end % function

        function [] = record_score(self,trial,score,err)
            query = ['INSERT OR REPLACE INTO scores(eid,trial,score,err) VALUES(' num2str(self.eid) ',' num2str(trial) ',' num2str(score) ',' num2str(err) ')'];
            fprintf([query '\n']);
            mksqlite(self.dbid,query);
        end % function

        function [] = create_tables(self)
            mksqlite(self.dbid,'CREATE TABLE IF NOT EXISTS groups(gid INTEGER PRIMARY KEY,timestamp DATETIME, UNIQUE(timestamp))');
            mksqlite(self.dbid,'CREATE TABLE IF NOT EXISTS groupoptions(gid INTEGER, key TEXT, value TEXT, UNIQUE(gid, key))');
            mksqlite(self.dbid,'CREATE TABLE IF NOT EXISTS experiments(eid INTEGER PRIMARY KEY, gid INTEGER, timestamp DATETIME, UNIQUE(timestamp))');
            mksqlite(self.dbid,'CREATE TABLE IF NOT EXISTS options(eid INTEGER, key TEXT, value TEXT, UNIQUE(eid, key))');
            mksqlite(self.dbid,'CREATE TABLE IF NOT EXISTS scores(eid INTEGER, trial INTEGER, score FLOAT, err FLOAT, UNIQUE(eid, trial))');
        end % function

        function [gid,xdt] = register_group(self)
            xdt = rl.utils.timestamp();
            mksqlite(self.dbid,['INSERT INTO groups(timestamp) VALUES(''' xdt ''')']);
            result = mksqlite(self.dbid,['SELECT gid FROM groups WHERE timestamp = ''' xdt '''']);
            gid = result.gid;
            self.gid = gid;
        end % function

        function [eid,xdt] = register_experiment(self,gid)
            if nargin < 2
                gid = self.gid;
            end
            xdt = rl.utils.timestamp();
            mksqlite(self.dbid,['INSERT INTO experiments(gid,timestamp) VALUES(' num2str(gid) ',''' xdt ''')']);
            result = mksqlite(self.dbid,['SELECT eid FROM experiments WHERE timestamp = ''' xdt '''']);
            eid = result.eid;
            self.eid = eid;
        end % function

        function  [ suppress ] = suppress_who(self,numagents,timesteps,episodes)
            % now set the suppress_who func for scheduling
            switch self.schedtype
                case 'sync'
                    suppress = self.suppress_who_sync(numagents,timesteps,episodes);
                case 'inter'
                    suppress = self.suppress_who_inter(numagents,timesteps,episodes);
                case 'batch'
                    suppress = self.suppress_who_batch(numagents,timesteps,episodes,self.batchsize);
                case 'randbatch'
                    % sets the approximate batch size. Hazard func of 1/batchsize
                    if isempty(self.last_suppress) | (rand(1) < 1/self.batchsize)
                        suppress = self.suppress_who_rand(numagents,timesteps,episodes,self.weights);
                        self.last_suppress = suppress;
                    else
                        suppress = self.last_suppress;
                    end
                case 'rand'
                    suppress = self.suppress_who_rand(numagents,timesteps,episodes,self.weights);
                otherwise
                    error('Unsupported schedtype');
            end
        end

    end % methods

    methods(Static)
        function [ suppress ] = suppress_who_sync(numagents,timesteps,episodes)
            % all agents learn simultaneously
            suppress = repmat(false,1,numagents);
        end
        function [ suppress ] = suppress_who_inter(numagents,timesteps,episodes)
            % cycle through agents to determine which not to suppress
            suppress = repmat(true,1,numagents);
            suppress(mod(timesteps,numagents)+1) = false;
        end
        function [ suppress ] = suppress_who_batch(numagents,timesteps,episodes,batchsize)
            % cycle through agents to determine which not to suppress, but in batches
            suppress = repmat(true,1,numagents);
            suppress(mod(floor(timesteps/batchsize),numagents)+1) = false;
        end
        function [ suppress ] = suppress_who_rand(numagents,timesteps,episodes,weights)
            % determine randomly which agent not to suppress.
            suppress = repmat(true,1,numagents);
            suppress(find(mnrnd(1,weights),1)) = false;
        end
    end % methods - static
end % classdef

