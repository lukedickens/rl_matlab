classdef DECSim < handle
%% An decentralised simulation for two agents
    properties (SetAccess='public')
        sim % the single agent simulation that we want access to
        smap
        dualstates %XXX not used
        %numstates
        invamap
        %dualactions %XXX not used now using invamap directly
        numstateslist % list of number of states one value per agent
        numactionslist % list of number of actions one value per agent
        numagents
        statenames
        actionnames
    end
    properties (SetAccess='private',GetAccess='protected')
        model % the model
        state % the current state
    end
    
    methods
        function self = DECSim(sim,kwargs)
            %% The constructor method
            self.sim = sim;
            self.smap = kwargs('smap');
            self.dualstates = self.smap([1:sim.numstates]');
            %self.numstates = max(self.dualstates);
            self.invamap = kwargs('invamap');
            %self.dualactions = self.amap([1:sim.numactions]'); %XXX replaced by using invamap directly
            self.numstateslist = max(self.dualstates);
            %self.numactionslist = max(self.dualactions);
            self.numactionslist = kwargs('numactionslist');
            self.numagents = length(self.numstateslist);
            self.statenames = self.sim.statenames; % TODO make this more human readable
            self.actionnames = self.sim.actionnames; % TODO make this more human readable
        end % Simulation
        
        function jointstate = init(self)
            % Initialises state from initial distribution and then outputs this (as no hidden state)
            stateindex = self.sim.init();
            jointstate = self.smap(stateindex);
        end % init

%%XXX not used. Only need to convert from stateindex to jointobs/jointstate (done by smap)
        function [ index ] = stateindex(self,jointstate)
            % find the index of the first joint state to match input
            for index=1:self.dualstates
                if all(self.dualstates(j,:) == jointstate)
                    break;
                end
            end
        end %

%%XXX not used. Now pass in invamap directly (replaces actionindex)
%%        function [ index ] = actionindex(self,jointaction)
%%            % find the index of the first joint action to match input
%%            for index=1:length(self.dualactions)
%%                if all(self.dualactions(index,:) == jointaction)
%%                    break;
%%                end
%%            end
%%        end %

        function [ jointreward, nextjointstate ] = next(self,jointaction)
            %% takes:
            %%      action choice, action
            %% returns:
            %%      next state and reward setting sim.state to new state
            actionindex = self.invamap(jointaction);
            %display(['joint action: ' mat2str(jointaction) ', action: ' num2str(actionindex) ])
            [ reward, nextstateindex ] = self.sim.next(actionindex);
            nextjointstate = self.smap(nextstateindex);
            %display(['joint state: ' mat2str(nextjointstate) ', state: ' num2str(nextstateindex) ])
            jointreward = repmat([reward],1,size(nextjointstate,2));
        end % next

        function answer = terminal(self)
            answer = self.sim.terminal();
        end % init

        function [unbiased_policies] = UnbiasedPolicies(sim)
            %%GETUNBIASEDPOLICY For every non-absorbing state (rows) we give equal probability to each action (columns)
            for i=1:self.numagents
                unbiased_policies{i} = repmat(1./self.numactionslist(i),self.numstateslist(i),self.numactionslist(i));
            end % for - each policy constructed
        end

        function [ numepisodes, accumreturn, expreturn, stderr] = stats(self,portion)
            [ numepisodes, accumreturn, expreturn, stderr ] = self.sim.stats(portion);
        end % stats

        function purge(self)
            % remove all records
            self.sim.purge();
        end
    end % methods

    methods(Static)
        function [ self ] = GetDECSim(varargin)
            kwargs = rl.utils.dict(varargin{:});
            model = GetModel('modeltype',kwargs('modeltype'));
            kwargs.rmkey('modeltype');
            sim = Simulation(model);
            if ~kwargs.iskey('smap')
                kwargs('smap') = @(x)([x x ]);
                kwargs('invamap') = @(ja) 2*(ja(:,1)-1) + ja(:,2); % -1 + 1; unnecessary addition and subtraction
                kwargs('numactionslist') = [2 2];
            end
            self = DECSim(sim,kwargs);

        end
    end
end % classdef
