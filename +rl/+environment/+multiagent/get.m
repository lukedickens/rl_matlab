function [ sim ] = get(maintype,subtype, varargin)
% Get a multiagent simulation 
    import rl.environment.multiagent.*
    maintype = lower(maintype);
    subtype = lower(subtype);
    kwargs = rl.utils.dict(varargin{:});
    switch maintype
    case 'coord-graph'
        error('Not yet implemented');
    case 'coop-strat'
        CoopStratGame.GetGame(subtype,kwargs);
    case 'decsim'
        error('Not yet implemented');
    otherwise
        error(['Unrecognised main simulation type: ' maintype]);
    end
end

