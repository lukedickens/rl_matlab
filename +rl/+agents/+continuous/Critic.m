classdef Critic < rl.agents.base.Agent & handle
    properties (SetAccess='public')
        numstates
        numactions
        totsteps
        V
        absorbing
        % optional properties (may not be used)
        gamma % geometric discount
        epsilon % function takes time, returns probability of non-dominant action  (for egreedy policy)
        temperature % function takes time, returns boltzman temperature (for softmax policy)
        alpha % function takes time, returns learning step-size
    end % properties - public
    properties (SetAccess='protected',GetAccess='protected')
        mindelta % can set upper limit on size of td-error
        maxdelta % can set lower limit on size of td-error
    end % properties - protected
    
    methods
        function self = Critic(numstates,numactions,varargin)
            self = self@rl.agents.base.Agent();
            self.totsteps = 0;
            self.numstates = numstates;
            self.numactions = numactions;
            kwargs = rl.utils.dict(varargin{:});
            % initialise properties
            if kwargs.iskey('absorbing')
                % specified input values for absorbing
                self.absorbing = kwargs('absorbing');
            else
                self.absorbing = zeros(1,self.numstates);
            end % if - absorbing defined
            if kwargs.iskey('V')
                % specified input values for Q
                self.V = kwargs('V');
            else
                % default state-action value function is zero everywhere
                self.V = zeros(self.numstates, 1);
            end % if - V defined
            % set optional properties
            self.gamma = kwargs.get('gamma',[]);
            if kwargs.iskey('alpha')
                alpha = kwargs('alpha');
                if isa(alpha,'function_handle')
                    self.alpha = alpha;
                elseif isnumeric(alpha)
                    self.alpha = @(i)(alpha);
                else
                    error('Inappropriate type of alpha passed');
                end
            end
            % policy
            policytype = kwargs.get('policytype','egreedy');
            policyargs = rl.utils.dict();
            if kwargs.iskey('policy')
                policyargs('weights') = kwargs('policy');
            end % if - policy predefined
            %XXX replaced with Policy.get class method
%%            switch policytype
%%            case 'softmax'
%%                policyargs('temperature') = kwargs.get('temperature',1);
%%                self.policy = rl.agents.discrete.SoftMaxPolicy(self.numstates,self.numactions,policyargs);
%%            case 'egreedy'
%%                policyargs('epsilon') = kwargs.get('epsilon',0.1);
%%                self.policy = rl.agents.discrete.eGreedyPolicy(self.numstates,self.numactions,policyargs);
%%            case 'fixed'
%%                % input argument 'policy' sets the weights.
%%                self.policy = rl.agents.discrete.FixedPolicy(policyargs('policy'));
%%            case 'none'
%%                self.policy = rl.agents.discrete.DummyPolicy();
%%            otherwise
%%                error('Unrecognised policy type');
%%            end
            self.policy = rl.agents.discrete.Policy.get(policytype,self.numstates,self.numactions,policyargs);
            % td error type methods can require maxima/minima on the delta
            if kwargs.iskey('maxdelta')
                self.maxdelta = kwargs('maxdelta');
            else
                self.maxdelta = inf; 
            end % if 
            if kwargs.iskey('mindelta')
                self.mindelta = kwargs('mindelta');
            else
                self.mindelta = -inf; 
            end % if 
        end % function - Critic

        % Q tables
        function [] = reset(self)
            % reset the self to zero learning.
            self.totsteps = 0;
            self.policy.unbiased(find(~self.absorbing));
            self.V = zeros(self.numstates, 1);
        end % function - reset
        
    end % methods
end % classdef
