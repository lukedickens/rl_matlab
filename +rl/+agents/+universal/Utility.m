classdef Utility < handle
    properties (SetAccess='public')
        subagent
        utilfunc
    end % properties - protected, protected
    
    methods
        function self = Utility(subagent,utilfunc)
            self.subagent = subagent;
            self.utilfunc = utilfunc; % utility function
        end % function - Utility

        function [ current_a ] = first(self,first_s)
            current_a = self.subagent.first(first_s);
        end % function - first

        function [ current_a ] = next( self, current_r, current_s, suppress )
            utility = self.utilfunc(current_r);
            current_a = self.subagent.next(utility,current_s,suppress);
        end % function - next

        function [ ] = last(self, current_r, current_s, suppress )
            utility = self.utilfunc(current_r);
            self.subagent.last(utility, current_s, suppress );
        end % function - last

        function [] = reset(self)
            self.subagent.reset();
        end % function - reset

        % the full greedy policy
        function [ greedyPolicy ]  = greedy_policy(self)
            greedyPolicy = self.subagent.greedy_policy();
        end

        function [ logprob ] = imitate_run(self,trace)
            logprob = self.subagent.imitate_run(trace);
        end % function - imitaterun

        function [ logprob ] = imitate_run_multiple( self, traces )
            logprob = self.subagent.imitate_run_multiple( traces );
        end % function - imitate_run_multiple


    end % methods
end % classdef
