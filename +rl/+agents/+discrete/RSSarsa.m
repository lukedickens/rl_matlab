classdef RSSarsa < rl.agents.discrete.Critic & handle
%% RSSarsa - the sarsa version of the risk sensitive function defined in:
%% O. Mihatsch and R. Neuneier. Risk-Sensitive Reinforcement Learning. Mach. Learn., 49(2-3), 2002.
    properties (SetAccess='public')
        kappa
    end % properties - protected, protected
    
    methods
        function self = RSSarsa(numstates,numactions,gamma,alpha,kappa,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            kwargs('gamma') = gamma;
            kwargs('alpha') = alpha;
            self = self@rl.agents.discrete.Critic(numstates,numactions,kwargs);
            if isa(kappa,'function_handle')
                self.kappa = kappa;
            elseif isnumeric(kappa)
                self.kappa = @(i)(kappa);
            else
                error('Inappropriate type of kappa passed');
            end
        end % function - RSSarsa

        function [ tderror ] = update( self, current_r, current_s, current_a, suppress )
            if ~suppress % suppress prevents a new update from applying
                self.totsteps = self.totsteps + 1;
                % for efficiency we only want to read this value once.
                priorQ = self.Q(self.prior_s,self.prior_a);
                % Now update the Q table and the policy
                % the temporal difference update
                tderror = max(self.mindelta,min(self.maxdelta,current_r + self.gamma*self.Q(current_s,current_a) - priorQ ));
                if tderror > 0
                    tderror = (1 - self.kappa(self.totsteps))*tderror;
                else
                    tderror = (1 + self.kappa(self.totsteps))*tderror;
                end
                self.Q(self.prior_s,self.prior_a) = ...
                    priorQ + self.alpha(self.totsteps)*tderror;
                % update the policy
                self.policy.update( self.prior_s, self.Q(self.prior_s,:), self.totsteps );
            end
        end % function - update

        function [ tderror ] = lastupdate(self, current_r, current_s, suppress )
            if ~suppress % suppress prevents a new update from applying
                self.totsteps = self.totsteps + 1;
                % the final temporal difference update
                priorQ = self.Q(self.prior_s,self.prior_a);
                % update the Q-table
                tderror = max(self.mindelta,min(self.maxdelta,current_r - priorQ));
                if tderror > 0
                    tderror = (1 - self.kappa(self.totsteps))*tderror;
                else
                    tderror = (1 + self.kappa(self.totsteps))*tderror;
                end
                self.Q(self.prior_s,self.prior_a) = ...
                    priorQ + self.alpha(self.totsteps)*tderror;
                % update the policy
                self.policy.update( self.prior_s, self.Q(self.prior_s,:), self.totsteps );
            end
        end % function - lastupdate

    end % methods
end % classdef
