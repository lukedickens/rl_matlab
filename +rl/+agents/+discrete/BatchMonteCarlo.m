classdef BatchMonteCarlo < rl.agents.discrete.Critic & handle
% BATCHMONTECARLO - is object of type agent that stores returns in returnlists similar to the sutton
% montecarlo agent as described in chapter 5.
% ### optional settings 
% buffersize : maximum length of returns list, after which it forgets oldest values.
    properties (SetAccess='public')
        batchsize % number of episodes before updating Q table
        epcount % current number of episodes
        returnslist
        buffersize % max length of each return list
        trace
        epstep
    end % properties - public

    methods
        function self = BatchMonteCarlo(numstates,numactions,batchsize,gamma,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            kwargs('gamma') = gamma;
            self = self@rl.agents.discrete.Critic(numstates,numactions,kwargs);
            self.buffersize = kwargs.get('buffersize',inf);
            self.batchsize = batchsize;
            self.reset();
        end % function - BatchMonteCarlo constructor

        function [ current_a ] = first(self,first_s)
            current_a = first@rl.agents.base.Agent(self,first_s);
            self.trace = {};      % init traces record
            self.epstep = 1;
        end % first function

        function [ tderror ] = update( self, current_r, current_s, current_a, suppress )
            % suppress prevents a new update from applying for this step
            % here we store the alphas until the update at the end of the episode
            if suppress 
                error([class(self) ' does not support learning suppression']);
            else
                self.totsteps = self.totsteps + 1; % not used
            end
            % suppress doesn't work with monte-carlo
            self.trace.states(self.epstep) = self.prior_s;
            self.trace.actions(self.epstep) = self.prior_a;
            self.trace.rewards(self.epstep) = current_r;
            self.epstep = self.epstep + 1;
        end % function - update

        function [ tderror ] = lastupdate(self, current_r, current_s, suppress )
            % suppress prevents a new update from applying for this step
            % here we store the alphas until the update at the end of the episode
            if suppress
                error([class(self) ' does not support learning suppression']);
            else
                self.totsteps = self.totsteps + 1;
            end
            self.trace.states(self.epstep) = self.prior_s;
            self.trace.actions(self.epstep) = self.prior_a;
            self.trace.rewards(self.epstep) = current_r;
            T = self.epstep + 1;
            self.epcount = self.epcount + 1;
            % calculate returns for trace by working back through trace
            tot = 0;
            for t=1:T-1
                tot = tot + self.trace.rewards(T-t);
                returns(t) = tot;
            end
            % this is every visit so we update the returns list with the return for every visited state.
            for t=1:T-1
                s=self.trace.states(t);
                a=self.trace.actions(t);
                R=returns(t);
                self.returnslist{s,a} = [ self.returnslist{s,a}(max(end-self.buffersize+2,1):end) R ];
            end
            % every self.batchsize number of episodes we update the Q table.
            if mod(self.epcount,self.batchsize) == 0
                for s=1:self.numstates
                    for a=1:self.numactions
                        if ~isempty(self.returnslist{s,a})
                            self.Q(s,a) = mean(self.returnslist{s,a});
                        end
                    end
                end
                notabsorbing = find(~self.absorbing);
                self.policy.update(notabsorbing , self.Q(notabsorbing,:), self.totsteps );
            end
        end % function - lastupdate

        function [] = reset(self)
            reset@rl.agents.discrete.Critic(self);
            self.returnslist = cell(self.numstates,self.numactions);
            self.epcount = 1; % 
        end % function - reset
    end % methods
end % classdef
