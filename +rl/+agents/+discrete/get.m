function [ agent ] = get(agenttype, numstates, numactions, varargin)
    kwargs = rl.utils.dict(varargin{:});
    if kwargs.iskey('utilfunc')
        utilfunc = kwargs('utilfunc');
    else
        utilfunc = [];
    end    
    if kwargs.iskey('utiltype')
        utiltype = kwargs('utiltype');
    else
        utiltype = 'none';
    end    

    agenttype = lower(agenttype);
    switch agenttype
    case {'fixed'}
        agent = getFixedAgent(numstates, numactions, kwargs);
    case {'batchmontecarlo','bmc'}
        agent = getBatchMonteCarlo(numstates, numactions, kwargs);
    case {'iterativemontecarlo','montecarlo','imc'}
        agent = getMonteCarlo(numstates, numactions, kwargs);
    case {'sarsa'}
        agent = getSarsa(numstates, numactions, kwargs);
    case {'td'}
        agent = getTD(numstates, numactions, kwargs);
    case {'qlearn'}
        agent = getQLearn(numstates, numactions, kwargs);
    case {'sarsalambda'}
        agent = getSarsaLambda(numstates, numactions, kwargs);
    case {'modsarsalambda'}
        agent = getModSarsaLambda(numstates, numactions, kwargs);
    case {'pmc'}
        agent = getPseudoMonteCarlo(numstates, numactions, kwargs);
    case {'qp'}
        agent = getActorCriticQP(numstates, numactions, kwargs);
    % risk sensitive agents
    case {'rstd'}
        warning('Deprecated agent, hysteretic learner rather than risk-sensitive more Matignon than Mihatsch')
        agent = getRSTD(numstates, numactions, kwargs);
    case {'rssarsa'}
        agent = getRSSarsa(numstates, numactions, kwargs);
    case {'rsqlearn'}
        agent = getRSQLearn(numstates, numactions, kwargs);
    case {'sarsaxi'}
        agent = getSarsaXi(numstates, numactions, kwargs);
    % model based agents
    case {'modelbased'}
        agent = getModelBased(numstates, numactions, kwargs);
    otherwise
        error(sprintf('Unrecognised agent type %s',agenttype));
    end % if - agenttype
    if ~isempty(utilfunc)
        agent = rl.agents.universal.Utility(agent,utilfunc);
    elseif ~strcmpi(utiltype,'none')
        utilfunc = get_utilfunc(utiltype,kwargs);
        agent = rl.agents.universal.Utility(agent,utilfunc);
    end
end

function agent = getFixedAgent(numstates, numactions, kwargs)
    % iterative monte carlo that is
    if kwargs.iskey('trainedagent')
        % assumes you want the greedy policy (set greedy to false for current policy)
        greedy = kwargs.get('greedy',true);
        % passed in a trained agent you want to extract policy from
        trainedagent = kwargs('trainedagent');
        if greedy
            policyweights =  trainedagent.greedy_policy();
        else
            policyweights = trainedagent.policy.weights;
        end
    else
        policyweights = kwargs('policy');
    end
    agent = rl.agents.discrete.FixedAgent(numstates,numactions,policyweights);
end

function agent = getBatchMonteCarlo(numstates, numactions, kwargs)
    % iterative monte carlo that is
    batchsize = kwargs.pop('batchsize');
    gamma = kwargs.pop('gamma');
    kwargs('temperature') = GetDecayFunc('temperature',kwargs);
    kwargs('epsilon') = GetDecayFunc('epsilon',kwargs);
    agent = rl.agents.discrete.BatchMonteCarlo(numstates,numactions,batchsize,gamma,kwargs);
end

function agent = getMonteCarlo(numstates, numactions, kwargs)
    % iterative monte carlo that is
    gamma = kwargs.pop('gamma');
    alpha = GetDecayFunc('alpha',kwargs);
    kwargs('temperature') = GetDecayFunc('temperature',kwargs);
    kwargs('epsilon') = GetDecayFunc('epsilon',kwargs);
    agent = rl.agents.discrete.MonteCarlo(numstates,numactions,gamma,alpha,kwargs);
end

function agent = getSarsa(numstates, numactions, kwargs)
    gamma = kwargs.pop('gamma');
    alpha = GetDecayFunc('alpha',kwargs);
    kwargs('temperature') = GetDecayFunc('temperature',kwargs);
    kwargs('epsilon') = GetDecayFunc('epsilon',kwargs);
    agent = rl.agents.discrete.Sarsa(numstates,numactions,gamma,alpha,kwargs);
end

function agent = getQLearn(numstates, numactions, kwargs)
    gamma = kwargs.pop('gamma');
    alpha = GetDecayFunc('alpha',kwargs);
    kwargs('temperature') = GetDecayFunc('temperature',kwargs);
    kwargs('epsilon') = GetDecayFunc('epsilon',kwargs);
    agent = rl.agents.discrete.QLearn(numstates,numactions,gamma,alpha,kwargs);
end

function agent = getSarsaLambda(numstates, numactions, kwargs)
    gamma = kwargs.pop('gamma');
    lambda = kwargs.pop('lambda');
    alpha = GetAlpha(kwargs);
    kwargs('temperature') = GetDecayFunc('temperature',kwargs);
    kwargs('epsilon') = GetDecayFunc('epsilon',kwargs);
    agent = rl.agents.discrete.SarsaLambda(numstates,numactions,gamma,alpha,lambda,kwargs);
end

function agent = getModSarsaLambda(numstates, numactions, kwargs)
    gamma = kwargs.pop('gamma');
    lambda = kwargs.pop('lambda');
    alpha = GetAlpha(kwargs);
    kwargs('temperature') = GetDecayFunc('temperature',kwargs);
    kwargs('epsilon') = GetDecayFunc('epsilon',kwargs);
    agent = rl.agents.discrete.ModSarsaLambda(numstates,numactions,gamma,alpha,lambda,kwargs);
end

function agent = getPseudoMonteCarlo(numstates, numactions, kwargs)
    gamma = kwargs.pop('gamma');
    alpha = GetDecayFunc('alpha',kwargs);
    kwargs('temperature') = GetDecayFunc('temperature',kwargs);
    kwargs('epsilon') = GetDecayFunc('epsilon',kwargs);
    agent = rl.agents.discrete.PseudoMonteCarlo(numstates,numactions,gamma,alpha,kwargs);
end

function agent = getActorCriticQP(numstates, numactions, kwargs)
    % initialise the subagent
    critictype = kwargs('critictype');
    criticargs = kwargs.copy();
    criticargs('policytype') = 'none';
    % we only want to apply the utility function once, so the critic shouldn't be called with it.
    if criticargs.iskey('utiltype')
        criticargs.rmkey('utiltype');
    end
    if criticargs.iskey('utilfunc')
        criticargs.rmkey('utilfunc');
    end
    critic = rl.agents.discrete.get(critictype,numstates,numactions,criticargs);
    % now the full agent    
    % needed for actor
    beta = GetDecayFunc('beta',kwargs);
    temperature = GetDecayFunc('temperature',kwargs);
    agent = rl.agents.discrete.ActorCriticQP(critic,numstates,numactions,beta,temperature);
end


function agent = getSarsaNormed(numstates, numactions, kwargs)
    optargs = GetOptArgs(kwargs);
    gamma = kwargs('gamma');
    kwargs('epsilon') = GetDecayFunc('epsilon',kwargs);
    kwargs('alpha') = GetDecayFunc('alpha',kwargs);
    eta = kwargs.pop('eta');
    agent = rl.agents.discrete.SarsaNormed(numstates,numactions,gamma,epsilon,alpha,eta,optargs);
end

function agent = getRSTD(numstates, numactions, kwargs)
    gamma = kwargs.pop('gamma');
    % alphaplus and alphaminus
    alphaplus = kwargs.get('alphaplus',[]);
    alphaminus = kwargs.get('alphaminus',[]);
    if isempty(alphaplus)
        alphaplus0 = kwargs.get('alphaplus0',0.1);
        alphaplusdecay = kwargs.get('alphaplusdecay',0);
        alphaplus = @(i) alphaplus0/i^alphaplusdecay;
    else
        kwargs.rmkey('alphaplus');
    end
    if isempty(alphaminus)
        alphaminus0 = kwargs.get('alphaminus0',0.1);
        alphaminusdecay = kwargs.get('alphaminusdecay',0);
        alphaminus = @(i) alphaminus0/i^alphaminusdecay;
    else
        kwargs.rmkey('alphaminus');
    end
    agent = rl.agents.discrete.RSTD(numstates,numactions,gamma,alphaplus,alphaminus,kwargs);
end

function agent = getRSSarsa(numstates, numactions, kwargs)
    gamma = kwargs.pop('gamma');
    alpha = GetDecayFunc('alpha',kwargs);
    kappa = GetDecayFunc('kappa',kwargs);
    agent = rl.agents.discrete.RSSarsa(numstates,numactions,gamma,alpha,kappa,kwargs);
end

function agent = getRSQLearn(numstates, numactions, kwargs)
    gamma = kwargs.pop('gamma');
    alpha = GetDecayFunc('alpha',kwargs);
    kappa = GetDecayFunc('kappa',kwargs);
    agent = rl.agents.discrete.RSSarsa(numstates,numactions,gamma,alpha,kappa,kwargs);
end

function agent = getSarsaXi(numstates, numactions, kwargs)
    gamma = kwargs.pop('gamma');
    alpha = GetDecayFunc('alpha',kwargs);
    xi = GetDecayFunc('xi',kwargs);
    agent = rl.agents.discrete.SarsaXi(numstates,numactions,gamma,alpha,xi,kwargs);
end

function agent = getModelBased(numstates, numactions, kwargs)
    gamma = kwargs.pop('gamma');
    kwargs('temperature') = GetDecayFunc('temperature',kwargs);
    kwargs('epsilon') = GetDecayFunc('epsilon',kwargs);
    agent = rl.agents.discrete.ModelBased(numstates,numactions,gamma,kwargs);
end

% supporting functions
function utilfunc = get_utilfunc(utiltype,kwargs)
    utilmult = kwargs.get('utilmult',1);
    utilshift = kwargs.get('utilshift',0);
    if strcmpi(utiltype,'pow')
        utilpower = kwargs.get('utilpower',0.5);
        utilfunc = @(r) utilmult*sign(r+utilshift)*abs(r+utilshift).^utilpower;
    elseif strcmpi(utiltype,'exp')
        utiltemp = kwargs('utiltemp');
        utilfunc = @(r) utilmult*exp((r+utilshift)/utiltemp);
    elseif strcmpi(utiltype,'rplusexp')
        utilmult = kwargs('utilmult');
        utilfunc = @(r) r + utilmult*exp(utilmult*(r+utilshift));
    elseif strcmpi(utiltype,'log')
        utilfunc = @(r) utilmult*log(r+utilshift);
    else
        error('Unrecognised utiltype');
    end
end

function func = GetDecayFunc(name,kwargs)
    switch name
    case 'epsilon'
        default0 = 0.1;
        defaultdecay = 0;
        defaultfinal = 0;
    case 'temperature'
        default0 = 1;
        defaultdecay = 0;
        defaultfinal = 0;
    case 'alpha'
        default0 = 0.1;
        defaultdecay = 0;
        defaultfinal = 0;
    case 'beta'
        default0 = 0.01;
        defaultdecay = 0;
        defaultfinal = 0;
    case 'kappa'
        default0 = 0.1;
        defaultdecay = 0;
        defaultfinal = 0;
    case 'xi'
        default0 = 0.1;
        defaultdecay = 0;
        defaultfinal = 0;
    end
    if kwargs.iskey(name)
        func = kwargs(name);
    else
        param0 = kwargs.get([name '0'],default0);
        paramdecay = kwargs.get([name 'decay'],defaultdecay);
        paramfinal = kwargs.get([name 'final'],defaultfinal);
        func = @(i) (param0-paramfinal)/i^paramdecay + paramfinal;
    end
    %XXX DEBUG LINE
    %display( [ name ': ' num2str(func(1)) ])
end

function optargs = GetOptArgs(kwargs)
    optargs = dict;
    if kwargs.iskey('Q')
        optargs('Q') = kwargs('Q');
    end
    if kwargs.iskey('policy')
        optargs('policy') = kwargs('policy');
    end
    if kwargs.iskey('mindelta')
        optargs('mindelta') = kwargs('mindelta');
    end
    if kwargs.iskey('maxdelta')
        optargs('maxdelta') = kwargs('maxdelta');
    end
end
