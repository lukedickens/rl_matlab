classdef eGreedyPolicy < rl.agents.discrete.Policy & handle
    properties (SetAccess='public')
        epsilonfunc
    end % properties - public

    methods

        function self = eGreedyPolicy(numstates,numactions,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            self = self@rl.agents.discrete.Policy(numstates,numactions,kwargs);
            if kwargs.iskey('epsilon')
                epsilon = kwargs('epsilon');
                if isa(epsilon,'function_handle')
                    self.epsilonfunc = epsilon;
                elseif isnumeric(epsilon)
                    self.epsilonfunc = @(i)(epsilon);
                else
                    error('Inappropriate type of epsilon passed');
                end
            end
        end % function - eGreedyPolicy constructor

        function [ ] = update( self, states, vals, count )
            epsilon = self.epsilonfunc(count);
            [mxvs,inds] = max(vals,[],2);
            self.weights(states,:) = epsilon/(self.numactions-1);
            self.weights(sub2ind([self.numstates,self.numactions],states,inds')) = 1-epsilon;
        end % function - update

    end % methods
end % classdef
