classdef DummyPolicy < handle
    properties (SetAccess='public')
    end % properties - public

    methods
        function self = DummyPolicy()
        end % function - DummyPolicy constructor

        function [ ] = update( self, states, vals, count )
            % do nothing.
        end % next function

        function [ action ] = choose(state)
            error('choose not implemented for do nothing policy');
        end

        function [ ] = unbiased( self, states )
            % do nothing.
        end % next function

    end % methods
end % classdef
