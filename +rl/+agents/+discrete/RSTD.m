classdef RSTD < rl.agents.discrete.Critic & handle
    %% RSTD
    properties (SetAccess='public')
        alphaplus
        alphaminus
    end % properties - protected, protected
    
    methods
        function self = RSTD(numstates,numactions,gamma,alphaplus,alphaminus,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            kwargs('gamma') = gamma;
            self = self@rl.agents.discrete.Critic(numstates,numactions,kwargs);
            if isa(alphaplus,'function_handle')
                self.alphaplus = alphaplus;
            elseif isnumeric(alphaplus)
                self.alphaplus = @(i)(alphaplus);
            else
                error('Inappropriate type of alphaplus passed');
            end
            if isa(alphaminus,'function_handle')
                self.alphaminus = alphaminus;
            elseif isnumeric(alphaminus)
                self.alphaminus = @(i)(alphaminus);
            else
                error('Inappropriate type of alphaminus passed')
            end
        end % function - RSTD

        function [ tderror ] = update( self, current_r, current_s, current_a, suppress )
            if ~suppress % suppress prevents a new update from applying
                self.totsteps = self.totsteps + 1;
                % for efficiency we only want to read this value once.
                priorQ = self.Q(self.prior_s,self.prior_a);
                % Now update the Q table and the policy
                % the temporal difference update
                tderror = max(self.mindelta,min(self.maxdelta,current_r + self.gamma*max(self.Q(current_s,:)) - priorQ ));
                if tderror > 0
                    alpha = self.alphaplus(self.totsteps);
                else
                    alpha = self.alphaminus(self.totsteps);
                end
                self.Q(self.prior_s,self.prior_a) = ...
                    priorQ + alpha*tderror;
                % update the policy
                self.policy.update( self.prior_s, self.Q(self.prior_s,:), self.totsteps );
            end
        end % function - update

        function [ tderror ] = lastupdate(self, current_r, current_s, suppress )
            if ~suppress % suppress prevents a new update from applying
                self.totsteps = self.totsteps + 1;
                % the final temporal difference update
                priorQ = self.Q(self.prior_s,self.prior_a);
                % update the Q-table
                tderror = max(self.mindelta,min(self.maxdelta,current_r - priorQ));
                if tderror > 0
                    alpha = self.alphaplus(self.totsteps);
                else
                    alpha = self.alphaminus(self.totsteps);
                end
                self.Q(self.prior_s,self.prior_a) = ...
                    priorQ + alpha*tderror;
                % update the policy
                self.policy.update( self.prior_s, self.Q(self.prior_s,:), self.totsteps );
            end
        end % function - lastupdate
    end % methods
end % classdef
