classdef FixedAgent < rl.agents.base.Agent & handle
    properties (SetAccess='public')
        numstates
        numactions
        totsteps
    end % properties - public
    
    methods
        function self = FixedAgent(numstates,numactions,policy)
            self = self@rl.agents.base.Agent();
            self.totsteps = 0;
            self.numstates = numstates;
            self.numactions = numactions;
            self.policy = rl.agents.discrete.FixedPolicy(numstates,numactions,policy);
        end % function - FixedAgent

        function [ dummy ] = update( self, current_r, current_s, current_a, suppress )
            % do nothing & return dummy variable
            dummy = 0;
        end % function - next

        function [ dummy ] = lastupdate(self, current_r, current_s, suppress )
            % do nothing & return dummy variable
            dummy = 0;
        end % function - last

    end % methods
end % classdef
