classdef PseudoMonteCarlo < rl.agents.discrete.Critic & handle
    properties (SetAccess='public')
        z % eligibility trace (a pseudo trace in this case)
    end % properties - public

    methods
        function self = PseudoMonteCarlo(numstates,numactions,gamma,alpha,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            kwargs('gamma') = gamma;
            kwargs('alpha') = alpha;
            self = self@rl.agents.discrete.Critic(numstates,numactions,kwargs);
        end % function - PseudoMonteCarlo constructor

        function [ current_a ] = first(self,first_s)
            self.z = zeros(self.numstates,self.numactions);      % init eligibility vector
            current_a = first@rl.agents.base.Agent(self,first_s);
        end % first function

        function [ tderror ] = update( self, current_r, current_s, current_a, suppress )
            % suppress is an optional argument that suppresses learning of the Q-table
            % care needs to be taken as this isn't strictly principled in PseudoMonteCarlo
            if suppress % suppress prevents a new update from applying
                alpha = 0;
            else
                self.totsteps = self.totsteps + 1;
                alpha = self.alpha(self.totsteps);
            end
            self.z(self.prior_s,self.prior_a) = (1-alpha)*self.z(self.prior_s,self.prior_a) + alpha;
                                                                    % increment eligibility element
            self.Q(self.prior_s,self.prior_a) = (1-alpha)*self.Q(self.prior_s,self.prior_a); % modify just the visited state
            self.Q = self.Q + self.z*current_r;                % update all Q-table
            self.z = self.gamma*self.z;             % degrade eligibility vector
            % correct systematic bias in the Q table for the policy update
            notabsorbing = find(~self.absorbing);
            correctedQ = self.Q(notabsorbing,:) + ...
                self.gamma*self.Q(current_s,current_a)/(1-self.gamma*self.z(current_s,current_a))*self.z(notabsorbing,:);
            self.policy.update(notabsorbing , correctedQ, self.totsteps );
                                                                    % policy update
        end % function - update

        function [ tderror ] = lastupdate(self, current_r, current_s, suppress )
            % suppress is an optional argument that suppresses learning of the Q-table
            % care needs to be taken as this isn't strictly principled in PseudoMonteCarlo
            if suppress % suppress prevents a new update from applying
                alpha = 0;
            else
                self.totsteps = self.totsteps + 1;
                alpha = self.alpha(self.totsteps);
            end
            self.z(self.prior_s,self.prior_a) = self.z(self.prior_s,self.prior_a) + alpha;
                                                                    % increment eligibility element
            self.Q(self.prior_s,self.prior_a) = (1-alpha)*self.Q(self.prior_s,self.prior_a); % modify just the visited state
            self.Q = self.Q + self.z*current_r;                % update all Q-table
            self.z = zeros(self.numstates,self.numactions);      % reset eligibility vector
            % no need to correct systematic bias in the Q table for the policy update
            notabsorbing = find(~self.absorbing);
            self.policy.update(notabsorbing , self.Q(notabsorbing,:), self.totsteps );
                                                                    % policy update
        end % function - lastupdate

    end % methods
end % classdef
