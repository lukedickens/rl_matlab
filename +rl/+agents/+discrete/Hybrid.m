classdef Hybrid < rl.agents.base.Agent & handle
    properties % Set and get access public by default.
        numstates % number of states
        numactions % number of actions
        mbagent
        mfagent
        mixing
    end
    properties (Hidden=true)
    end % properties - public
    methods
        function self = Hybrid(mbagent,mfagent,mixing,varargin)
            kwargs = rl.utils.dict(varargin{:});
            self = self@rl.agents.base.Agent();
            self.numstates = self.mbagent.numstates;
            self.numactions = self.mbagent.numactions;
            self.mbagent = mbagent;
            self.mfagent = mfagent;
            self.mixing = mixing;
        end % function - Hybrid

        function [] = reset(self)
        end % function - reset

        function [ tderror ] = update( self, current_r, current_s, current_a, suppress )
            if ~suppress % suppress prevents a new update from applying
                self.totsteps = self.totsteps + 1;
                self.mbagent.update(current_r, current_s, current_a,false);
                self.mfagent.update(current_r, current_s, current_a,false);
                self.Q = mixing*self.mbagent.Q + (1-mixing)*self.mfagent.Q;
                self.policy.update([1:self.numstates],self.Q,self.totsteps);
                % update mfagent policy but no need to update mbagent as this doesn't affect optimisation.
                self.mfagent.policy.weights = self.policy.weights; %XXX Demeter's Law!!!!
            end % if not suppressing
        end % function - next

        function [ tderror ] = lastupdate(self, current_r, current_s, suppress )
            self.update( current_r, current_s, 0, suppress ); % ignore current action
        end % function - last

    end % methods
end % classdef
