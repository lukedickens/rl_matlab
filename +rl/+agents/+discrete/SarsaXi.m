classdef SarsaXi < rl.agents.discrete.Critic & handle
% SarsaXi - a homegrown exponetial value function based sarsa like agent
% As discounted by:
% O. Mihatsch and R. Neuneier. Risk-Sensitive Reinforcement Learning. Mach. Learn., 49(2-3), 2002.
    properties (SetAccess='public')
        xifunc
    end % properties - protected, protected
    
    methods
        function self = SarsaXi(numstates,numactions,gamma,alpha,xi,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            kwargs('gamma') = gamma;
            kwargs('alpha') = alpha;
            self = self@rl.agents.discrete.Critic(numstates,numactions,kwargs);
            if isa(xi,'function_handle')
                self.xifunc = xi;
            elseif isnumeric(kappa)
                self.xifunc = @(i)(c);
            else
                error('Inappropriate type of xi passed');
            end
        end % function - SarsaXi

        function [ xierror ] = update( self, current_r, current_s, current_a, suppress )
            if ~suppress % suppress prevents a new update from applying
                self.totsteps = self.totsteps + 1;
                % for efficiency we only want to read this value once.
                priorQ = self.Q(self.prior_s,self.prior_a);
                % Now update the Q table and the policy
                % the temporal difference update
                xi = self.xifunc(self.totsteps);
                xierror = exp(xi*current_r)*self.Q(current_s,current_a)^self.gamma - priorQ;
                self.Q(self.prior_s,self.prior_a) = ...
                    priorQ + self.alpha(self.totsteps)*xierror;
                % update the policy
                self.policy.update( self.prior_s, xi*self.Q(self.prior_s,:), self.totsteps );
            end
        end % function - update

        function [ xierror ] = lastupdate(self, current_r, current_s, suppress )
            if ~suppress % suppress prevents a new update from applying
                self.totsteps = self.totsteps + 1;
                % the final temporal difference update
                priorQ = self.Q(self.prior_s,self.prior_a);
                % update the Q-table
                xi = self.xifunc(self.totsteps);
                xierror = exp(xi*current_r) - priorQ;
                self.Q(self.prior_s,self.prior_a) = ...
                    priorQ + self.alpha(self.totsteps)*xierror;
                % update the policy
                self.policy.update( self.prior_s, log(self.Q(self.prior_s,:))/xi, self.totsteps );
            end
        end % function - lastupdate

        % Q tables
        function [] = reset(self)
            % reset the self to zero learning.
            self.totsteps = 0;
            self.policy.unbiased(find(~self.absorbing));
            self.Q = ones(self.numstates, self.numactions);
        end % function - reset

        function [ adv ] = advantage(self,s,a,polweights)
%            rowQ = self.Q(self.prior_s,:);
%	    xi = self.xifunc(self.totsteps);
	    % advantage is the difference between the certainty equivalent Q and certainty equiv V
%            adv = log(rowQ(self.prior_a))/xi - log(polweights*rowQ')/xi;
	% alternative method 1
%            rowQ = log(self.Q(self.prior_s,:))/self.xifunc(self.totsteps);
%            adv = rowQ(self.prior_a) - polweights*rowQ';
	% alternative method 2
            rowQ = self.Q(self.prior_s,:);
            adv = sign(self.xifunc(self.totsteps))*(rowQ(self.prior_a) - polweights*rowQ');
        end


    end % methods
end % classdef
