classdef Critic < rl.agents.base.Agent & handle
    properties (SetAccess='public')
        numstates
        numactions
        Q
        absorbing
        % optional properties (may not be used)
        gamma % geometric discount
        alpha % function takes time, returns learning step-size
    end % properties - public
    properties (SetAccess='public',GetAccess='public',Hidden=true)
    % now in appropriate policy classes
%        epsilon % function takes time, returns probability of non-dominant action  (for egreedy policy)
%        temperature % function takes time, returns boltzman temperature (for softmax policy)
        totsteps % count of number of steps in total
        mindelta % can set upper limit on size of td-error
        maxdelta % can set lower limit on size of td-error
        Q0 % initialise the Q table.
    end % properties - protected
    
    methods
        function self = Critic(numstates,numactions,varargin)
            self = self@rl.agents.base.Agent();
            self.totsteps = 0;
            self.numstates = numstates;
            self.numactions = numactions;
            kwargs = rl.utils.dict(varargin{:});
            % initialise properties
            if kwargs.iskey('absorbing')
                % specified input values for absorbing
                self.absorbing = kwargs('absorbing');
            else
                self.absorbing = zeros(1,self.numstates);
            end % if - absorbing defined
            if kwargs.iskey('Q')
                % specified input values for Q
                self.Q0 = kwargs('Q');
            else
                % default state-action value function is zero everywhere
                self.Q0 = zeros(self.numstates, self.numactions);
            end % if - Q defined
            % set optional properties
            self.gamma = kwargs.get('gamma',[]);
            if kwargs.iskey('alpha')
                alpha = kwargs('alpha');
                if isa(alpha,'function_handle')
                    self.alpha = alpha;
                elseif isnumeric(alpha)
                    self.alpha = @(i)(alpha);
                else
                    error('Inappropriate type of alpha passed');
                end
            end
            % policy
            policytype = kwargs.get('policytype','egreedy');
            policyargs = kwargs;
            if policyargs.iskey('policy')
                policyargs('weights') = policyargs.pop('policy');
            end % if - policy predefined
            self.policy = rl.agents.discrete.Policy.get(policytype,self.numstates,self.numactions,policyargs);
            % td error type methods can require maxima/minima on the delta
            if kwargs.iskey('maxdelta')
                self.maxdelta = kwargs('maxdelta');
            else
                self.maxdelta = inf; 
            end % if 
            if kwargs.iskey('mindelta')
                self.mindelta = kwargs('mindelta');
            else
                self.mindelta = -inf; 
            end % if 

            self.reset(); % Initialises Q table and sets other values appropriately.
        end % function - Critic

        function [] = reset(self)
            %XXX should be policy reset function (DONE)
            %self.policy.unbiased(find(~self.absorbing));
            reset@rl.agents.base.Agent(self);
            % reset the self to zero learning.
            self.totsteps = 0;
            self.Q = self.Q0;
        end % function - reset

        % Q tables
        function [ policy ] =  greedy_policy(self)
            policy = zeros(self.numstates,self.numactions); % each row has A possible actions each has an assigned probability
            [value, indices] = max(self.Q,[],2);
            for s = find(~self.absorbing)
                policy(s,indices(s)) = 1;
            end
        end
        function [ actionlist ] =  best_actions(self)
            greedy_policy = self.greedy_policy();
            [vals,actionlist] = max(greedy_policy,[],2);
        end
        function [ adv ] = advantage(self,s,a,polweights)
            rowQ = self.Q(self.prior_s,:); % cache row of Qs
            adv = rowQ(self.prior_a) - polweights*rowQ';
        end
        
    end % methods
end % classdef
