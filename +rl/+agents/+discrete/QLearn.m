classdef QLearn < rl.agents.discrete.Critic & handle
% Q-Learning : original Watkin's version - discrete state
    properties (SetAccess='public')
    end % properties - protected, protected
    methods
        function self = QLearn(numstates,numactions,gamma,alpha,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            kwargs('gamma') = gamma;
            kwargs('alpha') = alpha;
            self = self@rl.agents.discrete.Critic(numstates,numactions,kwargs);
        end % function - QLearn

        function [ tderror ] = update( self, current_r, current_s, current_a, suppress )
            if ~suppress % suppress prevents a new update from applying
                self.totsteps = self.totsteps + 1;
                % for efficiency we only want to read this value once.
                priorQ = self.Q(self.prior_s,self.prior_a);
                % Now update the Q table and the policy
                % the temporal difference update
                tderror = max(self.mindelta,min(self.maxdelta,current_r + self.gamma*max(self.Q(current_s,:)) - priorQ ));
                alpha = self.alpha(self.totsteps);
                self.Q(self.prior_s,self.prior_a) = priorQ + alpha*tderror;
                % update the policy
                self.policy.update( self.prior_s, self.Q(self.prior_s,:), self.totsteps );
            end
        end % function - next

        function [ tderror ] = lastupdate(self, current_r, current_s, suppress )
            if ~suppress % suppress prevents a new update from applying
                self.totsteps = self.totsteps + 1;
                % the final temporal difference update
                priorQ = self.Q(self.prior_s,self.prior_a);
                % update the Q-table
                tderror = max(self.mindelta,min(self.maxdelta,current_r - priorQ));
                alpha = self.alpha(self.totsteps);
                self.Q(self.prior_s,self.prior_a) = ...
                    priorQ + alpha*tderror;
                % update the policy
                self.policy.update( self.prior_s, self.Q(self.prior_s,:), self.totsteps );
            end
        end % function - last

    end % methods
end % classdef
