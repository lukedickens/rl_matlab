classdef ModelBased < rl.agents.base.Agent & handle
    properties % Set and get access public by default.
        numstates % number of states
        numactions % number of actions
        batchsize % number of steps before relearning
        gamma % geometric discount
        theta % the accuracy threshold for PolicyIteration
        V % the value table
        Q % the Q-value table (helpful for hybrid)
    end
    properties (Hidden=true)
        transcounts0
        weights0
        totsteps % total number of steps so far
        model % the model lf the system
    end % properties - public
    methods
        function self = ModelBased(numstates,numactions,gamma,varargin)
            kwargs = rl.utils.dict(varargin{:});
            self = self@rl.agents.base.Agent();
            self.numstates = numstates;
            self.numactions = numactions;
            self.model.initial = kwargs.get('initial',ones(1,self.numstates)/self.numstates);
            self.model.absorbing = kwargs.get('absorbing',zeros(1,self.numstates));
            self.model.numstates = self.numstates;
            self.model.numactions = self.numactions;
            self.batchsize = kwargs.get('batchsize',1);
            self.theta = kwargs.get('theta',10^-3);
            self.gamma = gamma;
            policytype =kwargs.get('policytype','egreedy');
            self.policy = rl.agents.discrete.Policy.get(policytype,self.numstates,self.numactions,kwargs);
            if kwargs.iskey('transcounts0')
                self.transcounts0 = kwargs('transcounts0');
            elseif kwargs.get('bayesian',true)
                %XXX Bayesian approach with uniformative prior.
                self.transcounts0 = ones(self.numstates,self.numstates,self.numactions);
            else
                %XXX max likelihood
                self.transcounts0 = zeros(self.numstates,self.numstates,self.numactions);
            end
            self.weights0 = kwargs.get('policy',ones(self.numstates,self.numactions)/self.numactions);
            self.reset();
        end % function - ModelBased

        function [] = reset(self)
            % reset the self to zero learning.
            self.totsteps = 0;
            self.model.transcounts = self.transcounts0;
            self.model.rewlists = cell(self.numstates,self.numstates,self.numactions);
            self.policy.weights = self.weights0;
            self.update_model();
            %self.policy.unbiased(find(~self.model.absorbing));
            self.V = zeros(self.numstates, 1);
        end % function - reset

        function [] = update_model(self)
            self.calcT();
            self.calcR();
        end
        
        function [] = calcT(self)
            for a=1:self.numactions ,for s=1:self.numstates
                tot = sum(self.model.transcounts(:,s,a));
                if tot > 0
                    self.model.T(:,s,a) = self.model.transcounts(:,s,a)/tot;
                else
                    self.model.T(:,s,a) = ones(self.numstates,1)/self.numstates;
                end % if row all zero
            end, end % for each action for each state
            self.model.t = @(s,a,ss) self.model.T(ss,s,a);
        end

        function [] = calcR(self)
            for a=1:self.numactions ,for s=1:self.numstates,for ss=1:self.numstates
                if isempty(self.model.rewlists{ss,s,a})
                    self.model.R(ss,s,a) = 0;
                else
                    %self.model.R(ss,s,a) = mean(self.model.rewlists{ss,s,a});
                    allrew = self.model.rewlists{ss,s,a};
                    self.model.R(ss,s,a) = allrew(end);
                end % if row all zero
            end, end, end % for each action for each state
            self.model.r = @(s,a,ss) self.model.R(ss,s,a);
        end

        function [ tderror ] = update( self, current_r, current_s, current_a, suppress )
            if ~suppress % suppress prevents a new update from applying
                self.totsteps = self.totsteps + 1;
                self.model.transcounts(current_s,self.prior_s,self.prior_a) = self.model.transcounts(current_s,self.prior_s,self.prior_a) + 1;
                self.model.rewlists{current_s,self.prior_s,self.prior_a} = [self.model.rewlists{current_s,self.prior_s,self.prior_a} current_r];
                if mod(self.totsteps,self.batchsize) == 0
                    % update the policy
                    self.update_model();
                    [ optpol self.V self.Q] = rl.algorithms.PolicyIterationFast( self.model, self.gamma, self.theta);
                    %self.policy.weights = rl.algorithms.eGreedyPolicyFromQ(Q,0.1,self.model.absorbing);
                    self.policy.update([1:self.numstates],self.Q,self.totsteps);
                end % if time to update
            end % if not suppressing
        end % function - next

        function [ tderror ] = lastupdate(self, current_r, current_s, suppress )
            self.update( current_r, current_s, 0, suppress ); % ignore current action
        end % function - last

    end % methods
end % classdef
