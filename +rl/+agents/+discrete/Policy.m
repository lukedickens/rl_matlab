classdef Policy < handle
%POLICY a fixed policy
    properties (SetAccess='public')
        numstates
        numactions
        weights
    end % properties - public

    methods
        function self = Policy(numstates,numactions,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            self.numstates = numstates;
            self.numactions = numactions;
            if kwargs.iskey('weights')
                self.weights = kwargs('weights');
                if ~all(sum(self.weights,2) == 1)
                    error('Initial weights not normalised');
                end
                if ~all(size(self.weights)==[self.numstates,self.numactions])
                    error('Size of initial policy weight matrix does not match numbers of states and actions')
                end
            else
                % default initial policy is unbiased
                self.unbiased();
            end % if - weights defined
        end % function - Policy constructor

        function [ ] = update( self, states, vals, count )
            error('Abstract class does not implement update. Use FixedPolicy class for fixed policy');
        end % next function

        function [ ] = unbiased( self, states )
        % sets the policy to unbiased
            if nargin < 2
                self.weights = ones(self.numstates,self.numactions)./self.numactions;
            else
                self.weights(states,:) = ones(length(states),self.numactions)./self.numactions;
            end
        end % next function
        
        function [ action ] = choose(self,state)
            action = find(mnrnd(1,self.weights(state,:)),1);
        end

        function [] = reset(self)
            %XXX should really define policy0
            self.unbiased();
        end
    end % methods

    methods(Static)
        function policy = get(policytype,numstates,numactions,varargin)
            kwargs = rl.utils.dict(varargin{:});
            if kwargs.iskey('policy')
                kwargs('weights') = kwargs('policy');
            end % if - policy predefined
            switch policytype
            case 'softmax'
                kwargs('temperature') = kwargs.get('temperature',1);
                policy = rl.agents.discrete.SoftMaxPolicy(numstates,numactions,kwargs);
            case 'egreedy'
                kwargs('epsilon') = kwargs.get('epsilon',0.1);
                policy = rl.agents.discrete.eGreedyPolicy(numstates,numactions,kwargs);
            case 'fixed'
                policy = rl.agents.discrete.FixedPolicy(numstates,numactions,kwargs('weights'));
            case 'none'
                policy = rl.agents.discrete.DummyPolicy();
            otherwise
                error('Unrecognised policy type');
            end
        end
    end
end % classdef
