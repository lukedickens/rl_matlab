classdef MonteCarlo < rl.agents.discrete.Critic & handle
    properties (SetAccess='public')
        trace
        epstep % step within the episode
    end % properties - public

    methods
        function self = MonteCarlo(numstates,numactions,gamma,alpha,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            kwargs('gamma') = gamma;
            kwargs('alpha') = alpha;
            self = self@rl.agents.discrete.Critic(numstates,numactions,kwargs);
        end % function - MonteCarlo constructor

        function [ current_a ] = first(self,first_s)
            self.trace = {};      % init traces record
            self.epstep = 1;
            current_a = first@rl.agents.base.Agent(self,first_s);
        end % first function

        function [ tderror ] = update( self, current_r, current_s, current_a, suppress )
            % suppress prevents a new update from applying for this step
            % here we store the alphas until the update at the end of the episode
            if suppress 
                self.trace.alpha(self.epstep) = 0;
            else
                self.totsteps = self.totsteps + 1;
                self.trace.alpha(self.epstep) = self.alpha(self.totsteps);
            end
            % suppress doesn't work with monte-carlo
            self.trace.states(self.epstep) = self.prior_s;
            self.trace.actions(self.epstep) = self.prior_a;
            self.trace.rewards(self.epstep) = current_r;
            self.epstep = self.epstep + 1;
        end % function - update

        function [ tderror ] = lastupdate(self, current_r, current_s, suppress )
            % suppress prevents a new update from applying for this step
            % here we store the alphas until the update at the end of the episode
            if suppress
                self.trace.alpha(self.epstep) = 0;
            else
                self.totsteps = self.totsteps + 1;
                self.trace.alpha(self.epstep) = self.alpha(self.totsteps);
            end
            self.trace.states(self.epstep) = self.prior_s;
            self.trace.actions(self.epstep) = self.prior_a;
            self.trace.rewards(self.epstep) = current_r;
            T = self.epstep + 1;
            tot = 0;
            for t=1:T-1
                tot = tot + self.trace.rewards(T-t);
                returns(t) = tot;
            end
            for t=1:T-1
                alpha = self.trace.alpha(t);
                s=self.trace.states(t);
                a=self.trace.actions(t);
                R=returns(t);
                self.Q(s,a) = (1-alpha)*self.Q(s,a) + alpha*R;
            end
            notabsorbing = find(~self.absorbing);
            self.policy.update(notabsorbing , self.Q(notabsorbing,:), self.totsteps );
        end % function - lastupdate
    end % methods
end % classdef
