classdef FixedPolicy < rl.agents.discrete.Policy & handle
    methods
        function self = FixedPolicy(numstates,numactions,weights)
            self = self@rl.agents.discrete.Policy(numstates,numactions,'weights',weights);
        end % function - FixedPolicy constructor

        function [ ] = update( self, states, vals, count )
            % do nothing.
        end % next function

    end % methods
end % classdef
