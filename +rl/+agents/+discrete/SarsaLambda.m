classdef SarsaLambda < rl.agents.discrete.Critic & handle
    properties (SetAccess='public')
        z % eligibility trace
        lambda % horizon parameter
    end % properties - public

    methods
        function self = SarsaLambda(numstates,numactions,gamma,alpha,lambda,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            kwargs('gamma') = gamma;
            kwargs('alpha') = alpha;
            self = self@rl.agents.discrete.Critic(numstates,numactions,kwargs);
            self.lambda = lambda;
        end % function - SarsaLambda constructor

        function [ current_a ] = first(self,first_s)
            self.z = zeros(self.numstates,self.numactions);      % init eligibility vector
            current_a = first@rl.agents.base.Agent(self,first_s);
        end % first function

        function [ tderror ] = update( self, current_r, current_s, current_a, suppress )
            % suppress is an optional argument that suppresses learning of the Q-table
            % care needs to be taken as this isn't strictly principled in SarsaLambda
            if suppress % suppress prevents a new update from applying
                alpha = 0;
            else
                self.totsteps = self.totsteps + 1;
                alpha = self.alpha(self.totsteps);
            end
            self.z(self.prior_s,self.prior_a) = self.z(self.prior_s,self.prior_a) + 1;
                                                                    % increment eligibility element
            delta = (current_r + self.gamma*self.Q(current_s,current_a) - self.Q(self.prior_s,self.prior_a) );
                                                                % get td-error
            self.Q = self.Q + alpha*delta*self.z;                % td-update
            self.z = self.gamma*self.lambda*self.z;             % degrade eligibility vector
            notabsorbing = find(~self.absorbing);
            self.policy.update(notabsorbing , self.Q(notabsorbing,:), self.totsteps );
                                                                    % policy update
        end % function - update

        function [ tderror ] = lastupdate(self, current_r, current_s, suppress )
            % suppress is an optional argument that suppresses learning of the Q-table
            % care needs to be taken as this isn't strictly principled in SarsaLambda
            if suppress % suppress prevents a new update from applying
                alpha = 0;
            else
                self.totsteps = self.totsteps + 1;
                alpha = self.alpha(self.totsteps);
            end
            self.z(self.prior_s,self.prior_a) = self.z(self.prior_s,self.prior_a) + 1;
                                                                    % increment eligibility element
            delta = (current_r + self.gamma*0 - self.Q(self.prior_s,self.prior_a) );
                                                                    % get td-error
            self.Q = self.Q + alpha*delta*self.z;                % td-update
            self.z = self.gamma*self.lambda*self.z;             % degrade eligibility vector
            notabsorbing = find(~self.absorbing);
            self.policy.update(notabsorbing , self.Q(notabsorbing,:), self.totsteps );
                                                                    % policy update
        end % function - lastupdate

    end % methods
end % classdef
