classdef ActorCriticQP < rl.agents.discrete.Critic & handle
    properties (SetAccess='public')
        beta % get P table learning step size
        critic
    end % properties - public
    properties (Dependent)
        P % the policy weights table
    end % properties - public

    methods
        function self = ActorCriticQP(critic,numstates,numactions,beta,temperature,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            self = self@rl.agents.discrete.Critic(numstates,numactions,'policytype','softmax','temperature',temperature);
            if isa(beta,'function_handle')
                self.beta = beta;
            elseif isnumeric(beta)
                self.beta = @(i)(beta);
            else
                error('Inappropriate type of beta passed')
            end
            if kwargs.iskey('P')
                self.P = kwargs('P');
            else
                % default initial policy is unbiased
                self.P = zeros(self.numstates, self.numactions); 
            end % if - P defined
            % initialise the subagent
            self.critic = critic;
        end % function - ActorCriticQP constructor

        function self = set.P(self,val)
          self.Q = val;
        end
        function value = get.P(self)
          value = self.Q;
        end

        function [ first_a ] = first(self,first_s)
            first_a = first@rl.agents.base.Agent(self,first_s);
            self.critic.imitatefirst(first_s, first_a );
        end
        function [ tderror ] = update( self, current_r, current_s, current_a, suppress )
            % suppress is an optional argument that suppresses learning of the Q-table
            if suppress
                beta = 0;
            else
                beta = self.beta(self.totsteps);
            end % if - suppress
            self.critic.totsteps = self.totsteps;
            self.critic.imitatenext(current_r, current_s, current_a, suppress );
            rowQ = self.critic.Q(self.prior_s,:);                          % cache row of Qs
            self.P(self.prior_s,self.prior_a) = self.P(self.prior_s,self.prior_a) + beta*(rowQ(self.prior_a) - self.policy.weights(self.prior_s,:)*rowQ');
                                                                    % P-table update
            self.policy.update( self.prior_s, self.P(self.prior_s,:), self.totsteps );
                                                                   % update the policy to reflect this
        end % next function

        function [ tderror ] = lastupdate(self, current_r, current_s, suppress )
            % suppress is an optional argument that suppresses learning of the Q-table
            if suppress
                beta = 0;
            else
                beta = self.beta(self.totsteps);
            end
            self.critic.totsteps = self.totsteps;
            self.critic.lastupdate(current_r, current_s, suppress );
            rowQ = self.critic.Q(self.prior_s,:);
                                                      % cache row of Qs
            %self.P(self.prior_s,self.prior_a) = self.P(self.prior_s,self.prior_a) + beta*(rowQ(self.prior_a) - self.policy.weights(self.prior_s,:)*rowQ');
            self.P(self.prior_s,self.prior_a) = self.P(self.prior_s,self.prior_a) + beta*self.critic.advantage(self.prior_s,self.prior_a,self.policy.weights(self.prior_s,:));
                                                                    % P-table update
            self.policy.update( self.prior_s, self.P(self.prior_s,:), self.totsteps );
                                                                   % update the policy to reflect this
        end % last function

        % Q tables
        function [] = reset(self)
            % reset the self to zero learning.
            reset@rl.agents.discrete.Critic(self);
            self.critic.reset();
        end % function - reset

    end % methods
end % classdef
