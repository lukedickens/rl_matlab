classdef SoftMaxPolicy < rl.agents.discrete.Policy & handle
    properties (SetAccess='public')
        tempfunc
    end % properties - public

    methods

        function self = SoftMaxPolicy(numstates,numactions,varargin)
            %% The constructor method
            kwargs = rl.utils.dict(varargin{:});
            self = self@rl.agents.discrete.Policy(numstates,numactions,kwargs);
            if kwargs.iskey('temperature')
                temperature = kwargs('temperature');
                if isa(temperature,'function_handle')
                    self.tempfunc = temperature;
                elseif isnumeric(temperature)
                    self.tempfunc = @(i)(temperature);
                else
                    error('Inappropriate type of temperature passed');
                end
            end
        end % function - SoftMaxPolicy constructor

        function [ ] = update( self, states, vals, count )
            temp = self.tempfunc(count);
            rawweights = exp(vals./temp);
            if length(states) == 1
                self.weights(states,:) = rawweights/sum(rawweights);
            else
                self.weights(states,:) = rawweights./repmat(sum(rawweights,2),1,size(vals,2));
            end
        end % next function

    end % methods
end % classdef
