classdef Agent < handle
    properties
        policy
    end
    properties (Hidden=true)
        prior_s
        prior_a
        verbose = false;
    end % properties - protected, protected
    
    methods
        function self = Agent()
        end % function - Agent

        function [] = reset(self)
            self.policy.reset();
        end % function - reset

        % run the agent
        function [ ] = run_multiple( self, sim, numepisodes, maxsteps )
            % train agent on multiple runs of the passed in simulation.
            for i=1:numepisodes
                run( self, sim, maxsteps );
            end % for each episode
        end % function - run_multiple

        function [] = run( self, sim, maxsteps )
            % Runs a single episode of the passed in simulation.
            first_s = sim.init(); % get initial state
            self.first(first_s);
            for j=1:maxsteps
                % get next state and reward from sim
                [ current_r, current_s ] = sim.next(self.prior_a);
                if ~sim.terminal();
                    self.next(current_r,current_s);
                else
                    self.last(current_r,current_s);
                    break
                end % if
            end % for each step
        end % function - run

        % step by step functions
        function [ current_a ] = first(self,first_s)
            current_a = self.policy.choose(first_s);
            self.prior_s = first_s;
            self.prior_a = current_a;
        end % function - first

        function [ current_a ] = next( self, current_r, current_s, suppress )
            if nargin < 4
                suppress = false;
            end
            % choose action then update
            current_a = self.policy.choose(current_s);
            self.update(current_r,current_s,current_a, suppress);
            self.prior_s = current_s;
            self.prior_a = current_a;
        end % function - next

        function [ ] = last(self, current_r, current_s, suppress )
            if nargin < 4
                suppress = false;
            end
            self.lastupdate(current_r,current_s,suppress);
            % make sure the prior state and actions are not misused.
            self.prior_s = 0;
            self.prior_a = 0;
        end % function - last

        %% For imitation learning
        function [ prob] = imitatefirst(self,first_s,first_a)
            if nargout > 0
                prob = self.policy.weights(first_s,first_a);
            end
            self.first(first_s); % may need initialising (sarsa lambda does)
            % now overwrite initial action
            self.prior_a = first_a;
        end % function - first

        function [ prob ] = imitatenext(...
                self, current_r, current_s, current_a, suppress )
            if nargin < 5
                suppress = false;
            end
            % choose action then update
            if nargout > 0
                prob = self.policy.weights(current_s,current_a);
            end
            self.update(current_r,current_s,current_a, suppress);
            self.prior_s = current_s;
            self.prior_a = current_a;
        end % function - next

        function [ logprob qtrace ] = imitate_run(self,trace)
            qtrace=[];
            rewards = trace.rewards;
            states = trace.states;
            actions = trace.actions;
            tracelen = length(trace.states);
            if length(trace.actions) ~= tracelen-1
                error('Trace length states does not match that of actions');
            end
            if length(trace.rewards) ~= tracelen-1
                error('Trace length states does not match that of rewards');
            end
            logprob = log(self.imitatefirst(trace.states(1),trace.actions(1)));
            %qtrace(1,1)=self.Q(1,1);qtrace(1,2)=self.Q(1,2);qtrace(1,3)=self.Q(1,1);qtrace(1,4)=self.Q(2,2);
            count = 1;
            for t=2:tracelen-1
                % verbose agents output when operating
                if self.verbose
                    fprintf('.');
                end
                pre = logprob;
                logprob = logprob + log(self.imitatenext(trace.rewards(t-1),trace.states(t),trace.actions(t)));
                a=size(qtrace,1)+1;
                %qtrace(a,1)=self.Q(1,1);qtrace(a,2)=self.Q(1,2);qtrace(a,3)=self.Q(1,1);qtrace(a,4)=self.Q(2,2);
            end
            if self.verbose
                fprintf('*');
            end
            % last does not produce a probability but is still needed.
            self.last(trace.rewards(tracelen-1),trace.states(tracelen));
        end % function - imitaterun

        function [ logprob ] = imitate_run_multiple( self, traces )
            logprob = 0;
            for t=1:length(traces)
                logprob = logprob + self.imitate_run(traces{t});
            end
        end % function - imitate_run_multiple

    end % methods

    methods(Static)

        function [unbiased_policy] = unbiased_policy(absorbing,numactions)
            %%GETUNBIASEDPOLICY For every non-absorbing state (rows) we give equal probability to each action (columns)
            unbiased_policy = 1./numactions * ~absorbing'*ones(1,numactions);
        end

        function [ figurestr ] = getfigurestr(agenttype)
            agenttype = lower(agenttype);
            switch agenttype
            case 'sarsa'
                figurestr = 'sarsa';
            case 'sarsalambda'
                figurestr = 'sarsa(\lambda)';
            case 'modsarsalambda'
                figurestr = 'mod-sarsa(\lambda)';
            case 'qlearn'
                figurestr = 'Q-Learning';
            case 'rstd'
                figurestr = 'RSTD';
            case 'rssarsa'
                figurestr = 'RS-sarsa';
            case {'rsq', 'rsqlearn'}
                figurestr = 'RSQ-learning';
            case {'qp', 'actorcriticqp' }
                figurestr = 'actor-critic (QP)';
            case {'vp', 'actorcritic' }
                figurestr = 'actor-critic';
            case { 'sarsaxi' }
                figurestr = 'sarsa(\xi)';
            end
        end
    end % methods - static
end % classdef
