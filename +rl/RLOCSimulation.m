%%%%% is what Luke added for keeping rewards
classdef RLOCSimulation < handle & SimulationBase
    %% ----- SET UP
    properties (SetAccess = 'public')
        % ----- in constructor:
        model
        dyn_fn % fn: takes type 2 x and control u, returning [xdot xdot_x xdot_u] i.e. defines the dynamics of the system we act upon
        cost_fn % fn: takes type 3 x and control u, returns real value defining optimal control cost
        Ls
        allowed_starting_states
        % ----- in init
        startVec
        starting_x
        stepNumber
        %%XXX slowing me down
        collectTraj
        xTrajectory
        uTrajectory
        cTrajectory
        % ----- in next
        next_x      % type 3
        next_u
        next_state
        next_reward
        current_x   % type 3
        current_u
        current_state
%%XXX Code used to determine empirically the maximum/min values etc
%%        max_x
%%        min_x
%%        count
%%        avg_x
%%        var_x
        % speedup parameters
        numOfSteps
        numOfActions
        xdim
        udim
        dt
        maxtimeslice
        maxcost
        mincost
        target
    end % properties

    methods
        %% ----- CONSTRUCTOR
        function sim = RLOCSimulation(model, dyn_fn, cost_fn,maxtimeslice)
            sim = sim@SimulationBase();
            sim.model = model;
            % speedup parameters
            sim.numOfSteps = sim.model.numOfSteps;
            sim.numOfActions =  sim.model.numOfActions;
            sim.xdim = sim.model.xdim;
            sim.udim = sim.model.udim;
            sim.dt = sim.model.dt;
            sim.target = sim.model.target;
            % functions
            sim.dyn_fn = dyn_fn;
            sim.cost_fn = cost_fn;
            sim.setLs();
            sim.allowed_starting_states = [1 : model.numOfStates];
            sim.startVec = [];
            sim.maxtimeslice = maxtimeslice;
            sim.maxcost = 2000000;
            sim.mincost = 0;
            sim.collectTraj = true;
            % historioes
            sim.purge();
        end % RLOCSimulation

        %% ----- OBTAINING L MATRICES
        function [] = setLs(sim)
            %model = sim.model;
            V = zeros(sim.xdim, sim.xdim, sim.numOfSteps, sim.numOfActions); % same size as Q since V(last) = Qf
            for cc = 1 : sim.numOfActions
                V(:,:,sim.numOfSteps,cc) = sim.model.Qf;                     % for each action last value of V = Qf
            end
            sim.Ls = zeros(sim.udim, sim.xdim, sim.numOfSteps-1, sim.numOfActions);
            for ii = 1 : sim.numOfActions
                [xdot, xdot_x, xdot_u] = sim.dyn_fn(sim.model.xCntr(:,ii)-sim.target, sim.model.u0); % sim.model xCntre in real space
                A = xdot_x*sim.dt + eye(sim.xdim);                   % eg 4x4 for arm
                B = xdot_u*sim.dt;                                     % eg 4x2 for arm
                for jj = sim.numOfSteps : -1 : 2                         %!!!NB!!! MUST MULTIPLY Q AND R BY dt HERE TO BE CONSISTENT WITH TODOROV'S iLQR!!!!
                    V(:,:,jj-1,ii) = sim.dt.*(sim.model.Q) + A'*V(:,:,jj,ii)*A - A'*V(:,:,jj,ii)*B* ((sim.dt.*(sim.model.R) + B'*V(:,:,jj,ii)*B) \ B'*V(:,:,jj,ii)*A); % eg 2x4xnumOfSteps for arm
                end
                for jj = 2 : sim.numOfSteps
                    sim.Ls(:,:,jj-1,ii) = (sim.dt.*(sim.model.R) + B'*V(:,:,jj,ii)*B) \ B'*V(:,:,jj,ii)*A; % eg 2x4xnumOfSteps-1 for arm
                end
            end
        end

        %% ----- starting new episode (resetting values)
        function [state] = init(sim)
            sim.newepisode();
            if length(sim.allowed_starting_states) == 1
                state = sim.allowed_starting_states(1);
            else
                state = randsample(sim.allowed_starting_states,1);
            end
            sim.startVec = [sim.startVec state]; % NB GETS BUILT UP OVER ENTIRE numOfTraces!!!!! (If needs to be used for another agent afterwards, use purge(sim) to reset to empty list)
            sim.current_x = sim.model.x0(:,state); % x0 is [model.xdim x model.numOfStates] in size
            sim.current_u = sim.model.u0;
            sim.stepNumber = 1;
%%XXX slowing me down
            if sim.collectTraj
                sim.xTrajectory = [];
                sim.uTrajectory = [];
                sim.cTrajectory = [];
            end
        end % init

        %% ----- resetting startVec
        function purge(sim)
            purge@SimulationBase(sim);
            sim.startVec = [];
%%XXX Code used to determine empirically the maximum/min values etc
%%            try
%%                sim.max_x = zeros(sim.xdim,1);
%%                sim.min_x = zeros(sim.xdim,1);
%%                sim.avg_x = 0;
%%                sim.var_x = 0;
%%                sim.count = 0;
%%            catch
%%            end
        end % purge

        %% ----- takes subjective action (integer) then ticks over the dynamic system until the subjective state s changes
        function [reward, nextState] = next(sim, action)
            sim.current_state = sim.model.getState(sim.current_x);
            sim.next_state = sim.current_state;
            reward = 0;
            timeslice = 0;
            while (sim.current_state == sim.next_state) && (sim.stepNumber <= (sim.numOfSteps-2)) && (timeslice <= sim.maxtimeslice)
                timeslice = timeslice+1;
                if sim.stepNumber == 1
                    immediate_cost = sim.dt * sim.cost_fn(sim.current_x-sim.target, sim.current_u, sim.numOfSteps, sim.model.Q, sim.model.Qf, sim.model.R); % on the first itartion control is zero
                    reward = reward - immediate_cost;
                end
                [xdot] = sim.dyn_fn(sim.current_x, sim.current_u);
                sim.next_x = sim.current_x + sim.dt*xdot;
                next_x_offset = sim.next_x - sim.target;
                if sum(isnan(sim.next_x))>0
                    error('RLOC:NaNx','next_x contains NaN!') % 1st part is the name of the error, second is what gets printed on the screen
                end
                sim.next_u = -sim.Ls(:,:,sim.stepNumber+1,action)*next_x_offset;
                sim.next_state = sim.model.getState(sim.next_x);
                immediate_cost = sim.dt * min(sim.maxcost,max(sim.mincost,sim.cost_fn(next_x_offset, sim.next_u, sim.numOfSteps, sim.model.Q, sim.model.Qf, sim.model.R)));
                reward = reward - immediate_cost;
                if sim.collectTraj
                    sim.xTrajectory = [sim.xTrajectory sim.next_x];
                    sim.uTrajectory = [sim.uTrajectory sim.next_u];
                    sim.cTrajectory = [sim.cTrajectory (-immediate_cost)];
                end
                sim.current_x = sim.next_x;
                sim.current_u = sim.next_u;
                sim.stepNumber = sim.stepNumber + 1;
            end % while loop

            if (sim.stepNumber == (sim.numOfSteps-1))
                [xdot] = sim.dyn_fn(sim.current_x, sim.current_u);
                sim.next_x = sim.current_x + sim.dt*xdot;
                immediate_cost = min(sim.maxcost,max(sim.mincost,sim.cost_fn(next_x_offset, NaN, NaN, sim.model.Q, sim.model.Qf, sim.model.R))); % penalty for final position
                %%XXX slowing me down
                if sim.collectTraj
                    sim.xTrajectory = [sim.xTrajectory sim.next_x];
                    sim.uTrajectory = [sim.uTrajectory sim.next_u];
                    sim.cTrajectory = [sim.cTrajectory (-immediate_cost)];
                end
                reward = reward - immediate_cost;
                sim.stepNumber = sim.stepNumber + 1;
            end

            sim.storereward(reward);
            nextState = sim.next_state;
            sim.next_reward = reward;
        end % next

        %% ----- keeps track if reached end of numOfSteps for Agent
        function [ finished ] = terminal(sim)
            finished = (sim.stepNumber == sim.numOfSteps);
        end % function - terminal

    end % methods
end % classdef
