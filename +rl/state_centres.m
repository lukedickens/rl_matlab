function [ centres ] = state_centres(partitions)
    % determine how many dimensions remain
    numdims = length(partitions);
    % head is a list of the divisions for this dimension
    headpartition = partitions{1};
    % numdivs is the number of divisions for this dimension
    numdivs = length(headpartition);
    % cpoints are the centres of partitions for this dimension
    cpoints = (headpartition(2:numdivs)+headpartition(1:numdivs-1))/2;
    if numdims > 1
        % tail is a collection (cell) of lists of the divisions for remaining dimensions
        tailpartitions = {partitions{2:numdims}};
        % RECURSE - call this function again, but with the remaining 'tail' dimensions
        tailcentres = state_centres(tailpartitions);
        % cross product between this dimensions points and the result of the recursive call
        centres = cross_prod(cpoints,tailcentres);
    else
        % there is only this dimension remaining, so the centres are a simple list
        centres = reshape(cpoints,numdivs-1,1);
    end
end

function [ product ] = cross_prod(points,centres)
%CROSS_PROD - cross product every member of points, with every row of centres
    product = [];
    for i=1:size(centres,1)
        for p=points
            product = [product; [p centres(i,:)]];
        end
    end     
end
