classdef AgentFASarsa < handle
    properties (SetAccess='public')
        numstates
        numactions
        gamma
        epsilon
        alpha
        totsteps
        absorbing % boolean vector of the states that are absorbing
        Q % this is an sdim x numactions matrix, weight vector for action a is Q(:,a)
        %
        sdim
%    end % properties - public
%    properties (SetAccess='protected',GetAccess='protected')
        prior_s
        prior_a
    end % properties - protected, protected
    
    methods
        function agent = AgentFASarsa(sdim,numactions,gamma,epsilon,alpha,varargin)
            %% The constructor method
            agent.sdim = sdim;
            agent.numactions = numactions;
            agent.gamma = gamma;
            if isa(epsilon,'function_handle')
                agent.epsilon = epsilon;
            elseif isnumeric(epsilon)
                agent.epsilon = @(i)(epsilon);
            else
                error('Inappropriate type of epsilon passed')
            end
            if isa(alpha,'function_handle')
                agent.alpha = alpha;
            elseif isnumeric(alpha)
                agent.alpha = @(i)(alpha);
            else
                error('Inappropriate type of alpha passed')
            end
            agent.totsteps = 0;

            if nargin > 5
                % if there is a 6th argument assume it is a dictionary
                kwargs = varargin{1};
            else
                % otherwise create and empty dictionary
                kwargs = dict;
            end % if - optional args
            if kwargs.iskey('Q')
                % specified input values for Q
                agent.Q = kwargs('Q');
            else
                % default state-action value function is zero everywhere
                agent.Q = zeros(agent.sdim, agent.numactions); 
            end % if - Q defined
        end % AgentFASarsa

        function [ final_Q ] = run_multiple( agent, simulation, numepisodes, maxsteps )
            % train agent on multiple runs of the passed in simulation.
            for i=1:numepisodes
                run( agent, simulation, maxsteps );
            end % for each episode
            % cannot output the optimal policy so outputs the final stack of q vectors Q.
              final_Q = agent.Q;
        end % run_multiple

        function [] = run( agent, simulation, maxsteps )
            % Runs a single episode of the passed in simulation.
            first_s = simulation.init(); % get initial state
            agent.first(first_s);
            for j=1:maxsteps
                agent.totsteps = agent.totsteps + 1;
                % get next state and agent.current_r from simulation
                [ current_r, current_s ] = simulation.next(agent.prior_a);
                if ~simulation.terminal();
                    agent.next(current_r,current_s);
                else
                    agent.last(current_r,current_s);
                    break
                end % if
            end % for each step
        end % run function

        function [ current_a ] = first(agent,first_s)
            current_a = agent.choose(first_s);
            agent.prior_s = first_s;
            agent.prior_a = current_a;
        end % first function

        function [ current_a ] = next( agent, current_r, current_s )
            agent.totsteps = agent.totsteps + 1;
            % choose action then update
            current_a = agent.choose(current_s);
            % evaluate the tderror
            prior_q = agent.prior_s'*agent.Q(:,agent.prior_a);
            delta = agent.tderror(current_r,prior_q,current_s,current_a);
            try
                % the temporal difference update
                agent.Q(:,agent.prior_a) = ...
                    prior_q + agent.alpha(agent.totsteps)*delta;
                if ~any(any(isfinite(agent.Q)))
                    error('The Q matrix is not all finite');
                end
            catch err
                display(['Agent Update throws an error: ' err.message]);
                display(['agent.Q: ' mat2str(agent.Q) ] );
                display(['agent.Q(agent.prior_s,agent.prior_a): ' num2str(agent.Q(:,agent.prior_a)) ] );
                display(['current_s: ' num2str(current_s) ] );
                display(['current_a: ' num2str(current_a) ] );
                display(['agent.Q(current_s,current_a): ' num2str(agent.Q(:,current_a)) ] );
                display(['prior_q: ' num2str(prior_q) ] );
                display(['agent.alpha(agent.totsteps): ' num2str(agent.alpha(agent.totsteps)) ] );
                display(['agent.gamma: ' num2str(agent.gamma) ] );
                rethrow(err);
            end
            % no need to update the policy, as this is calculated on the fly from agent.Q
            agent.prior_s = current_s;
            agent.prior_a = current_a;
        end % next function

        function [ ] = last(agent, current_r, current_s )
            dummy_s = zeros(agent.sdim,1);
            dummy_a = 0;
            agent.totsteps = agent.totsteps + 1;
            prior_q = agent.prior_s'*agent.Q(:,agent.prior_a);
            % evaluate the tderror
            delta = agent.tderror(current_r,prior_q,dummy_s,dummy_a);
            % update the Q-table
            agent.Q(:,agent.prior_a) = ...
                prior_q + agent.alpha(agent.totsteps)*delta;
            % no need to update the policy, as this is calculated on the fly from agent.Q
            % make sure the prior state and actions are not misused.
            agent.prior_s = dummy_s;
            agent.prior_a = dummy_a;
        end % last function

        function [] = reset(agent)
            % reset the agent to zero learning.
            agent.totsteps = 0;
            agent.Q = zeros(agent.numstates, agent.numactions); 
        end

        % helper function - select an action
        function [ a ] = choose(agent,s)
            % s'*Q gives the Q-values Q(s,a) for each action a as row vec
            [val,idx] = max(s'*agent.Q);
            % evaluate current epsilon
            epsilon = agent.epsilon(agent.totsteps);
            % weights are the probabilities for each action
            weights = epsilon/agent.numactions*ones(1,agent.numactions);
            weights(idx) = 1-epsilon+epsilon/agent.numactions;
            % display(['choosing with weights: ' mat2str(weights) ', sum = ' num2str(sum(weights))] ); % DEBUG
            a = find(mnrnd(1,weights),1);
            % display(['chosen: ' num2str(a) ]); % DEBUG
        end

        % helper function - determine td error
        function [delta] = tderror(agent,r,prior_q,post_s,post_a)
            if post_a == 0
                post_q = 0;
            else
                post_q = post_s'*agent.Q(:,post_a);
            end
            delta = r + agent.gamma*post_q - prior_q;
        end

    end % methods
end % classdef
