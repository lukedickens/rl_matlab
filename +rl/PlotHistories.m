function PlotHistories(histories,expnames,kwargs)
    delete(gcf);
    hold on;
    %% colour selection
    numexps = size(histories,2);
    if kwargs.iskey('colours')
        colours = kwargs('colours');
    else
        colours = hsv(numexps);
    end
    %% plot selection
    if kwargs.iskey('plottype')
        plottype = kwargs('plottype');
    else
        plottype = 'stepaccum';
    end
    if strcmp(plottype,'stepaccum')
        StepAccum(histories,expnames,colours);
    elseif strcmp(plottype,'episodeaccum')
        EpisodeAccum(histories,expnames,colours);
    elseif strcmp(plottype,'aveepisodes')
        if kwargs.iskey('batchsize')
            batchsize = kwargs('batchsize');
        else
            batchsize = floor(length(histories{1})/10);
        end
        AveEpisodes(histories,expnames,batchsize,colours);
    end
    %% additional settings for figure
    if kwargs.iskey('xlim')
        xlim(kwargs('xlim'));
    end
    if kwargs.iskey('ylim')
        ylim(kwargs('ylim'));
    end
    if kwargs.iskey('plotfile')
        print(gcf,'-depsc',kwargs('plotfile'));
    end
end

function StepAccum(histories,expnames,colours)
    numexps = size(histories,2);
    for e=1:numexps 
        history = histories{e};
        numepisodes = size(history,2);
        allrewards = [];
        totreward = 0;
        totsteps = 0;
        for i=1:numepisodes
            reward_trace = history{i};
    %        totreward = totreward + sum(reward_trace);
    %        totsteps = totsteps + length(reward_trace);
            allrewards = [allrewards reward_trace];
        end

        totreward = sum(allrewards);
        totsteps = length(allrewards);
        aggrewards = [];
        agg = 0;
        for r=allrewards;
            agg = agg+r;
            aggrewards = [ aggrewards agg];
        end
        h = plot([1:totsteps]',aggrewards);
        colour = colours(e,:);
        set(h,'Color',colour);
        H(e) = h;
    end
    h_legend=legend(H,expnames,'Location','SouthEast');
    set(h_legend,'FontSize',12);
hold off; 
end


function EpisodeAccum(histories,expnames,colours)
    numexps = size(histories,2);
    for e=1:numexps 
        history = histories{e};
        numepisodes = size(history,2);
        accumepisode = [];
        accum = 0;
        for i=1:numepisodes
            reward_trace = history{i};
            accum = accum + sum(reward_trace);
            accumepisode(i) = accum;
        end

        h = plot([1:numepisodes]',accumepisode);
        colour = colours(e,:);
        set(h,'Color',colour);
        H(e) = h;
    end
    h_legend=legend(H,expnames,'Location','SouthEast');
    set(h_legend,'FontSize',12);
    hold off; 

end

function AveEpisodes(histories,expnames,batchsize,colours)
    numexps = size(histories,2);
    for e=1:numexps
        history = histories{e};
        numepisodes = size(history,2);
        % truncate the episodes appropriately
        numbatches = floor(numepisodes/batchsize);
        numepisodes = numbatches*batchsize;
        perepisode = [];
        for i=1:numepisodes
            reward_trace = history{i};
            perepisode(i)= sum(reward_trace);
        end
        perbatch = mean(reshape(perepisode,numbatches,batchsize),2);
        errbars = std(reshape(perepisode,numbatches,batchsize),0,2)/sqrt(batchsize);
        h = errorbar(batchsize*[1:numbatches]',perbatch,errbars,'o');
        colour = colours(e,:);
        set(h,'Color',colour);
        H(e) = h;
    end
    h_legend=legend(H,expnames,'Location','SouthEast');
    set(h_legend,'FontSize',12);
    hold off; 
end

