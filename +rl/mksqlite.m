% A MATLAB interface to SQLite mksqlite
% SQLite is an embedded SQL engine, which no server SQL databases
% Within is able to manage files. Mksqlite provides the interface
% To this SQL database.
%
% General Usage:
%  dbid = mksqlite([dbid, ] SQLCommand [, Argument])
% The parameter dbid is optional dbid and is only needed when using
% Multiple databases to be worked simultaneously. If dbid
% Omitted, the database automatically No. 1 is used.
%
% Function calls:
%  mksqlite('open', 'databasefile')
% or
%  dbid = mksqlite(0, 'open', 'databasefile')
% Open the database file with the filename "database file". when a
% Such a file does not exist, create it.
% If one is given dbid and this is an already open
% Refers database, it will be closed before command execution. at
% 0, specifying the dbid dbid comes back the next free.
%
%  mksqlite('close')
% or
%  mksqlite(dbid, 'close')
% or
%  mksqlite(0, 'close')
% Closes a database file. If you specify a dbid is the database
% Closed. If you specify the dbid of 0 all open databases are closed.
% 
%
%  mksqlite('SQL command')
% or
%  mksqlite(dbid, 'SQL command')
% Executes SQL command.
%
% Example:
%  mksqlite('open', 'testdb.db3');
% Result = mksqlite ('select * from test table');
%  mksqlite('close');
% Reads all fields of the table "test table" in the database "testdb.db3"
% In the variable "result" field.
%
% Example:
%  mksqlite('open', 'testdb.db3')
%  mksqlite('show tables')
%  mksqlite('close')
% Shows all tables in the database "testdb.db3" to.
%
% (C) 2008 by Martin Kortmann <mail@kortmann.de>
%

