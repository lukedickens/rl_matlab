display('Unit testing the ParameterSearcher object');

[x,y] = meshgrid(0:0.05:1,0:0.05:1);
z = -(x-0.3).^2-(y-0.7).^2;

f = @(x,y)(-(x-0.3).^2-(y-0.7).^2 + 0.2*randn());


gamma = 1;
% names
names{1} = 'x';
names{2} = 'y';
% starting params
params(1) = 0.1;
params(2) = 0.1;
% steps
steps(1) = 0.1;
steps(2) = 0.1;
% bounds
bounds(1,:) = [0,1];
bounds(2,:) = [0,1];

cycles = 3;

searchargs = dict;
%searchargs('verbose') = true;
paramsearcher = ParameterSearcher(names,params,steps,bounds,cycles,searchargs);

[params,terminate] = paramsearcher.suggest_params();
while ~terminate
    %display(['params: ' mat2str(params)]);
    x = params(1);
    y = params(2);
    for i=1:8
        vals(i) = f(x,y);
    end
    display(['x: ' num2str(x) ...
    ', y: ' num2str(y) ...
    ', mean(accumrewards): ' num2str(mean(vals)) ] );
    %%display(['mean(accumrewards): ' num2str(mean(accumrewards)) ] );
    paramsearcher.post_scores(vals);
    [params, terminate] = paramsearcher.suggest_params();
end

[params, terminate] = paramsearcher.suggest_params()




