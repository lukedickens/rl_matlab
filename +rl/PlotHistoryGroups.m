function PlotHistoryGroups(historygroups,expnames,kwargs)
    delete(gcf);
    if nargin < 3
        kwargs = dict;
    end
    numexps = size(historygroups,2);
    % if only one history per experiment then run simpler plotting function
    if size(historygroups{1},2) == 1
        for e=1:numexps
            histories{e} = historygroups{e}{1};
        end % for
        PlotHistories(histories,expnames,kwargs);
        return
    end % if

    %% batchsize selection
    if kwargs.iskey('batchsize')
        batchsize = kwargs('batchsize');
    else
        batchsize = floor(size(historygroups{1}{1},2)/10);
    end % if
    %% colour selection
    if kwargs.iskey('colours')
        colours = kwargs('colours');
    else
        colours = hsv(numexps);
    end % if
    %% plot selection
    if kwargs.iskey('plottype')
        plottype = kwargs('plottype');
    else
        plottype = 'episodeaccum';
    end % if
    if strcmp(plottype,'episodeaccum')
        EpisodeAccum(historygroups,expnames,batchsize,colours,kwargs);
    else
        error('Unrecognised plottype');
    end % if
    %% additional settings for figure
    if kwargs.iskey('xlim')
        xlim(kwargs('xlim'));
    end % if
    if kwargs.iskey('ylim')
        ylim(kwargs('ylim'));
    end % if
    if kwargs.iskey('plotfile')
        print(gcf,'-depsc',kwargs('plotfile'));
    end % if
end


function EpisodeAccum(historygroups,expnames,batchsize,colours,kwargs)
    if nargin < 5
        kwargs = dict;
    end
    if kwargs.iskey('legendloc')
        legendloc = kwargs('legendloc');
    else
        legendloc = 'SouthEast';
    end
    numexps = size(historygroups,2);
    hold on;
    for e=1:numexps
        histories = historygroups{e};
        numtrials= size(histories,2);
        minnumepisodes = size(histories{1},2);
        for h=1:numtrials
            history = histories{h};
            numepisodes = size(history,2);
            accumepisodes = [];
            accum = 0;
            for i=1:numepisodes
                reward_trace = history{i};
                accum = accum + sum(reward_trace);
                accumepisodes(i) = accum;
            end
            minnumepisodes = min(numepisodes,minnumepisodes);
            groupaccumscell{h}=accumepisodes;
        end
        % take the smallest length history, then truncate further to ensure a round number of batches
        numbatches = floor(minnumepisodes/batchsize);
        usableepisodes = numbatches*batchsize;
        % poi = points of interest (numbers of episodes at which we want to measure performance)
        poi = linspace(batchsize,usableepisodes,numbatches);
        % now truncate every accumulated history to the same length and insert into matrix
        scores = [];
        for h=1:numtrials
            scores = [scores; groupaccumscell{h}(poi)];
        end
        meanscores = [0 mean(scores)];
        errbars = [0 std(scores)/sqrt(batchsize)];
        h = errorbar([0 poi],meanscores,errbars);
        colour = colours(e,:);
        set(h,'Color',colour);
        H(e) = h;
    end

    h_legend=legend(H,expnames,'Location',legendloc);
    set(h_legend,'FontSize',12);
    hold off; 

end


