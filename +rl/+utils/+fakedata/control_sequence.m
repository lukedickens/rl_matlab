function [ X, UY ] = control_sequence(A,B,C,u,eplength,numeps,x0dist,noisex,noisey)
    % A state transition matrix
    % C obs matrix
    % eplength is number of steps in episode (called in Ghahramani T)
    % numeps is number of episode (Ghahramani says N=numeps*eplength)
    % noisex - distribution of noise for column vector x (called w')
    % noisey - dist for noise for column vector y (called v')
    [rowsa,colsa] = size(A);
    if rowsa ~= colsa
        error('A is not square');
    end
    [rowsb,colsb] = size(B);
    lengthu = length(u());
    if rowsb ~= colsa
        error('B is not appropriate for x');
    end
    if lengthu ~= colsb
        error('B is not appropriate for u');
    end
    [lengthy,lengthx] = size(C);
    if rowsa ~= lengthx
        error('C is not right size');
    end
    if nargin < 9
        %noisey = @() randn(lengthy,1);
        noisey = @() 0.1*randn(lengthy,1);
    end
    if nargin < 8
        %noisex = @() randn(lengthx,1);
        noisex = @() 0.1*randn(lengthx,1);
    end
    for e=1:numeps
        for i=1:eplength
            t = (e-1)*eplength+i; % the row id of the very next data point in our big X and Y vectors (stacks of episodes)
            if i==1
                Xprev = x0dist(); 
            end
            Uprev = u();
            Xt = A*Xprev + B*Uprev + noisex();
            U(t,:) = Uprev'; % the control vector
            Y(t,:) = [(C*Xt + noisey())'];
            X(t,:) = Xt'; % 1x2
            Xprev = Xt;
        end
    end
    UY = [U Y];
end


