function [ X, Y ] = affine_io(N,A,varargin)
    % A affine transformation matrix
    kwargs = dict(varargin{:});
    if kwargs.iskey('X')
        X = kwargs('X');
    else
        if kwargs.iskey('xgen')
            xgen = kwargs('xgen');
            X = xgen(N);
        else
            if kwargs.iskey('xmean')
                xmean = kwargs('xmean');
                if size(A,2) ~= length(xmean)
                    error('Matrix A is not compatible with supplied xmean');
                end
            else
                xdim = kwargs.get('xdim',size(A,2));
                xmean = zeros(1,xdim);
            end
            xcov = kwargs.get('xcov',eye(xdim));
            xR = chol(xcov);
            X = repmat(xmean,N,1) + randn(N,xdim)*xR;
        end
    end
    shift = kwargs.get('shift',zeros(1,xdim));
    if kwargs.iskey('noisegen')
        noisegen = kwargs('noisegen');
        E = repmat(shift,N,1) + noisegen(N);
    else
        noisecov = kwargs.get('noisecov',eye(xdim));
        noiseR = chol(noisecov);
        E = repmat(shift,N,1) + randn(N,xdim)*noiseR;
    end
    Y = X*A' + E;
end
