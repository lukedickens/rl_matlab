%[X,Y] =fake_data([0.9,0;0,-0.9],[3,-1.5],20,5,@() randn(2,1) + [3;-3])
function [ X, Y ] = sequence(A,C,eplength,numeps,x0dist,noisex,noisey)
    % A state transition matrix
    % C obs matrix
    % eplength is number of steps in episode (called in Ghahramani T)
    % numeps is number of episode (Ghahramani says N=numeps*eplength)
    % noisex - distribution of noise for column vector x (called w')
    % noisey - dist for noise for column vector y (called v')
    [rowsa,colsa] = size(A);
    if rowsa ~= colsa
        error('A is not square');
    end
    [lengthy,lengthx] = size(C);
    if rowsa ~= lengthx
        error('C is not right size');
    end
    if nargin < 7
        noisey = @() randn(lengthy,1);
    end
    if nargin < 6
        noisex = @() randn(lengthx,1);
    end
    for e=1:numeps
        i = 0;
        for i=1:eplength
            t = (e-1)*eplength+i; % the row id of the very next data point in our big X and Y vectors (stacks of episodes)
            if i==1
                Xt = x0dist();
            else
                Xt = (A*X(t-1,:)' + noisex());
            end
            Y(t,:) = (C*Xt + noisey())';
            X(t,:) = Xt';
        end
        
    end
end
