function [ path ] = get_path(dir, fstem, ext, varargin)
    kwargs = rl.utils.dict(varargin{:});
    ossep = kwargs.get('ossep', '/'); % for windows replace with '\\'
    path = sprintf(...
        '%s%s%s.%s', dir, ossep, fstem, ext);
end
