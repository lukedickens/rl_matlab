function [ result ] = ste(data,flag,dim)
% STE - the standard error function
% options:
% [result] = ste(data)
% [result] = ste(data,flag)
% [result] = ste(data,flag,dim)
% see std for details of calling arguments flag and dim.
    if nargin < 2, flag = 0; end;
    if nargin < 3, dim = 1; end;
    result = std(data,flag,dim)./sqrt(size(data,dim));
end
