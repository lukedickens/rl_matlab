classdef CardDeck < handle

    properties (SetAccess=protected)
        dealt        
    end % properties - protected
    
    methods 

        function self = CardDeck()
            self.shuffle();
        end % function - constructor
        function card = deal(self)
            while ~all(self.dealt) 
                i = randi(1,52);
                suit = mod(i,4)+1;
                if ~self.dealt(i)
                    self.dealt(i) = 1;
                    val = i -(suit-1)*13;
                    card = [suit,val];
                    break;
                end
            end
        end
        function shuffle(self)
            dealt = zeros(1,52);
        end % function

        function show(self,card)
            switch card(1)
            case 1
                suitstr = 'Hearts';
            case 2
                suitstr = 'Clubs';
            case 3
                suitstr = 'Diamonds';
            case 4
                suitstr = 'Spades';
            end
            switch card(2)
            case 1
                valstr = 'A';
            case 13
                valstr = 'K';
            case 12
                valstr = 'Q';
            case 11
                valstr = 'J';
            otherwise
                valstr = num2str(card(2));
            end

        end % function

    end % methods
    
end % classdef 

