%% create the world
[ model ] = GridWorld2()
VisualiseGrid(model,model.statenames ) % show it
simulation = GetSimulation(model)

% choose parameters
gamma = 0.9   
numepisodes = 1000
maxsteps = 100
policy = rl.algorithms.GetUnbiasedPolicy(simulation.absorbing,simulation.A)
[Q] = rl.algorithms.MonteCarloEstimation(simulation,gamma,numepisodes,maxsteps,policy)
% can extract the value function if we like (for visualisation)
V = sum(Q .* policy,2)
rl.algorithms.VisualiseGrid(model,V ) % show it


% update the policy with an eGreedy policy
epsilon = 0.1
[policy] =  rl.algorithms.eGreedyPolicyFromQ(Q, epsilon, simulation.absorbing)
[ dominant_actions ] = rl.algorithms.GetDominantActions(policy, simulation.actionnames)
rl.algorithms.VisualiseGrid(model,dominant_actions )

% estimate it's value again
[Q] = rl.algorithms.MonteCarloEstimation(simulation,gamma,numepisodes,maxsteps,policy)
V = sum(Q .* policy,2)
rl.algorithms.VisualiseGrid(model,V ) % show it

% update the policy with an eGreedy policy
epsilon = 0.1
[policy] =  rl.algorithms.eGreedyPolicyFromQ(Q, epsilon, simulation.absorbing)
[ dominant_actions ] = rl.algorithms.GetDominantActions(policy, simulation.actionnames)
rl.algorithms.VisualiseGrid(model,dominant_actions )

%%%% Doing the same thing with montecarlo batch optimisation
gamma = 0.9   
numbatches = 4
numepisodes = 4000
maxsteps = 15
epsilon = 0.1
[policy] = rl.algorithms.MonteCarloBatchOptimisation( simulation, gamma, epsilon, numbatches, numepisodes, maxsteps)
[ dominant_actions ] = rl.algorithms.GetDominantActions(policy, simulation.actionnames, simulation.absorbing)
rl.algorithms.VisualiseGrid(model,dominant_actions )

%% again if needed using existing policy
[policy, Q] = rl.algorithms.MonteCarloBatchOptimisation( simulation, gamma, epsilon, numbatches, numepisodes, maxsteps, policy)
[ dominant_actions ] = rl.algorithms.GetDominantActions(policy, simulation.actionnames, simulation.absorbing)
rl.algorithms.VisualiseGrid(model,dominant_actions )

%% visualise the value function
V = sum(Q .* policy,2)
rl.algorithms.VisualiseGrid(model,V )


%%%% Doing the same thing with montecarlo iterative optimisation
gamma = 0.9   
alpha = 0.1
numepisodes = 100
maxsteps = 15
epsilon = 0.1
[policy] = rl.algorithms.MonteCarloIterativeOptimisation( simulation, gamma, epsilon, alpha, numepisodes, maxsteps)
[ dominant_actions ] = rl.algorithms.GetDominantActions(policy, simulation.actionnames, simulation.absorbing)
rl.algorithms.VisualiseGrid(model,dominant_actions )

%% again if needed using existing policy
[policy, Q] = rl.algorithms.MonteCarloIterativeOptimisation( simulation, gamma, epsilon, alpha, numepisodes, maxsteps, policy)
[ dominant_actions ] = rl.algorithms.GetDominantActions(policy, simulation.actionnames, simulation.absorbing)
rl.algorithms.VisualiseGrid(model,dominant_actions )

%% visualise the value function
V = sum(Q .* policy,2)
rl.algorithms.VisualiseGrid(model,V )

