classdef ParameterSearcher < handle
    properties (SetAccess='public')
        terminate % flag terminate parameter search
        linesearcher % when in line search this holds the linesearcher object
        params % current params
        numparams % number of parameters
        names % collection of parameter names
        steps % collection of starting steps
        bounds % collection of bounds
        index % index of parameter currently being searched
        cycles % remaining cycles
        verbose
    end % properties - public

    methods
        function self = ParameterSearcher(names,params,steps,bounds,cycles,varargin)
            % The constructor method
            self.names = names;
            self.params = params;
            self.numparams = length(params);
            self.steps = steps;
            if ~self.numparams == length(self.steps)
                error('steps vector is the wrong length for params');
            end
            self.bounds = bounds;
            if ~( (self.numparams == size(self.bounds,1)) & (size(self.bounds,2) == 2) )
                error('bounds matrix is the wrong dimension for params');
            end
            self.cycles = cycles;
            self.index = 1;
            self.terminate = false;
            self.set_linesearcher();
            kwargs = rl.utils.dict(varargin{:});
            if kwargs.iskey('verbose')
                self.verbose = kwargs('verbose');
            else
                self.verbose = false;
            end % if - set self.verbose

        end % function - ParameterSearcher constructor


        function [ params, terminate ] = suggest_params(self)
            if ~self.terminate
                [newparam,lineterminate] = self.linesearcher.suggest_param();
                self.params(self.index) = newparam;
                while lineterminate
                    if self.index == self.numparams
                        self.cycles = self.cycles -1;
                    end
                    if self.cycles > 0
                        self.index = rem(self.index,self.numparams) + 1;
                        self.set_linesearcher();
                        [newparam, lineterminate] = self.linesearcher.suggest_param();
                        self.params(self.index) = newparam;
                        self.steps(self.index) = self.linesearcher.step;
                    else
                        lineterminate = false; % this guarantees termination of the while loop
                        self.terminate = true;
                    end
                end % if - lineterminate
            end % if - not terminated
            params = self.params; 
            terminate = self.terminate;
        end % function - suggest_params

        function [score,err] = post_scores(self,scores)
            if ~self.terminate
                [score,err] = self.linesearcher.post_scores(scores);
            else
                error('Cannot post scores. Parameter searcher has terminated');
            end % if
        end % function - post_scores

        function [] = post_score(self,score,err)
            if ~self.terminate
                self.linesearcher.post_score(score,err);
            else
                error('Cannot post score. Parameter searcher has terminated');
            end % if
        end % function - post_score

        function [] = set_linesearcher(self)
            kwargs = dict;
            kwargs('verbose') = self.verbose;
            try
                self.linesearcher = LineSearcher(self.params(self.index),self.steps(self.index),self.bounds(self.index,:),kwargs);
            catch err
                display(['self.index = ' num2str(self.index)]);
                display(['size(self.params) = ' mat2str(size(self.params))]);
                display(['size(self.steps) = ' mat2str(size(self.steps))]);
                display(['size(self.bounds) = ' mat2str(size(self.bounds))]);
                rethrow(err);
            end
        end

    end % methods
end % classdef

