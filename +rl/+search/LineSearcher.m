classdef LineSearcher < handle
    properties (SetAccess='public')
        % the three main parameters (built during bracketing and revised during searching)
        params  % currently accepted params
        scores  % currently accepted scores
        errs % currently accepted errs
        % the parameter passed out by suggest_param
        param   % tracking parameter

        % used for bracketing
        bounds  % the bounds for the parameter
        param0  % starting parameter
        step0   % starting stepsize - magnitude and direction in which to try second paramter
        step    % tracking stepsize

        % the test parameters used during searching
        searchparams % search params
        searchscores % search scores
        searcherrs  % search errs

        % flags
        bracketing  % in bracketing phase?
        searching   % in searching phase?
        awaiting_score % needs a score posting?
        verbose     % self.verbose output?
        terminate   % terminate search?

        % robustness
        maxabsval
        minstepsize
    end % properties - public

    methods
        function self = LineSearcher(param0,step0,bounds,varargin)
            % The constructor method
            self.param0 = param0;
            self.step0 = step0;
            self.bounds = bounds;
            self.param = max(self.bounds(1),min(self.bounds(2),self.param0));
            if (self.param == self.bounds(1)) | (self.param == self.bounds(2))
                self.step0 = 0;
            end
            self.step = self.step0;
            self.params = [];
            self.scores = [];
            self.errs = [];
            self.bracketing = true;
            self.searching = false;
            self.minstepsize = 10^-30;
            if abs(self.step0) < self.minstepsize
                warning('Not performing line search as step size too small');
                self.terminate = true;
            else
                self.terminate = false;
            end
            self.awaiting_score = false;
            kwargs = rl.utils.dict(varargin{:});
            if kwargs.iskey('verbose')
                self.verbose = kwargs('verbose');
            else
                self.verbose = false;
            end % if - set self.verbose
            if kwargs.iskey('maxabsval')
                self.maxabsval = kwargs('maxabsval');
            else
                self.maxabsval = 10^12;
            end % if - set self.verbose

        end % function - LineSearcher constructor

        function [ param, terminate ] = suggest_param(self)
            param = self.param;
            terminate = self.terminate;
            self.awaiting_score = true;
        end % function - suggest_param

        function [score,err] = post_scores(self,scores)
            score = sum(scores);
            err = mean(scores)/sqrt(length(scores));
            self.post_score(score,err);
        end

        function [ ] = post_score(self,score,err)
            if (abs(score) > self.maxabsval) || (~isfinite(err))
                % if the score is too high then we aren't interested in the error (it could be non-finite)
                err = 0;
            end
            if ~isfinite(score)
                display('Warning: setting score to -maxabsval as it isn''t finite, this should be done with caution');
                score = -self.maxabsval;
            end
            if self.verbose

                display(['param: ' num2str(self.param) ', score: ' num2str(score,'%1.3f') ', err: ' num2str(err,'%1.3f') ]);
            end
            if self.awaiting_score
                self.awaiting_score = false;
                if self.bracketing
                    self.post_score_bracketing(score,err);
                elseif self.searching
                    self.post_score_searching(score,err);
                else
                    error('Why are we awaiting score when we are neither bracketing or searching.');
                end % if - bracketing
            else
                error('Cannot post a score, we are not awaiting one');
            end % if - awaiting_score
            
        end % function - post_score


        function [ ] = post_score_bracketing(self,score,err)
            i = length(self.params);
            % first score simply store
            if i == 0
                if self.verbose
                    display('First score');
                end
                self.store_score(1,score,err);
                self.param = max(self.bounds(1),min(self.bounds(2),self.param0+self.step));
            % second score must be significantly different if not a bound
            elseif i == 1
                if self.verbose
                    display('Second param value')
                end
                if ( abs(score - self.scores(1)) > (err+self.errs(1)) )
                    if self.verbose
                        display('Score significantly different, so keeping param');
                    end
                    self.store_score(2,score,err);
                    if self.scores(1) > self.scores(2)
                        self.switchdirection();
                    else
                        self.step = 2*self.step;
                        self.param = max(self.bounds(1),min(self.bounds(2),self.param0+self.step));
                    end
                elseif any(repmat(self.param,1,2) == self.bounds)
                    if self.verbose
                        display('param is a bound so must save');
                    end
                    self.store_score(2,score,err);
                    % also must switch due to bound
                    self.switchdirection();
                else
                    if self.verbose
                        display('score is not significantly different so continuing without storing');
                    end
                    self.step = 2*self.step;
                    % param is first param plus the step, truncated to be within bounds
                    self.param = max(self.bounds(1),min(self.bounds(2),self.param0+self.step));
%%                    % if we did truncate then we must change step to account for this
%%                    self.step = self.param - self.param0;
                end
            elseif i == 2

                if ( self.scores(2) - score ) > ( self.errs(2) + err )
                    if self.verbose
                        display('Score significantly worse than the middle, so can be the bracket');
                    end
                    self.store_score(3,score,err);
                    self.bracketing = false;
                    self.searching = true;
                    self.newsearchparam();
                elseif any(repmat(self.param,1,2) == self.bounds)
                    if self.verbose
                        display('Is a bound so must terminate bracketing')
                    end
                    self.store_score(3,score,err);
                    self.bracketing = false;
                    self.searching = true;
                    self.newsearchparam();
                elseif (self.scores(2) - self.errs(2) ) < (score - err) % luke's latest
                    self.store_score(2,score,err); % this replaces the current best with the new best.
                    self.step = 2*self.step;
                    % next param is first param plus the new step, truncated to be within bounds
                    self.param = max(self.bounds(1),min(self.bounds(2),self.param0+self.step));
                else
                    self.step = 2*self.step;
                    % param is first param plus the step, truncated to be within bounds
                    self.param = max(self.bounds(1),min(self.bounds(2),self.param0+self.step));
                end % if - terminating bracketing

                if self.searching & ( self.step < 0 )
                    % for sensibilities we order them in parameter order
                    self.params = self.switchelems(self.params,1,3);
                    self.scores = self.switchelems(self.scores,1,3);
                    self.errs = self.switchelems(self.errs,1,3);
                end % if - pre searching switch

                    
            else
                error('We can''t have more than 3 params in the bracketing')
            end % if - cases for size of self.params

        end % function - post_score_bracketing

        function [ ] = post_score_searching(self,score,err)
            if length(self.searchscores) == 0
                self.searchscores(1) = score;
                self.searcherrs(1) = err;
            elseif length(self.searchscores) == 1
                self.searchscores(2) = score;
                self.searcherrs(2) = err;
                self.resolvesearchparams();
            else
                error('Can only post two search scores before revising bounds')
            end
            if ~self.terminate
                self.newsearchparam();
            end

        end % function - post_score_searching


        %%% helper functions %%%

        function [ ] = resolvesearchparams(self)

            % for ease of reading we give existing accepted variables clearer names
            leftparam = self.params(1);
            centreparam = self.params(2);
            rightparam = self.params(3);
            leftscore = self.scores(1);
            centrescore = self.scores(2);
            rightscore = self.scores(3);
            lefterr = self.errs(1);
            centreerr = self.errs(2);
            righterr = self.errs(3);

            %% the default newparams are the current ones
            newleftparam = leftparam;
            newleftscore = leftscore;
            newlefterr = lefterr;
            newcentreparam = centreparam;
            newcentrescore = centrescore;
            newcentreerr = centreerr;
            newrightparam = rightparam;
            newrightscore = rightscore;
            newrighterr = righterr;

            % we don't recurse if the three parameters stay the same
            recurse = false;
            %% minimum requirement for recurse is that the new mean test scores are higher than brackets either side
            % so discount cases (L*) or (*L) -- we want it to look like a local maximum 
            if self.searchscores(1) > leftscore && self.searchscores(2) > rightscore

                %% casing the ordering of the middle three points
                % it is only worth accepting a new point if it is going to allow tighter bounds (higher lower bound) on the score
                % so we are interested in the score minus err (we call this lower-bound lb, but is only informally so).
                newleftlb = self.searchscores(1)-self.searcherrs(1);
                centrelb = centrescore-centreerr;
                newrightlb = self.searchscores(2)-self.searcherrs(2);
                % case 1 (MM) - centrescore is still the best
                % keep the centre and potentially tighten both brackets
                if centrelb > newleftlb && centrelb > newrightlb
                    newcentreparam = centreparam;
                    newcentrescore = centrescore;
                    newcentreerr = centreerr;

                    % can we make the left bound tighter
                    if ( centrescore-self.searchscores(1) ) > ( centreerr + self.searcherrs(1) )
                        newleftparam = self.searchparams(1);
                        newleftscore = self.searchscores(1);
                        newlefterr = self.searcherrs(1);
                        recurse = true;
                    end
                    % can we make the right bound tighter
                    if ( centrescore-self.searchscores(2) ) > ( centreerr + self.searcherrs(2) )
                        newrightparam = self.searchparams(2);
                        newrightscore = self.searchscores(2);
                        newrighterr = self.searcherrs(2);
                        recurse = true;
                    end

                % case 2 (HM) - left test score is the best, centre is next best
                % shift the centre to the test left and potentially tighten right bracket
                elseif centrelb > newrightlb % implying newleftlb is better
                    recurse = true;
                    newcentreparam = self.searchparams(1);
                    newcentrescore = self.searchscores(1);
                    newcentreerr = self.searcherrs(1);
                    
                    % can we make the right bound tighter
                    if ( self.searchscores(1) - centrescore ) > ( self.searcherrs(1) + centreerr )
                        newrightparam = centreparam;
                        newrightscore = centrescore;
                        newrighterr = centreerr;
                    elseif ( self.searchscores(1) - self.searchscores(2) ) > ( self.searcherrs(1) + self.searcherrs(2) )
                        newrightparam = self.searchparams(2);
                        newrightscore = self.searchscores(2);
                        newrighterr = self.searcherrs(2);
                    end

                % case 3 (MH) - right test score is the best, centre is next best
                % shift the centre to the test right and potentially tighten left bracket
                elseif centrelb > newleftlb % implying newrightlb is better
                    recurse = true;
                    newcentreparam = self.searchparams(2);
                    newcentrescore = self.searchscores(2);
                    newcentreerr = self.searcherrs(2);
                    
                    % can we make the left bound tighter
                    if ( self.searchscores(2) - centrescore ) > ( self.searcherrs(2) + centreerr )
                        newleftparam = centreparam;
                        newleftscore = centrescore;
                        newlefterr = centreerr;
                    elseif ( self.searchscores(2) - self.searchscores(1) ) > ( self.searcherrs(2) + self.searcherrs(1) )
                        newleftparam = self.searchparams(1);
                        newleftscore = self.searchscores(1);
                        newlefterr = self.searcherrs(1);
                    end

                % case 4 (HH) - both left and right is better
                % in this case we retain the old brackets but shift the centre
                else
                    recurse = true;
                    if newleftlb > newrightlb
                        % make test left point the new centre
                        newcentreparam = self.searchparams(1);
                        newcentrescore = self.searchscores(1);
                        newcentreerr = self.searcherrs(1);
                    else
                        % make test right point the new centre
                        newcentreparam = self.searchparams(2);
                        newcentrescore = self.searchscores(2);
                        newcentreerr = self.searcherrs(2);
                    end
                end % if - casing the ordering of the middle three points
            end % if - testing continuation

            if recurse && abs(self.step > self.minstepsize)
                self.params = [ newleftparam newcentreparam newrightparam ];
                self.scores = [ newleftscore newcentrescore newrightscore ];
                self.errs = [ newlefterr newcentreerr newrighterr ];
                self.searchparams = [];
                self.searchscores = [];
                self.searcherrs = [];
            else
                % we have finished, so param should be best param
                % if there are params on bounds these may be the best
                if self.verbose
                    display('Terminating line search');
                end
                [bestscore,bestindex] = max(self.scores);
                self.param = self.params(bestindex);
                if bestindex == 2
                    self.step = self.params(3) - self.param;
                else
                    self.step = self.params(2) - self.param;
                end
                self.searching = false;
                self.terminate = true;
            end % if - recurse

        end % function - resolvesearchparams

        function [ ] = newsearchparam(self)

            if length(self.searchparams) == 0
                leftparam = self.params(1);
                centreparam = self.params(2);
                rightparam = self.params(3);

                % segmenting the space
                goldenratio = (1+sqrt(5))/2;
                paramdiffl = centreparam-leftparam;
                paramdiffr = rightparam-centreparam;
                self.searchparams(1) = leftparam+paramdiffl/goldenratio;
                self.searchparams(2) = rightparam-paramdiffr/goldenratio;

                self.searchscores = [];
                self.searcherrs = [];

                self.param = self.searchparams(1);
            else
                self.param = self.searchparams(2);
            end
            
        end % function - newsearchparams


        function [ ] = store_score(self,pos,score,err)
            %display(['self.param: ' num2str(self.param) ]);
            %display(['self.params: ' num2str(self.params) ]);
            
            self.params(pos) = self.param;
            self.scores(pos) = score;
            self.errs(pos) = err;
        end % function - store_score

        function switchdirection(self)
            if self.verbose
                display('switching direction');
            end
            assert(self.bracketing & (length(self.params) == 2),'Can only switch direction when bracketing and with two stored values');
            self.step = -self.step0;
            self.param = max(self.bounds(1),min(self.bounds(2),self.param0+self.step));
            self.params = self.switchelems(self.params,1,2);
            self.scores = self.switchelems(self.scores,1,2);
            self.errs = self.switchelems(self.errs,1,2);
        end % function - switchdirection

        function [ vec ] = switchelems(self,vec,pos1,pos2)
            tmp = vec(pos1);
            vec(pos1) = vec(pos2);
            vec(pos2) = tmp;
        end % function - switchelems
    end % methods

    methods(Static)

        % finally you might want a set of parameters extended around the maximum point
        function extendedparams = extendparams(params,bounds)
            % for cross section plot, we want to enrich our parameter set.
            paramleft = params(1)
            paramcentre = params(2)
            paramright = params(3)
            stepleft = paramleft - paramcentre
            stepright = paramright - paramcentre
            if (paramleft+stepleft) < bounds(1)
                paramfarleft = paramleft;
                paramleft = paramcentre + 0.5*stepleft;
            else
                paramfarleft = paramleft+stepleft;
            end
            if (paramright+stepright) > bounds(2)
                paramfarright = paramright;
                paramright = paramcentre + 0.5*stepright;
            else
                paramfarright = paramright+stepright;
            end

            extendedparams = [ paramfarleft paramleft paramcentre paramright paramfarright ]
        end % function - extendparams


    end % methods
end % classdef

