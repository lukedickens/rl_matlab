% create model
[ model ] = GridWorld2()
VisualiseGrid(model,model.statenames ) % show it

% get unbiased policy and evaluate it
gamma = 1.
[unbiased_policy] = GetUnbiasedPolicy(model.absorbing, model.numactions)
[ V ] = PolicyEvaluationSteps(model, gamma, 1, unbiased_policy)
VisualiseGrid(model,V ) % show the value function

% show 2-steps of evaluation
[ V ] = PolicyEvaluationSteps(model, gamma, 2, unbiased_policy)
VisualiseGrid(model,V )


% show full evaluation
theta = 0.0001
[ V ] = PolicyEvaluation(model, gamma, theta, unbiased_policy)
VisualiseGrid(model,V )

% get stochastic greedy policy and again display it
[ greedy_policy ] = StochasticGreedyPolicyFromV(V, model, gamma, 0.000001,0.000001)
[ dominant_actions ] = GetAllDominantActions(greedy_policy, model.actionnames)
VisualiseGrid(model,dominant_actions )

% now evaluate greedy policy
[ V ] = PolicyEvaluation( model, gamma, theta, greedy_policy )
VisualiseGrid(model,V )

% now show that the new policy gives four directions at certain states
[ greedy_policy ] = StochasticGreedyPolicyFromV(V, model, gamma, 0.000001,0.000001)
[ dominant_actions ] = GetAllDominantActions(greedy_policy, model.actionnames)
VisualiseGrid(model,dominant_actions )

% when we evaluate again the value function doesn't change
[ V ] = PolicyEvaluation( model, gamma, theta, greedy_policy)
VisualiseGrid(model,V )


%%% random grid world
[ model ] = RandomGridWorld()
VisualiseGrid(model,model.statenames )

gamma = 0.99
[unbiased_policy] = GetUnbiasedPolicy(model.absorbing, model.numactions)
theta = 0.0001
[ V ] = PolicyEvaluation( model, gamma, theta, unbiased_policy)
VisualiseGrid(model,V )
% improve
[ greedy_policy ] = StochasticGreedyPolicyFromV(V, model, gamma, 0.000001,0.000001)
[ dominant_actions ] = GetAllDominantActions(greedy_policy, model.actionnames)
VisualiseGrid(model,dominant_actions )
% evaluate
[ V ] = PolicyEvaluation( model, gamma, theta, greedy_policy)
VisualiseGrid(model,V )
% improve
[ greedy_policy ] = StochasticGreedyPolicyFromV(V, model, gamma, 0.000001,0.000001)
[ dominant_actions ] = GetAllDominantActions(greedy_policy, model.actionnames)
VisualiseGrid(model,dominant_actions )


