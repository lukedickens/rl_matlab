clear
extent = 5;
numpenalties = 2;
numobstacles = 2;
modeltype = 'gw6'
[ model ] = GetModel('modeltype',modeltype,'extent',extent,'numpenalties',numpenalties,'numobstacles',numobstacles)
VisualiseGrid(model)


schedtype = 'sync'
for modeltype=['gw1';'gw2';'gw3']'
agenttype = 'Sarsa'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'SarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'ModSarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
end

schedtype = 'inter'
for modeltype=['gw1';'gw2';'gw3']'
agenttype = 'Sarsa'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'SarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'ModSarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
end

schedtype = 'batch'
for modeltype=['gw1';'gw2';'gw3']'
agenttype = 'Sarsa'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'SarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'ModSarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
end

schedtype = 'rand'
for modeltype=['gw1';'gw2';'gw3']'
agenttype = 'Sarsa'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'SarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'ModSarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
end

schedtype = 'sync'
for modeltype=['gw4';'gw5';'gw6']'
agenttype = 'Sarsa'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'SarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'ModSarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
end

schedtype = 'inter'
for modeltype=['gw4';'gw5';'gw6']'
agenttype = 'Sarsa'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'SarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'ModSarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
end

schedtype = 'batch'
for modeltype=['gw4';'gw5';'gw6']'
agenttype = 'Sarsa'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'SarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'ModSarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
end

schedtype = 'rand'
for modeltype=['gw4';'gw5';'gw6']'
agenttype = 'Sarsa'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'SarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
agenttype = 'ModSarsaLambda'
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype});
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)sign(r)*abs(r).^2);
%%DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'utilfunc',@(r)1.01.^r);
end


% fixed alpha
clear
modeltypes = {'gw1','gw2','gw3','gw4','gw5','gw6'}
%modeltypes = {'gw2','gw3'}
schedtypes = {'sync','rand','inter','batch'};
%schedtypes = {'sync','rand'};
%agenttypes = {'Sarsa','SarsaLambda','ModSarsaLambda'};
agenttypes = {'Sarsa'};
for schedid=1:length(schedtypes)
    for modelid=1:length(modeltypes)
        modeltype = modeltypes{modelid};
        for at=1:length(agenttypes)
            agenttype = agenttypes{at};
            if strcmpi(agenttype,'SarsaLambda') | strcmpi(agenttype,'ModSarsaLambda')
                error('Not implemented');
            else
                switch(modeltype)
                    case 'gw1'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.3, 0, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.3, 0, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    case 'gw2'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.5, 0, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.5, 0, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    case 'gw3'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.25, 0, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.25, 0, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    case 'gw4'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.1, 0, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.1, 0, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    case 'gw5'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.2, 0, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.2, 0, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    case 'gw6'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.3, 0, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.3, 0, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    otherwise
                        error('Unrecognised model type');
                end % switch - model type
            end % if - agenttype
            paramnames = { agent1paramnames agent2paramnames};
            params = [agent1params agent2params];
            paramsteps = [agent1paramsteps agent2paramsteps];
            bounds = [agent1bounds; agent2bounds];
            agentargs = dict('utiltype','exp','utilmult',1);
            schedtype = schedtypes{schedid};
            DECSimExperiment('modeltype',modeltype,'schedtype',schedtype,'agenttypes',{agenttype,agenttype}, 'dbfile',['fixedalpha_' modeltype '_' schedtype '.db'], 'paramnames', paramnames, 'params', params, 'paramsteps', paramsteps, 'bounds', bounds);    
        end % for - agenttype
    end % for - modeltype
end % for - schedtype

% decaying alphas
clear
modeltypes = {'gw1','gw2','gw3','gw4','gw5','gw6'}
%modeltypes = {'gw2','gw3'}
schedtypes = {'sync','rand','inter','batch'};
%schedtypes = {'sync','rand'};
%agenttypes = {'Sarsa','SarsaLambda','ModSarsaLambda'};
agenttypes = {'Sarsa'};
for schedid=1:length(schedtypes)
    for modelid=1:length(modeltypes)
        modeltype = modeltypes{modelid};
        for at=1:length(agenttypes)
            agenttype = agenttypes{at};
            if strcmpi(agenttype,'SarsaLambda') | strcmpi(agenttype,'ModSarsaLambda')
                error('Not implemented');
            else
                switch(modeltype)
                    case 'gw1'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.3, 0.2, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.1, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.3, 0.2, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.1, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    case 'gw2'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.5, 0.2, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.1, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.5, 0.2, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.1, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    case 'gw3'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.25, 0.2, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.1, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.25, 0.2, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.1, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    case 'gw4'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.1, 0.2, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.1, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.1, 0.2, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.1, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    case 'gw5'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.2, 0.2, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.1, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.2, 0.2, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.1, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    case 'gw6'
                        agent1paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.3, 0.2, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.1, 0];
                        agent1bounds = [ [0.0001,1];[0,1];[0,1]];
                        agent2paramnames = {'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.3, 0.2, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.1, 0];
                        agent2bounds = [ [0.0001,1];[0,1];[0,1]];
                    otherwise
                        error('Unrecognised model type');
                end % switch - model type
            end % if - agenttype
            paramnames = { agent1paramnames agent2paramnames};
            params = [agent1params agent2params];
            paramsteps = [agent1paramsteps agent2paramsteps];
            bounds = [agent1bounds; agent2bounds];
            agentargs = dict('utiltype','exp','utilmult',1);
            schedtype = schedtypes{schedid};
            DECSimExperiment('modeltype',modeltype,'schedtype',schedtype,'agenttypes',{agenttype,agenttype}, 'dbfile',['decayingalpha_' modeltype '_' schedtype '.db'], 'paramnames', paramnames, 'params', params, 'paramsteps', paramsteps, 'bounds', bounds);    
        end % for - agenttype
    end % for - modeltype
end % for - schedtype


%% UTILs
clear
modeltypes = {'gw1','gw2','gw3','gw4','gw5','gw6'}
%modeltypes = {'gw2','gw3'}
%schedtypes = {'sync','rand','inter','batch'};
schedtypes = {'sync','rand'};
agenttypes = {'Sarsa','SarsaLambda','ModSarsaLambda'};
for schedid=1:length(schedtypes)
    for modelid=1:length(modeltypes)
        modeltype = modeltypes{modelid};
        for at=1:length(agenttypes)
            agenttype = agenttypes{at};
            if strcmpi(agenttype,'SarsaLambda') | strcmpi(agenttype,'ModSarsaLambda')
                switch(modeltype)
                    case 'gw1'
                        agent1paramnames = {'lambda','utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.2, 0.1, 0, 0.55, 0.2, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.05, 0, 0, 0, 0 ];
                        agent1bounds = [ [0.0001,1]; [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                        agent2paramnames = {'utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.2, 0.1, 0, 0.2, 0.1, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.05, 0, 0, 0, 0 ];
                        agent2bounds = [ [0.0001,1]; [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                    case 'gw2'
                        agent1paramnames = {'lambda','utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.75, 0.1, 0, 0.5, 0.1, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.05, 0, 0, 0, 0 ];
                        agent1bounds = [ [0.0001,1]; [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                        agent2paramnames = {'utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.75, 0.1, 0, 0.3, 0.25, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.05, 0, 0, 0, 0 ];
                        agent2bounds = [ [0.0001,1]; [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                    case 'gw3'
                        agent1paramnames = {'lambda','utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.5, 0.1, 0, 0.3, 0.05, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.05, 0, 0, 0, 0 ];
                        agent1bounds = [ [0.0001,1]; [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                        agent2paramnames = {'utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.5, 0.1, 0, 0.2, 0.1, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.05, 0, 0, 0, 0 ];
                        agent2bounds = [ [0.0001,1]; [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                    case 'gw4'
                        agent1paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift', 'lambda' };
                        agent1params = [ 0.0001, 0.3, 0, 0.1, 0, 0.5 ];
                        agent1paramsteps = [ 0, 0.05, 0, 0.05, 0, 0.1 ];
                        agent1bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]; [0,1]];
                        agent2paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift', 'lambda' };
                        agent2params = [ 0.0001, 0.3, 0, 0.1, 0, 0.5 ];
                        agent2paramsteps = [ 0, 0.05, 0, 0.05, 0, 0.1 ];
                        agent2bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]; [0,1]];
                    case 'gw5'
                        agent1paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift', 'lambda' };
                        agent1params = [ 0.0001, 0.3, 0, 0.1, 0, 0.5 ];
                        agent1paramsteps = [ 0, 0.05, 0, 0.05, 0, 0.1 ];
                        agent1bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]; [0,1]];
                        agent2paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift', 'lambda' };
                        agent2params = [ 0.0001, 0.3, 0, 0.1, 0, 0.5 ];
                        agent2paramsteps = [ 0, 0.05, 0, 0.05, 0, 0.1 ];
                        agent2bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]; [0,1]];
                    case 'gw6'
                        agent1paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift', 'lambda' };
                        agent1params = [ 0.0001, 0.3, 0, 0.1, 0, 0.5 ];
                        agent1paramsteps = [ 0, 0.05, 0, 0.05, 0, 0.1 ];
                        agent1bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]; [0,1]];
                        agent2paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift', 'lambda' };
                        agent2params = [ 0.0001, 0.3, 0, 0.1, 0, 0.5 ];
                        agent2paramsteps = [ 0, 0.05, 0, 0.05, 0, 0.1 ];
                        agent2bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]; [0,1]];
                    otherwise
                        error('Unrecognised model type');
                end % switch - model type
            else
                switch(modeltype)
                    case 'gw1'
                        agent1paramnames = {'utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.1, 0, 0.55, 0.2, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.2, 0, 0, 0 ];
                        agent1bounds = [ [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                        agent2paramnames = {'utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.1, 0, 0.2, 0.1, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.2, 0, 0, 0 ];
                        agent2bounds = [ [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                    case 'gw2'
                        agent1paramnames = {'utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.1, 0, 0.5, 0.1, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.2, 0, 0, 0 ];
                        agent1bounds = [ [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                        agent2paramnames = {'utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.1, 0, 0.3, 0.25, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.2, 0, 0, 0 ];
                        agent2bounds = [ [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                    case 'gw3'
                        agent1paramnames = {'utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent1params = [ 0.1, 0, 0.3, 0.05, 0.0001 ];
                        agent1paramsteps = [ 0.05, 0.2, 0, 0, 0 ];
                        agent1bounds = [ [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                        agent2paramnames = {'utiltemp', 'utilshift', 'alpha0', 'alphadecay', 'epsilon' };
                        agent2params = [ 0.1, 0, 0.2, 0.1, 0.0001 ];
                        agent2paramsteps = [ 0.05, 0.2, 0, 0, 0 ];
                        agent2bounds = [ [0.0001,1]; [-5,5]; [0.01,1];[0,1];[0,1]];
                    case 'gw4'
                        agent1paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift' };
                        agent1params = [ 0.0001, 0.3, 0, 0.1, 0  ];
                        agent1paramsteps = [ 0, 0.05, 0, 0.05, 0 ];
                        agent1bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]];
                        agent2paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift' };
                        agent2params = [ 0.0001, 0.3, 0, 0.1, 0  ];
                        agent2paramsteps = [ 0, 0.05, 0, 0.05, 0 ];
                        agent2bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]];
                    case 'gw5'
                        agent1paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift' };
                        agent1params = [ 0.0001, 0.3, 0, 0.1, 0  ];
                        agent1paramsteps = [ 0, 0.05, 0, 0.05, 0 ];
                        agent1bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]];
                        agent2paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift' };
                        agent2params = [ 0.0001, 0.3, 0, 0.1, 0  ];
                        agent2paramsteps = [ 0, 0.05, 0, 0.05, 0 ];
                        agent2bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]];
                    case 'gw6'
                        agent1paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift' };
                        agent1params = [ 0.0001, 0.3, 0, 0.1, 0  ];
                        agent1paramsteps = [ 0, 0.05, 0, 0.05, 0 ];
                        agent1bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]];
                        agent2paramnames = {'epsilon', 'alpha0', 'alphadecay', 'utiltemp', 'utilshift' };
                        agent2params = [ 0.0001, 0.3, 0, 0.1, 0  ];
                        agent2paramsteps = [ 0, 0.05, 0, 0.05, 0 ];
                        agent2bounds = [ [0.00001,1]; [0.0001,1]; [0,1]; [0.01,1]; [0,1]];
                    otherwise
                        error('Unrecognised model type');
                end % switch - model type
            end % if - agenttype
            paramnames = { agent1paramnames agent2paramnames};
            params = [agent1params agent2params];
            paramsteps = [agent1paramsteps agent2paramsteps];
            bounds = [agent1bounds; agent2bounds];
            agentargs = dict('utiltype','exp','utilmult',1);
            schedtype = schedtypes{schedid};
            DECSimExperiment('modeltype',modeltype,'schedtype',schedtype,'agenttypes',{agenttype,agenttype}, 'dbfile',['utilsearch_' modeltype '_' schedtype '.db'], 'paramnames', paramnames, 'params', params, 'paramsteps', paramsteps, 'bounds', bounds);    
        end % for - agenttype
    end % for - modeltype
end % for - schedtype



%%%%%%%%%%
%%
%%

for modeltype=['gw1';'gw2';'gw3';'gw4';'gw5';'gw6']'
schedtypes = {'sync','rand','inter','batch'};
for st=1:length(schedtypes)
schedtype = schedtypes{st}
agenttype = 'Sarsa'
agent1paramnames = {'epsilon','alpha0','alphadecay' };
agent1params = [ 0.0001, 0.2, 0.1 ];
agent1paramsteps = [ 0, 0.1, 0.1 ];
agent1bounds = [ [0,1];[0,1];[0,1]];
agent2paramnames = {'epsilon','alpha0','alphadecay' };
agent2params = [ 0.0001, 0.2, 0.1 ];
agent2paramsteps = [ 0, 0.1, 0.1 ];
agent2bounds = [ [0,1];[0,1];[0,1]];
paramnames = { agent1paramnames agent2paramnames};
params = [agent1params agent2params];
paramsteps = [agent1paramsteps agent2paramsteps];
bounds = [agent1bounds; agent2bounds];
agentargs = dict;
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'agentargs',agentargs,'paramnames',paramnames,'params',params,'paramsteps',paramsteps,'bounds',bounds);
agentargs = dict('utiltype','exp','utilmult',1,'utiltemp',0.1);
DECSimExperiment('schedtype',schedtype,'modeltype',modeltype','agenttypes',{agenttype,agenttype},'agentargs',agentargs,'paramnames',paramnames,'params',params,'paramsteps',paramsteps,'bounds',bounds);
end
end


for modeltype=['gw1';'gw2';'gw3';'gw4';'gw5']'
modeltype = modeltype'
switch modeltype
    case 'gw1'
        lambda = 0.2;
    case 'gw2'
        lambda = 0.75;
    case 'gw3'
        lambda = 0.5;
    case 'gw4'
        lambda = 0.5;
    case 'gw5'
        lambda = 0.1;
    otherwise
        lambda = 0.5;
end
schedtypes = {'sync','rand','inter','batch'};
    for st=1:length(schedtypes)
    schedtype = schedtypes{st}
    agenttypes = {'SarsaLambda','ModSarsaLambda'};
        for at=1:length(agenttypes)
        agenttype = agenttypes{at};
        agent1paramnames = {'epsilon','alpha0','alphadecay' 'lambda' };
        agent1params = [ 0.0001, 0.2, 0.1 lambda ];
        agent1paramsteps = [ 0, 0.1, 0.1 0.05 ];
        agent1bounds = [ [0,1];[0,1];[0,1];[0,1]];
        agent2paramnames = {'epsilon','alpha0','alphadecay' 'lambda' };
        agent2params = [ 0.0001, 0.2, 0.1 lambda ];
        agent2paramsteps = [ 0, 0.1, 0.1 0.05 ];
        agent2bounds = [ [0,1];[0,1];[0,1];[0,1]];
        paramnames = { agent1paramnames agent2paramnames};
        params = [agent1params agent2params];
        paramsteps = [agent1paramsteps agent2paramsteps];
        bounds = [agent1bounds; agent2bounds];
        agentargs = dict;
        DECSimExperiment('schedtype',schedtype,'modeltype',modeltype,'agenttypes',{agenttype,agenttype},'agentargs',agentargs,'paramnames',paramnames,'params',params,'paramsteps',paramsteps,'bounds',bounds,'dbfile',['decsim_' modeltype '_' agenttype '_' schedtype '.db']);
        agentargs = dict('utiltype','exp','utilmult',1,'utiltemp',0.1);
        DECSimExperiment('schedtype',schedtype,'modeltype',modeltype,'agenttypes',{agenttype,agenttype},'agentargs',agentargs,'paramnames',paramnames,'params',params,'paramsteps',paramsteps,'bounds',bounds,'dbfile',['decsim_' modeltype '_' agenttype '_' schedtype '.db']);
        end % for - agenttype
    end % for - schedtype
end % for - modeltype

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%% Single agent
clear
for modeltype=['gw1';'gw2';'gw3';'gw4';'gw5';'gw6']'
    modeltype = modeltype'
    decsimargs = dict('smap',@(x) x,'invamap',@(ja) ja,'numactionslist',4);
    agent1paramnames = {'alpha0','alphadecay','epsilon' };
    agent1params = [ 0.4, 0.1, 0.0002 ];
    agent1paramsteps = [ 0.1, 0.05, 0 ];
    agent1bounds = [ [0,1];[0,1];[0.00001,0.5]];
    paramnames = { agent1paramnames };
    params = [agent1params];
    paramsteps = [agent1paramsteps];
    bounds = [agent1bounds];
    schedtype = 'sync';
    agenttype = 'Sarsa';
    DECSimExperiment('decsimargs', decsimargs, 'schedtype', schedtype, 'agenttypes', {agenttype}, 'modeltype', modeltype, 'dbfile',['decsim_singleagent_' modeltype '_pow.db'], 'paramnames', paramnames, 'params', params, 'paramsteps', paramsteps, 'bounds', bounds);
end


%% testing single agent

[model, modeltype] = GetModel('modeltype','gw1');
agent = GetAgent('sarsa','numstates',model.numstates,'numactions',model.numactions,'alpha0',0.37,'alphadecay',0,'epsilon',0.0002,'gamma',0.9)
sim = Simulation(model)
maxsteps = 30;
numepisodes = 1000;
run_multiple( agent, sim, numepisodes, maxsteps )
[statsepisodes accumreward avereturn stderr] = sim.stats(0.5)

modeltype = 'gw1';
decsimargs = dict('smap',@(x) x,'invamap',@(ja) ja,'numactionslist',4);
agent1paramnames = {'alpha0','alphadecay','epsilon' };
agent1params = [ 0.37, 0.1, 0.0002 ];
agent1paramsteps = [ 0.1, 0.05, 0 ];
agent1bounds = [ [0,1];[0,1];[0.00001,0.5]];
paramnames = { agent1paramnames };
params = [agent1params];
paramsteps = [agent1paramsteps];
bounds = [agent1bounds];
schedtype = 'sync';
agenttype = 'Sarsa';
DECSimExperiment('decsimargs', decsimargs, 'schedtype', schedtype, 'agenttypes', {agenttype}, 'modeltype', modeltype, 'dbfile',['decsim_singleagent_' modeltype '_pow.db'], 'paramnames', paramnames, 'params', params, 'paramsteps', paramsteps, 'bounds', bounds);

%%
clear
for modeltype=['gw1';'gw2';'gw3';'gw4';'gw5';'gw6']'
    modeltype = modeltype'
    decsimargs = dict('smap',@(x) x,'invamap',@(ja) ja,'numactionslist',4);
    agent1paramnames = {'alpha0','alphadecay','epsilon' };
    agent1params = [ 0.4, 0.1, 0.0002 ];
    agent1paramsteps = [ 0.1, 0.05, 0 ];
    agent1bounds = [ [0,1];[0,1];[0.00001,0.5]];
    paramnames = { agent1paramnames };
    params = [agent1params];
    paramsteps = [agent1paramsteps];
    bounds = [agent1bounds];
    schedtype = 'sync';
    agenttype = 'Sarsa';
    DECSimExperiment('decsimargs', decsimargs, 'schedtype', schedtype, 'agenttypes', {agenttype}, 'modeltype', modeltype, 'dbfile',['decsim_singleagent_' modeltype '_pow.db'], 'paramnames', paramnames, 'params', params, 'paramsteps', paramsteps, 'bounds', bounds);
end


%% testing cooperative game
clear
agent1paramnames = {'epsilon','alpha0','alphadecay' };
agent1params = [ 0.1, 0.2, 0 ];
agent1paramsteps = [ 0, 0.1, 0 ];
agent1bounds = [ [0,1];[0,1];[0,1]];
agent2paramnames = {'epsilon','alpha0','alphadecay' };
agent2params = [ 0.1, 0.2, 0 ];
agent2paramsteps = [ 0, 0.1, 0 ];
agent2bounds = [ [0,1];[0,1];[0,1]];
paramnames = { agent1paramnames agent2paramnames };
params = [ agent1params agent2params ];
paramsteps = [ agent1paramsteps agent2paramsteps ];
bounds = [ agent1bounds; agent2bounds ];
schedtype = 'sync';
agenttype = 'Sarsa';
game = CoopStratGame.GetGame('coord','k',-40);
dbfile = 'coordgame.db'
marshallargs = dict('agenttypes',{agenttype agenttype},'schedtype',schedtype,'dbfile',dbfile,'agentargs',dict('gamma',0.9));
marshall = DECSimMarshall(game,marshallargs);
maxsteps = 300;
numepisodes = 1;
%cycles = 2;
%numtrials = 8;
%statportion = 1;
%marshall.parameter_search( paramnames, params, paramsteps, bounds, maxsteps, numepisodes, cycles, numtrials, statportion);
marshall.set_agents(paramnames, params);
marshall.run_multiple( maxsteps, numepisodes );
accums = marshall.decsim.getaccums();
plot(1:length(accums),accums);


%history = marshall.decsim.history{1};
%accums(1) = history(1)
%for i=2:length(history)
%    accums(i) = accums(i-1)+history(i);
%end
%plot(1:length(accums),accums);
[v i] = max(marshall.agents{1}.Q,[],2)
[v i] = max(marshall.agents{2}.Q,[],2)

