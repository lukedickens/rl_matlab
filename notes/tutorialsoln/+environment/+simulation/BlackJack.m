classdef BlackJack < handle
%% An imperfect implementation of BlackJack as an MDP simulation
    properties (SetAccess='public')
        maximumsum
        maxfaceval
        dealerfloor % dealer continues to hit until over dealerfloor
    end
    properties (Hidden=true)
        numstates % number of states
        numactions % number of actions
        absorbing % boolean vector over state ids corresponding to whether states are absorbing
        state % the current state
        carddeck
    end
    
    methods
        function self = BlackJack(varargin)
            % construct the simulation object
            self.carddeck = environment.simulation.CardDeck();
            self.maxfaceval = 10;
            self.dealerfloor = 16;
            self.maximumsum = 21;
            self.numstates = 2*self.maxfaceval^2 + 1;
            self.numactions = 2; % hit = 1, stick = 2
            self.absorbing = zeros(1,self.numstates+1);
            self.absorbing(end) = 1;
        end % function - BlackJack constructor
        
        function stateindex = init(self)
            % shuffle the cards
            self.carddeck.shuffle();
            % deal the first card (value of picture cards
            % is equivalent to highest value card)
            firstcard = self.carddeck.deal();
            firstval = min(firstcard(1),self.maxfaceval);
            % likewise for dealer
            dealerscard = self.carddeck.deal();
            dealershowing = min(dealerscard(1),self.maxfaceval);
            % deal next card(s) to player (must be large enough sum)
            [ playersum usableace ] = self.initialise_hand(firstval);
            % store the full representation
            self.state = [ playersum dealershowing usableace ];
            % observable state is the index
            stateindex = self.get_index(self.state);
        end % function - init
        
        function [ reward, stateindex ] = next(self,action)
            % get the next state and reward
            playersum = self.state(1);
            dealershowing = self.state(2);
            usableace = self.state(3); % 0 => no ace, 1 => ace
            if action == 1 % player requests hit
                [playersum, usableace] = self.update_hand(playersum,usableace);
                if playersum > 0
                    self.state = [ playersum dealershowing usableace ];
                    stateindex = self.get_index(self.state);
                    reward = 0;
                    return;
                end
            end
            % if action is 2 (stick) or hit ended in a bust (playersum==0)
            % then play out the dealer's hand
            dealersum = self.play_dealer();
            self.state = [0 0 0]; % terminal state
            stateindex = self.get_index(self.state);
            if playersum > dealersum
                reward = 10; % player beats dealer
            elseif playersum == dealersum
                reward = 0; % both score the same or both bust
            else
                reward = -10;
            end
        end % function - next

        function answer = terminal(self)
            answer = (self.state(1)==0);
        end % init

        function [unbiased_policy] = UnbiasedPolicy(self)
            %%GETUNBIASEDPOLICY For every non-absorbing state (rows) we give equal probability to each action (columns)
            unbiased_policy = 1./self.numactions * ~self.absorbing'*ones(1,self.numactions);
        end

        function [ handsum usableace ] = initialise_hand(self,firstval)
            % takes the initial card value and deals cards until over 12.
            % returns the final sum and whether a usable ace
            usableace = (firstval==1);
            % if ace count it as 11
            handsum = firstval + usableace*self.maxfaceval;
            % to limit the statespace to interesting states, ignore very low initial hands
            while handsum < (self.maxfaceval+2)
                nextcard = self.carddeck.deal();
                nextval = min(nextcard(1),self.maxfaceval);
                if (nextval==1) & (handsum < (self.maximumsum-self.maxfaceval-1))
                    % if card is an ace (and we can use it) then count as 11 and 
                    % indicate as a usable ace
                    handsum = handsum + self.maxfaceval + 1;
                    usableace = 1;
                else
                    % otherwise simply add the card to the playersum
                    handsum = handsum + nextval;
                end
            end
        end

        function [ handsum usableace ] = update_hand(self, handsum, usableace )
            % deals a hand and updates the state representation appropriately
            nextcard = self.carddeck.deal();
            % picture cards are counted as the maximum face value
            nextval = min(nextcard(1),self.maxfaceval);
            % an ace will initially be counted as its larger value
            handsum = handsum + nextval + (nextval==1)*self.maxfaceval;
            usableace = usableace | (nextval==1);
            % if the hand is over maximum allowed value (21)
            if handsum > self.maximumsum
                % check if there is a usable ace
                if usableace
                    % if so use the ace
                    handsum = handsum - self.maxfaceval;
                    usableace = 0;
                else
                    % otherwise you are bust
                    handsum = 0;
                end
            end
        end

        function [ dealersum ] = play_dealer(self)
            % the dealer continues to play until over
            [dealersum,dealerace] =  self.initialise_hand(self.state(2));
            while dealersum < self.dealerfloor & dealersum > 0
                % dealer gets cards until over 16 or bust
                [dealersum,dealerace] = self.update_hand(dealersum,dealerace);
            end
        end

        function [ staterep ] = get_representation(self,stateindex)
            % Convert a state index into 
            % a representation, i.e. [ playersum dealershowing usableace ]
            if stateindex <= 2*self.maxfaceval^2
                playersumindex = mod(stateindex-1,self.maxfaceval);
                playersum = playersumindex + self.maxfaceval + 2;
                dealershowing = mod((stateindex - playersumindex - 1)/self.maxfaceval,self.maxfaceval)+1;
                usableace = (((stateindex - playersumindex - 1)/self.maxfaceval - dealershowing-1)/self.maxfaceval) > 0;
                staterep = [ playersum dealershowing usableace ];
            else
                staterep = [ 0 0 0 ]; % bust
            end
        end

        function [ stateindex ] = get_index(self,staterep)
            % Convert a state representation, i.e. [ playersum dealershowing usableace ]
            % into an index
            if staterep(1) > 0
                playersum = staterep(1);
                dealershowing = staterep(2);
                usableace = staterep(3); % 0 => no ace, 1 => ace
                stateindex = ((usableace*self.maxfaceval) + dealershowing-1)*self.maxfaceval + (playersum - self.maxfaceval - 1);
            elseif staterep(1) == 0
                % bust
                stateindex = 2*self.maxfaceval^2 + 1;
            end
        end

        function [trace] = run(self,maxsteps,policy)
            %% takes:
            %%      maximum number of steps per epsiode, maxsteps;
            %%      control policy, policy;
            %% returns:
            %%      a simulated trace/episode in the sim following the policy

            % get the initial state
            prior_s = self.init();
            % and the initial action
            prior_a = find(mnrnd(1,policy(prior_s,:)),1);
            % for convenience we will make the r_{0}=0 - it is not used
            reward = 0;
            step = [ reward, prior_s, prior_a ]; % first reward is a dummy reward
            trace = [ step; ];
            for i = 1:maxsteps
                % the posterior state is drawn probabilistically from the next function
                % the state-action-state triple determines the reward
                [reward, post_s] = self.next(prior_a); % s' ~ t(s,a,s')
                if self.absorbing(self.state) ~= 1
                    % if the trace has not terminated then choose another action from the policy
                    post_a = find(mnrnd(1,policy(post_s,:)));
                    % record the step for the trace
                    step = [ reward, post_s, post_a ];
                    trace = [ trace; step ];
                    % for the next iteration of the loop
                    prior_s = post_s;
                    prior_a = post_a;
                else
                    % otherwise the trace is terminated and no action is needed.
                    step = [reward, post_s, 0]; % last action is a dummy action
                    trace = [ trace; step ];
                    break;
                end % if
            end % for
        end % function - run

    end
end % classdef
