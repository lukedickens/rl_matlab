classdef Simulation < handle
%% An mdp simulation
    properties (SetAccess='public')
        initial
    end
    properties (SetAccess='protected')
        numstates % number of states
        numactions % number of actions
        statenames % ordered list of state names (index is state id)
        actionnames % ordered list of action names (index is action id)
        absorbing % boolean vector over state ids corresponding to whether states are absorbing
        t % the transition function
        r % the reward function
    end
    properties (SetAccess='private',GetAccess='protected')
        state % the current state
    end
    
    methods
        function self = Simulation(model)
            %% The constructor method
            self.numstates = model.numstates;
            self.numactions = model.numactions;
            self.statenames = model.statenames;
            self.actionnames = model.actionnames;
            self.absorbing = model.absorbing;
            self.initial = model.initial;
            self.t = model.t;
            self.r = model.r;
        end % Simulation
        
        function state = init(self)
            % Initialises state from initial distribution and then outputs this
            state = find(mnrnd(1,self.initial),1); % index of the 1 in random vector drawn from initial multinomial parameters
            self.state = state;
        end % init
        
        function [ reward, nextstate ] = next(self,action)
            %% takes:
            %%      action choice, action
            %% returns:
            %%      next state and reward setting self.state to new state
            dist = self.t(self.state,action,[1:self.numstates]'); % weights over next state
            nextstate = find(mnrnd(1,dist),1); % next state drawn from initial multinomial parameters
            reward = self.r(self.state,action,nextstate); % reward is r(s,a,s')
            self.state = nextstate; % current state is now the newly created one
        end % next

        function [trace] = run(self,maxsteps,policy)
            %% takes:
            %%      maximum number of steps per epsiode, maxsteps;
            %%      control policy, policy;
            %% returns:
            %%      a simulated trace/episode in the sim following the policy

            %% NOtation
            % s - the prior/previous state
            % a - the prior/previous action
            % ss - the subsequent state
            % aa - the subsequent action
            % get the initial state
            self.init();
            % store the initial state
            trace.states(1) = self.state;
            for i = 1:maxsteps
                % if the trace has not terminated then choose another action from the policy
                action = find(mnrnd(1,policy(self.state,:)),1);
                % store the action
                trace.actions(i) = action;
                % the posterior state is drawn probabilistically from the next function
                % the state-action-state triple determines the reward
                [reward] = self.next(action);
                % store the reward
                trace.rewards(i) = reward;
                % store the state
                trace.states(i+1) = self.state;
                if self.terminal()
                    break;
                end % if
            end % for
        end % run_trace
        function answer = terminal(self)
            answer = self.absorbing(self.state);
        end % init
        function [unbiased_policy] = UnbiasedPolicy(self)
            %%GETUNBIASEDPOLICY For every non-absorbing state (rows) we give equal probability to each action (columns)
            unbiased_policy = ~self.absorbing'*ones(1,self.numactions)./self.numactions;
        end
    end % methods

end % classdef
