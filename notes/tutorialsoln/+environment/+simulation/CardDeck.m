classdef CardDeck < handle
% Simulates a card deck.
    properties (SetAccess=public)
        dealt
        numcards    
    end % properties - protected
    properties (Hidden=true)
        cards
    end % properties - protected
    
    methods 
        function self = CardDeck(numpacks)
            % numpacks determines how many full packs to simulate
            if nargin < 1
                self.numcards = 52;
            else
                self.numcards = numpacks*52;
            end
            self.shuffle();
        end % function - constructor
        function card = deal(self)
            if self.dealt < self.numcards
                self.dealt = self.dealt + 1;
                card = self.cards(self.dealt);
            else
                error('All cards have been dealt')
            end
            %self.show(card);
        end

        function shuffle(self)
            self.dealt = 0;
            self.cards = randperm(self.numcards);
        end % function

    end % methods

    methods(Static)
        function card = indextocard(i)
            val = mod(i-1,13)+1;
            suit = mod(floor((i-1)/13),4)+1;
            card = [val,suit];
        end

        function show(card)
            % card can be card representation (length=2) or card index (length=1)
            if length(card) == 1
                card = environment.simulation.CardDeck.indextocard(card);
            end
            switch card(1)
            case 1
                valstr = 'A';
            case 13
                valstr = 'K';
            case 12
                valstr = 'Q';
            case 11
                valstr = 'J';
            otherwise
                valstr = num2str(card(1));
            end
            switch card(2)
            case 1
                suitstr = char(9829);
            case 2
                suitstr = char(9831);
            case 3
                suitstr = char(9830);
            case 4
                suitstr = char(9828);
            otherwise
                error('Suit not recognised!\n');
            end
            fprintf('%s%s ',valstr,suitstr)

        end % function
    end    
end % classdef 

