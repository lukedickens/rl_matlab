function [ model ] = GridWorld3()
% A more complex gridworld

% number of rows, columns in gridworld
shape = [5,5];
% probability of respectively moving 0 degrees, 90 degrees, 180 degrees, 270 degrees from desired direction.
action_effects = [ 0.8, 0.1, 0.0, 0.1];
% obstacles block movement
obstacles = [[2,2];[3,2];[3,4]];
% absorbing/terminal locations
absorbing_locs = [[5,1];[5,2];[5,3];[5,4];[5,5];[3,5];];
% the default reward
reward_desc.default = 0;
% locations with non-default rewards
reward_desc.special_locs = [[3,3];[3,5];[5,1];[5,2];[5,3];[5,4];[5,5]];
% the values for the non-default rewards
reward_desc.special_vals = [[1  ];[10 ];[-10];[-10];[-10];[-10];[-10]];
% initial locations
initial_desc.locs = [[4,1];];
initial_desc.weights = ones(size(initial_desc.locs,1),1);

%
[ model ] = environment.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc);
end

%--------------------------------------------------------------------------
