function [ model ] = GridWorld6()

% number of rows, columns in gridworld
shape = [7,18];
% probability of respectively moving 0 degrees, 90 degrees, 180 degrees, 270 degrees from desired direction.
action_effects = [ 1, 0, 0, 0];
% obstacles block movement
obstacles = [[[1:6]' 16*ones(6,1)]];
% penalty locations are both absorbing and have non-default rewards
penalty_locs = [4*ones(12,1) [1:12]'];
% goal locations are both absorbing and have non-default rewards
goal_locs = [[5,1];[1,18];];
% absorbing/terminal locations
absorbing_locs = [goal_locs;penalty_locs];
% the default reward
reward_desc.default = -1;
% locations with non-default rewards
reward_desc.special_locs = penalty_locs;
% the values for the non-default rewards
reward_desc.special_vals = [-100*ones(length(penalty_locs),1)];
% initial locations
initial_desc.locs = [[3,1];];
% non-zero weights for the initial distribution
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = environment.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc);
end

%--------------------------------------------------------------------------
