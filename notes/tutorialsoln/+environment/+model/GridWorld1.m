function [ model ] = GridWorld1()
% The simple GridWorldManual by another method

% number of rows, columns in gridworld
shape = [3,4];
% probability of respectively moving 0 degrees, 90 degrees, 180 degrees, 270 degrees from desired direction.
action_effects = [ 0.8, 0.1, 0.0, 0.1];
% obstacles block movement
obstacles = [[2,2];];
% absorbing/terminal locations
absorbing_locs = [[1,4];[2,4];];
% the default reward
reward_desc.default = -1;
% locations with non-default rewards
reward_desc.special_locs = [[1,4];[2,4]];
% the values for the non-default rewards
reward_desc.special_vals = [[10];[-100]];
% initial locations
initial_desc.locs = [[3,1];];
% non-zero weights for the initial distribution
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = environment.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc);
end

%--------------------------------------------------------------------------
