function [ model ] = GridWorld7()

% number of rows, columns in gridworld
shape = [5,5];
% probability of respectively moving 0 degrees, 90 degrees, 180 degrees, 270 degrees from desired direction.
action_effects = [ 1, 0, 0, 0];
% obstacles block movement
obstacles = [[2,2];[3,2];[3,4];[4,4]];
% penalty locations are both absorbing and have non-default rewards
penalty_locs = [];
% goal locations are both absorbing and have non-default rewards
goal_locs = [[3,3]];
% absorbing/terminal locations
absorbing_locs = [goal_locs;penalty_locs];
% the default reward
reward_desc.default = 0;
% locations with non-default rewards
reward_desc.special_locs = absorbing_locs;
% the values for the non-default rewards
reward_desc.special_vals = [10];
% initial locations
initial_desc.locs = [[[1:5]' ones(5,1)];[1,2];[4,2];[5,2];[1,3];[2,3];[4,3];[5,3];[1,4];[2,4];[5,4];[[1:5]' 5*ones(5,1)]];
% non-zero weights for the initial distribution
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = environment.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc);
end

%--------------------------------------------------------------------------
