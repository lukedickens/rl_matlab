function [ model ] = GridWorld4()
%%GRIDWORLD4 the cliff grid world from Sutton and Barto, 1998, MIT Press

% number of rows, columns in gridworld
shape = [4,12];
% probability of respectively moving 0 degrees, 90 degrees, 180 degrees, 270 degrees from desired direction.
action_effects = [ 1, 0, 0, 0];
% obstacles block movement
obstacles = [];
% absorbing/terminal locations
absorbing_locs = [[4,2];[4,3];[4,4];[4,5];[4,6];[4,7];[4,8];[4,9];[4,10];[4,11];[4,12];];
% the default reward
reward_desc.default = -1;
% locations with non-default rewards
reward_desc.special_locs = [4*ones(10,1) [2:11]'];
% the values for the non-default rewards
reward_desc.special_vals = -100*ones(length(reward_desc.special_locs),1);
% initial locations
initial_desc.locs = [[4,1];];
% non-zero weights for the initial distribution
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = environment.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc);
end

%--------------------------------------------------------------------------
