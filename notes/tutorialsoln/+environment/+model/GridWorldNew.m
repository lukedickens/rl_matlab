function [ model ] = GridWorldManual()
%%GRIDWORLDMANUAL creates a simple MDP model of a grid-world, with teleportations

% Number of states
numstates = 11; 
statenames =  ['s01'; 's02'; 's03'; 's04'; 's05'; 's06'; 's07'; 's08'; 's09'; 's10'; 's11'];
% States are laid out as follows:
%  index          name
% 1 2 3 4       S1 S2 S3  S4
% 5 # 6 7   --> S5 #  S6  S7
% 8 9 10 11     S8 S9 S10 S11 

% Number of actions
numactions = 4; 
actionnames = ['N'; 'E'; 'S'; 'W'];
% Actions are as follows: 
%index       name
% 1    -->   N
% 2    -->   E
% 3    -->   S
% 4    -->   W

% Matrix indicating absorbing states
absorbing = [
%1  2   3   4   5   6   7   8   9   10  11 STATE 
0   0   0   1   0   0   1   0   0   0   0  ];

% Matrix indicating absorbing states
initial = [
%1  2   3   4   5   6   7   8   9   10  11 STATE 
0   0   0   0   0   0   0   1   0   0   0  ];

% load transition
T = transition_matrix();

% load reward matrix
R = reward_matrix(numstates,numactions);

%% create the output struct
model.shape = [ 3 4 ];
model.locs = [ [ones(1,4);[1:4]] [2*ones(1,3);[1 3 4]] [3*ones(1,4);[1:4]] ]'; % the row and column coordinates for the states
model.numstates = numstates; % number of states
model.statenames = statenames; % and their names
model.numactions = numactions; % number of actions
model.actionnames = actionnames; % and their names
model.initial = initial/sum(initial); % probability distribution over initial states
model.absorbing = absorbing; % which states terminate an episode
model.t = @(s,a,s_) T(s_,s,a); % the transition function - note the different order of the arguments.
model.r = @(s,a,s_) R(s_,s,a); % the reward function
model.T = T; % the transition probabilities in matrix form
model.R = R; % the rewards in matrix form

end

%--------------------------------------------------------------------------

%% Notation:
%%      prior_s = prior state
%%      prior_a = prior action
%%      post_s = posterior state

% get the transition matrix (defined as local function)
function T = transition_matrix()
%
% 1 2 3 4       S1 S2 S3  S4
% 5 # 6 7   --> S5 #  S6  S7
% 8 9 10 11     S8 S9 S10 S11
%
TN = [
%1  2     3   4   5   6   7   8   9   10  11 FROM STATE 
0.9 0.075 0   0   0.8 0   0   0   0   0   0  ; % 1 TO STATE
0.1 0.6   0.1 0   0   0   0   0   0   0   0  ; % 2
0   0.075 0.8 0   0   0.8 0   0   0   0   0  ; % 3
0   0     0.1 1   0   0   0   0   0   0   0  ; % 4
0   0     0   0   0.2 0   0   0.8 0   0   0  ; % 5
0   0     0   0   0   0.1 0   0   0   0.8 0  ; % 6
0   0     0   0   0   0.1 1   0   0   0   0.8; % 7
0   0     0   0   0   0   0   0.1 0.1 0   0  ; % 8
0   0     0   0   0   0   0   0.1 0.8 0.1 0  ; % 9
0   0     0   0   0   0   0   0   0.1 0   0.1; % 10
0   0.25  0   0   0   0   0   0   0   0.1 0.1; % 11
];
%
TE = [
%1  2    3   4   5   6   7   8   9   10  11 
0.1 0    0   0   0.1 0   0   0   0   0   0  ; % 1 
0.8 0.15 0   0   0   0   0   0   0   0   0  ; % 2
0   0.6  0.1 0   0   0   0   0   0   0   0  ; % 3
0   0    0.8 1   0   0.1 0   0   0   0   0  ; % 4
0.1 0    0   0   0.8 0   0   0.1 0   0   0  ; % 5
0   0    0.1 0   0   0   0   0   0   0.1 0  ; % 6
0   0    0   0   0   0.8 1   0   0   0   0.1; % 7
0   0    0   0   0.1 0   0   0.1 0   0   0  ; % 8
0   0    0   0   0   0   0   0.8 0.2 0   0  ; % 9
0   0    0   0   0   0   0   0   0.8 0.1 0  ; % 10
0   0.25 0   0   0   0.1 0   0   0   0.8 0.9; % 11
];    
%
TS = [
%1  2   3   4   5   6   7   8   9   10  11
0.1 0.075 0   0   0   0   0   0   0   0   0  ; % 1
0.1 0.6   0.1 0   0   0   0   0   0   0   0  ; % 2
0   0.075 0   1   0   0   0   0   0   0   0  ; % 3
0   0     0.1 0   0   0   0   0   0   0   0  ; % 4
0.8 0     0   0   0.2 0   0   0   0   0   0  ; % 5
0   0     0.8 0   0   0.1 0   0   0   0   0  ; % 6
0   0     0   0   0   0.1 1   0   0   0   0  ; % 7
0   0     0   0   0.8 0   0   0.9 0.1 0   0  ; % 8
0   0     0   0   0   0   0   0.1 0.8 0.1 0  ; % 9
0   0     0   0   0   0.8 0   0   0.1 0.8 0.1; % 10
0   0.25  0   0   0   0   0   0   0   0.1 0.9; % 11 
];    
%
TW = [
%1  2    3   4   5   6   7   8   9   10  11
0.9 0.6  0   0   0.1 0   0   0   0   0   0  ; % 1
0   0.15 0.8 0   0   0   0   0   0   0   0  ; % 2
0   0    0.1 0   0   0.1 0   0   0   0   0  ; % 3
0   0    0   1   0   0   0   0   0   0   0  ; % 4
0.1 0    0   0   0.8 0   0   0.1 0   0   0  ; % 5
0   0    0.1 0   0   0.8 0   0   0   0.1 0  ; % 6
0   0    0   0   0   0   1   0   0   0   0.1; % 7
0   0    0   0   0.1 0   0   0.9 0.8 0   0  ; % 8
0   0    0   0   0   0   0   0   0.2 0.8 0  ; % 9
0   0    0   0   0   0.1 0   0   0   0.1 0.8; % 10
0   0.25 0   0   0   0   0   0   0   0   0.1; % 11
];
%
% T(3,6,4) = 0.1 % i.e. 4th matrix tW, 3rd row, 6th column (postState,prior_s, prior_a)
T = cat(3, TN, TE, TS, TW); % transition probabilities for each action 
end

%--------------------------------------------------------------------------

% the reward function (defined as a local function - subfunction)
function rew = reward_function(prior_s, prior_a, post_s) % reward function (defined locally)
    % only the resulting state is important when considering these rewards
    if post_s == 4
        rew = 10;
    elseif post_s == 7
        rew = -5;
    else
        rew = -1;
    end
end
% get the reward matrix (defined as a local function - subfunction)
function R = reward_matrix(numstates, numactions)
    % i.e. 11x11 matrix of rewards for being in state s, performing action a and ending in state s'
    R = zeros(numstates, numstates, numactions); 
    for i = 1:numstates
       for j = 1:numactions
          for k = 1:numstates
             R(k, i, j) = reward_function(i, j, k);
          end
       end    
    end
end

