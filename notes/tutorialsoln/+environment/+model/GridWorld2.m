function [ model ] = GridWorld2()

% number of rows, columns in gridworld
shape = [4,4];
% probability of respectively moving 0 degrees, 90 degrees, 180 degrees, 270 degrees from desired direction.
action_effects = [ 1, 0, 0, 0];
% obstacles block movement
obstacles = [];
% absorbing/terminal locations
absorbing_locs = [[1,1];[4,4];];
% the default reward
reward_desc.default = -1;
% locations with non-default rewards
reward_desc.special_locs = [];
% the values for the non-default rewards
reward_desc.special_vals = [];
% initial locations
initial_desc.locs = [ones(3,1) [2:4]';2*ones(4,1) [1:4]';3*ones(4,1) [1:4]';4*ones(3,1) [1:3]';];
% non-zero weights for the initial distribution
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = environment.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc);

end
%--------------------------------------------------------------------------
