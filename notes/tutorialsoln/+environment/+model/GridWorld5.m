function [ model ] = GridWorld5()
% A maze from Joe Roop (Joseph.Roop@asdl.gatech.edu) found at:
% http://ccl.northwestern.edu/netlogo/models/community/Reinforcement%20Learning%20Maze

% number of rows, columns in gridworld
shape = [11,11];
% probability of respectively moving 0 degrees, 90 degrees, 180 degrees, 270 degrees from desired direction.
action_effects = [ 1, 0, 0, 0];
% obstacles block movement
obstacles = [];
% penalty locations are both absorbing and have non-default rewards
penalty_locs =  [ ones(11,1) [1:11]'; [2:10]' ones(9,1); [2:10]' 11*ones(9,1); 11*ones(11,1) [1:11]'; 7*ones(4,1) [2:5]'; 7*ones(4,1) [7:10]'; [4,5];[5,5];[5,6];[5,7]; [9,3];[9,4]; ];
% goal locations are both absorbing and have non-default rewards
goal_locs = [[2,10];];
% absorbing/terminal locations
absorbing_locs =  [penalty_locs;goal_locs];
% the default reward
reward_desc.default = 0;
% locations with non-default rewards
reward_desc.special_locs = absorbing_locs;
% the values for the non-default rewards
reward_desc.special_vals = [-10*ones(length(penalty_locs),1); 10];
% initial locations
initial_desc.locs = [[10,2];];
% non-zero weights for the initial distribution
initial_desc.weights = ones(size(initial_desc.locs,1),1);
%
[ model ] = environment.model.BuildGridWorld(shape,obstacles,absorbing_locs,action_effects,reward_desc,initial_desc);
end

%--------------------------------------------------------------------------
